# Dungeon Finder #
* [Website](https://dungeonfinder.vaultofkundarak.com/)
* [Source](https://gitlab.com/VaultOfKundarak/dungeon-finder)
* [Discord](https://discord.gg/bfMZnbz)

## Prerequisites ##
* A game produced by SSG installed on your computer.

## Development Environment Requirements ##
* [Visual Studio 2022](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&channel=Release&version=VS2022) with the following Workloads:
* [DotNet 6.0 .NET Desktop Runtime](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
* [DotNet 6.0 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
* To Build the Installer Project from within Visual Studio 2022:
	* [Heatwave for VS2022 Extension](https://marketplace.visualstudio.com/items?itemName=FireGiant.FireGiantHeatWaveDev17)

## Building the Installer Projects ##
* Use the `build.ps` powershell script from command line, to build the Application: ```powershell -executionpolicy bypass -file .\build.ps1```