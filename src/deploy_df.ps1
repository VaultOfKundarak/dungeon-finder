Param(
    $doPublish=$false,
    $isMandatory=$false
)

Write-Output "Deploy DF (DoPublish: $doPublish, IsMandatory: $isMandatory)..."

$dotnetVersion="net8.0-windows10.0.17763.0"

# $msbuild_path="C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\msbuild.exe"

$sourceProject="DungeonFinder.csproj"
$platform_target="x64"
$build_target="Release"

$download_url="https://s3.amazonaws.com/downloads.dungeonhelper.com/public/DF/AutoUpdate.xml"
$changelog_url="https://s3.amazonaws.com/downloads.dungeonhelper.com/public/DF/Changelog.html"
$is_mandatory=$isMandatory

# find the version from the SharedAssembly.cs
$verFile = "SharedAssembly.cs"
$pattern = '\[assembly: AssemblyVersion\("(.*)"\)\]'

(Get-Content $verFile) | ForEach-Object{
    if($_ -match $pattern){
        $fileVersion = [version]$matches[1]
        #stripping revision from: $version = "{0}.{1}.{2}.{3}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build, ($fileVersion.Revision)
        $version = "{0}.{1}.{2}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build
    }
}

# C:\Users\gitlab_runner\AppData\Local\Microsoft\dotnet\sdk\8.0.403\
# "%ProgramFiles%\Microsoft Visual Studio\2022\Community\Common7\Tools\VsDevCmd.bat"

"Version: $version" | Tee-Object ../../publishOutput.txt -Append | write-host

if ([Environment]::GetCommandLineArgs().Contains('-NonInteractive')) {
    "Non Interactive Console Found." | Tee-Object ../../publishOutput.txt -Append | Write-host
    $InteractiveMode=$false
} else {
    "Interactive Console Found." 
    $InteractiveMode=$true
}

$final_version_msi_name="Dungeon_Finder_$($version).msi"

# if we don't have publish speficied yet, so ask the dev user:
if (($doPublish -eq $False) -and ($InteractiveMode -eq $True)) {
    $doPublish = $(read-host "to continue, press Y and hit the enter key:").ToLower() -eq "y"
}

$bucket = "downloads.dungeonhelper.com"
$keyBase = "public/DF/"

$installVersionKey = $keyBase + "$final_version_msi_name"
$autoUpdateKey = $keyBase + "AutoUpdate.xml"
$latestInstallKey = $keyBase + "Dungeon_Finder.msi"
$changelogKey = $keyBase + "Changelog.html"

Write-host "Bucket: $bucket Keys: $installVersionKey $latestInstallKey $autoUpdateKey $changelogKey"

if ($PSVersionTable.PSVersion.Major -gt 3) {
    "Checking running directory: " + $PSScriptRoot | Tee-Object ../../publishOutput.txt -Append | write-host
}

if ($doPublish -eq $True) {
    # upload versioned msi installer to s3
    if ((Test-Path ../builds/$final_version_msi_name) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $installVersionKey -File ../builds/$final_version_msi_name -CannedACL "Public-Read"
        "uploaded ../builds/$final_version_msi_name to https://s3.amazonaws.com/$bucket/$installVersionKey" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../builds/$final_version_msi_name" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../builds/
        exit 1
    }
    # upload the "always latest" installer to s3
    if ((Test-Path ../builds/Dungeon_Finder.msi) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $latestInstallKey -File ../builds/Dungeon_Finder.msi -CannedACL "Public-Read"
        "uploaded ../builds/Dungeon_Finder.msi to https://s3.amazonaws.com/$bucket/$latestInstallKey" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../builds/Dungeon_Finder.msi" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../builds/
        exit 1
    }
    # upload AutoUpdate.xml to s3
    if ((Test-Path ../builds/AutoUpdate.xml) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $autoUpdateKey -File ../builds/AutoUpdate.xml -CannedACL "Public-Read"
        "uploaded ../builds/AutoUpdate.xml to https://s3.amazonaws.com/$bucket/$autoUpdateKey" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../builds/AutoUpdate.xml" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../builds/
        exit 1
    }
    # upload Changelog.html to s3
    if ((Test-Path ../builds/Changelog.html) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $changelogKey -File ../builds/Changelog.html -CannedACL "Public-Read"
        "uploaded ../builds/Changelog.html to https://s3.amazonaws.com/$bucket/$changelogKey" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../Changelog.html" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../builds/
        exit 1
    }
}