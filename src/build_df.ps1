Param(
    $doPublish=$false,
    $isMandatory=$false
)

$starting_location = Get-Location

Write-Output "Begining build of DF (DoPublish: $doPublish, IsMandatory: $isMandatory)..."

$dotnetVersion="net8.0-windows10.0.17763.0"

# $msbuild_path="C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\msbuild.exe"

$sourceProject="DungeonFinder.csproj"
$platform_target="x64"
$build_target="Release"
$changelog_url="https://s3.amazonaws.com/downloads.dungeonhelper.com/public/DF/Changelog.html"

# XML requires that we have a lowercase, so we are forcing the string values:
if ($isMandatory) {
    $is_mandatory="true"
} else {
    $is_mandatory="false"
}

# find the version from the SharedAssembly.cs
$verFile = "SharedAssembly.cs"
$pattern = '\[assembly: AssemblyVersion\("(.*)"\)\]'

(Get-Content $verFile) | ForEach-Object{
    if($_ -match $pattern){
        $fileVersion = [version]$matches[1]
        #stripping revision from: $version = "{0}.{1}.{2}.{3}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build, ($fileVersion.Revision)
        $version = "{0}.{1}.{2}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build
    }
}

$final_version_msi_name="Dungeon_Finder_$($version).msi"
$download_url="https://s3.amazonaws.com/downloads.dungeonhelper.com/public/DF/Dungeon_Finder_$($version).msi"

# C:\Users\gitlab_runner\AppData\Local\Microsoft\dotnet\sdk\8.0.403\
# "%ProgramFiles%\Microsoft Visual Studio\2022\Community\Common7\Tools\VsDevCmd.bat"

"Version: $version" | Tee-Object ../../publishOutput.txt -Append | write-host

if ([Environment]::GetCommandLineArgs().Contains('-NonInteractive')) {
    "Non Interactive Console Found." | Tee-Object ../../publishOutput.txt -Append | Write-host
    $InteractiveMode=$false
} else {
    "Interactive Console Found." | Tee-Object ../../publishOutput.txt -Append | write-host
    $InteractiveMode=$true
}

# install dotnet tool prereqs
# dotnet tool install --global zipper | Tee-Object ../../publishOutput.txt -Append

# C:\Dev\VoK\dungeon-finder\src\DungeonFinder\
cd DungeonFinder

# make the builds directory if it does not exist
if ((Test-Path ../../builds/) -eq $False) {
  md ../../builds/
}

"Copying Changelog.html to ../../builds/" | Tee-Object ../../../publishOutput.txt -Append | Write-host
Copy-Item -Force ../Changelog.html ../../builds/Changelog.html

"Restoring packages..." | Tee-Object ../../../publishOutput.txt -Append | Write-host
dotnet restore | Tee-Object ../../publishOutput.txt -Append | write-host

"Updating DF AutoUpdate.xml data..." | Tee-Object ../../../publishOutput.txt -Append | Write-host

# create new update xml
$updateXml = $(Get-Content AutoUpdate.xml).replace("{version}", $version).replace("{downloadUrl}",$download_url).replace("{changelogUrl}",$changelog_url).replace("{mandatory}",$is_mandatory)

"Building Dungeon Finder..." | Tee-Object ../../../publishOutput.txt -Append | Write-host
# build time!
msbuild $sourceProject -p:Configuration=Release -p:Pipeline=Gitlab | Tee-Object ../../publishOutput.txt

if ((Test-Path "bin/Release/$dotnetVersion/win-x64/DungeonFinder.dll") -eq $False) 
{
	Write-Host "Could not find bin/Release/$dotnetVersion/win-x64/DungeonFinder.dll"
    ls bin/
    ls bin/Release/
    ls bin/Release/$dotnetVersion/
    ls bin/Release/$dotnetVersion/win-x64/
	exit 1
}

# Outputs to: C:\Dev\VoK\dungeon-finder\src\DungeonFinder\bin\Any CPU\Release\net8.0-windows\win-x64

# C:\Dev\VoK\dungeon-finder\src\DungeonFinder.WiXHarvester\
cd ..\\DungeonFinder.WiXHarvester

#& $msbuild_path -property:Configuration=$build_target -property:Platform=$platform_target .\DungeonFinder.WiXHarvester.csproj | Tee-Object ../../../publishOutput.txt -Append

"Restoring packages for the WiXHarvester..." | Tee-Object ../../../publishOutput.txt -Append | Write-host
dotnet restore | Tee-Object ../../publishOutput.txt -Append | write-host

"Building WiXHarvester..." | Tee-Object ../../../publishOutput.txt -Append | Write-host
dotnet build .\DungeonFinder.WiXHarvester.csproj

"Running WiXHarvester..." | Tee-Object ../../../publishOutput.txt -Append | Write-host

# run the template creation tool
dotnet run --confirm --inputPath="..\\DungeonFinder\\bin\\Release\\$dotnetVersion\\win-x64\\" --verbose -c Release --project DungeonFinder.WiXHarvester.csproj | Tee-Object ../../../publishOutput.txt -Append

# C:\Dev\VoK\dungeon-finder\src\DungeonFinder.Installer\
cd ..\\DungeonFinder.Installer

"Restoring packages for the Installer..." | Tee-Object ../../../publishOutput.txt -Append | Write-host
dotnet restore | Tee-Object ../../publishOutput.txt -Append | write-host

"Packaging MSI..." | Tee-Object ../../../publishOutput.txt -Append | Write-host

# dotnet build -c Release -verbosity:d
# & $msbuild_path -property:Configuration=$build_target -property:Platform=$platform_target | Tee-Object ../../../publishOutput.txt -Append
msbuild -p:Configuration=Release .\DungeonFinder.Installer.wixproj

if ((Test-Path "bin/Release/en-US/DungeonFinder.Installer.msi") -eq $False) 
{
	Write-Host "bin/Release/en-US/DungeonFinder.Installer.msi"
    ls bin/Release/
    ls bin/Release/en-US
	exit 1
}

# outputs: C:\Dev\VoK\dungeon-finder\src\DungeonFinder.Installer\bin\Any CPU\Release\en-US\DungeonFinder.Installer.msi
# outputs: C:\Dev\VoK\dungeon-finder\src\DungeonFinder.Installer\bin\Any CPU\Release\en-US\DungeonFinder.Installer.wixpdb

# Auto Update file
if (($doPublish -eq $False) -and ($InteractiveMode -eq $True)) {
    "Updated AutoUpdate.xml, on deployment would be:" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    $updateXml
} else {
    "Writing ../../builds/AutoUpdate.xml" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    out-file -FilePath ../../builds/AutoUpdate.xml -InputObject $updateXml
}

"Moving Dungeon_Finder.msi and $final_version_msi_name to ../../builds/" | Tee-Object ../../../publishOutput.txt -Append | Write-host

# copy over the msi
Copy-Item -Force bin/Release/en-US/DungeonFinder.Installer.msi ../../builds/$final_version_msi_name
Copy-Item -Force bin/Release/en-US/DungeonFinder.Installer.msi ../../builds/Dungeon_Finder.msi

if ($PSVersionTable.PSVersion.Major -gt 3) {
    $current_location = Get-Location
    "Checking running directory: " + $PSScriptRoot | Tee-Object ../../publishOutput.txt -Append | write-host
    "Starting location: $starting_location" | Tee-Object ../../publishOutput.txt -Append | write-host
    "Current location: $current_location" | Tee-Object ../../publishOutput.txt -Append | write-host
}

# if we don't have publish speficied yet, so ask the dev user:
if (($doPublish -eq $False) -and ($InteractiveMode -eq $True)) {
    $doPublish = $(read-host "to continue, press Y and hit the enter key:").ToLower() -eq "y"
}

if ($doPublish -eq $True) {
    $bucket = "downloads.dungeonhelper.com"
    $keyBase = "public/DF/"

    $installVersionKey = $keyBase + "$final_version_msi_name"
    $autoUpdateKey = $keyBase + "AutoUpdate.xml"
    $latestInstallKey = $keyBase + "Dungeon_Finder.msi"
    $changelogKey = $keyBase + "Changelog.html"

    # writing version file:
    "Writing version file $version to verstion.txt" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    out-file -FilePath ../../builds/verstion.txt -InputObject $version

    # upload versioned msi installer to s3
    if ((Test-Path ../../builds/$final_version_msi_name) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $installVersionKey -File ../../builds/$final_version_msi_name -CannedACL "Public-Read"
        "uploaded ../../builds/$final_version_msi_name to https://s3.amazonaws.com/$bucket/$keyBase/$final_version_msi_name" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../../builds/$final_version_msi_name" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../../builds/
        exit 1
    }
    # upload the "always latest" installer to s3
    if ((Test-Path ../../builds/Dungeon_Finder.msi) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $latestInstallKey -File ../../builds/Dungeon_Finder.msi -CannedACL "Public-Read"
        "uploaded ../../builds/Dungeon_Finder.msi to https://s3.amazonaws.com/$bucket/$keyBase/Dungeon_Finder.msi" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../../builds/Dungeon_Finder.msi" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../../builds/
        exit 1
    }
    # upload AutoUpdate.xml to s3
    if ((Test-Path ../../builds/AutoUpdate.xml) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $autoUpdateKey -File ../../builds/AutoUpdate.xml -CannedACL "Public-Read"
        "uploaded ../../builds/AutoUpdate.xml to https://s3.amazonaws.com/$bucket/$keyBase/AutoUpdate.xml" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ../../builds/AutoUpdate.xml" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../../builds/
        exit 1
    }
    # upload Changelog.html to s3
    if ((Test-Path ../../builds/Changelog.html) -eq $True) {
        Write-S3Object -BucketName $bucket -Key $changelogKey -File ../../builds/Changelog.html -CannedACL "Public-Read"
        "uploaded ../../builds/Changelog.html to https://s3.amazonaws.com/$bucket/$keyBase/Changelog.html" | Tee-Object ../../../publishOutput.txt -Append | Write-host
    } else {
        "Could not find ./../Changelog.html" | Tee-Object ../../../publishOutput.txt -Append | Write-host
        ls ../../builds/
        exit 1
    }

    # cleanup update file
    remove-item ../../builds/AutoUpdate.xml
} else {
    "Finished building, locate ../../builds/$final_version_msi_name." | Tee-Object ../../publishOutput.txt -Append | Write-host
}

# save version for gitlab tag:
$env:version=$version

# return to the folder where we started:
Set-Location $starting_location

# report where we end up
$current_location = Get-Location
Write-host "Location: $current_location"