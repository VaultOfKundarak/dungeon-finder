﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DungeonFinder.WiXHarvester
{
    public static class Program
    {
        private static List<string> outputStrings = new List<string>();

        private static bool _silent = false;

        private readonly static string _dotnetVersion = "net5.0-windows";

        private static void SaveString(string content)
        {
            if (!_silent)
                Console.WriteLine(content);
            outputStrings.Add(content);
        }

        // dumb tab fix:
        private static string? DoPossibleTab(string? contentCheck, int directoriesDeepLevel)
        {

            for (int i = directoriesDeepLevel; i >= 0; i--)
            {
                if (!_silent)
                {
                    Console.Write("\t");
                }
                contentCheck = "\t" + contentCheck;
            }

            return contentCheck;
        }

        public static void Main(string[] args)
        {
            // Author: RabidSquirrel:
            // Date: 9/12/23
            // Description: This app will take all of the files from the target output directory (win-x64 SCD publish folder) and template them for source code inclusion.
            bool autoConfirm = false;

            string inputFromCommandLine = string.Empty;

            // This app is created to output a single file that can be used to replace ApplicationFilesComponents.wxs in the wix4 installer project.
            if (args.Length > 0)
            {
                foreach (var arg in args)
                {
                    switch (arg?.ToLower())
                    {
                        case "--confirm":
                            autoConfirm = true;
                            _silent = true;
                            break;
                    }

                    if (arg != null && arg.ToLower().Contains("--inputpath="))
                    {
                        inputFromCommandLine = arg.Split("=")[1];
                    }
                }
            }

            var currentDir = Directory.GetCurrentDirectory();
            var relPath = string.Empty;
            var inputPath = string.Empty;

            if (!_silent)
                Console.WriteLine($"Current directory: {currentDir}");

            if (!currentDir.EndsWith("DungeonFinder.WiXHarvester"))
                relPath += "..\\..\\..\\";

            if (!string.IsNullOrEmpty(inputFromCommandLine))
            {
                if (!_silent)
                    Console.WriteLine($"Command Line Input Path: {inputFromCommandLine}");
                inputPath = Path.GetFullPath(inputFromCommandLine);
                Console.WriteLine($"Full Path: {inputPath}");
            }
            else
            {
                if (currentDir == "C:\\")
                {
                    currentDir = "C:\\Dev\\VoK\\dungeon-finder\\src\\DungeonFinder.WiXHarvester";
                }

                inputPath = relPath + $"..\\DungeonFinder\\bin\\x64\\Release\\{_dotnetVersion}\\win-x64\\";
            }

            if (!Directory.Exists(inputPath))
            {
                Console.WriteLine($"Resolved: {Path.GetFullPath(inputPath)}");
                Console.WriteLine("Unable to find input directory. \r\n\tCurrent directory is: " + Directory.GetCurrentDirectory() + "\nexpected: " + inputPath);
                return;
            }

            // confirm the output file is correct:
            // C:\Dev\VoK\dungeon-finder\src\DungeonFinder.WiXHarvester ->
            // C:\Dev\VoK\dungeon-finder\src\DungeonFinder.Installer\
            var outputDirectory = Path.GetFullPath(Path.Combine(currentDir, relPath + "..\\DungeonFinder.Installer"));

            if (!Directory.Exists(outputDirectory))
            {
                Console.WriteLine($"Resolved: {Path.GetFullPath(currentDir)}");
                Console.WriteLine($"Could not find output directory: {outputDirectory}");
                return;
            }

            var inputFiles = Directory.GetFiles(inputPath, "*.*", SearchOption.AllDirectories);

            if (!_silent)
            {
                Console.WriteLine($"Processing Input Path: {inputPath}");
                Console.WriteLine($"{inputFiles.Count()} files to process:");
            }

            List<string> outputFiles = new List<string>();
            List<string> foldersCreated = new List<string>();
            List<string> skippedFiles = new List<string>();

            // Header for files that require:
            SaveString("<?define sourceExePath=\"$(var.DungeonFinder.TargetDir)\" ?>\r\n");
            SaveString("<Wix xmlns=\"http://wixtoolset.org/schemas/v4/wxs\">");
            SaveString("\t<Fragment>");
            SaveString("\t\t<DirectoryRef Id=\"INSTALLFOLDER\">");

            var remover = Path.GetFullPath(Path.Combine(inputPath));
            // base directory = 
            if (remover.EndsWith("\\"))
                remover = remover.Substring(0, remover.Length - 1);

            // placeholder for directory traversal
            var previousDirectoryStripped = string.Empty;
            var currentDirectoryStripped = string.Empty;
            var parentDirectoryStripped = string.Empty;

            int currentDirectoryLevel = 0;
            int previousDirectoryLevel = 0;

            DirectoryInfo currentDirectoryInfo;
            DirectoryInfo previousDirectoryInfo = new DirectoryInfo(Path.GetFullPath(inputPath));

            var removeFoldersGuid = Guid.NewGuid();

            // for each file, add it to the template:
            foreach (var currentFilePath in inputFiles)
            {
                // We filter out publish, unobfuscated, mapping.txt and all pdb, to prevent debug symbols and non-needed items from entering the installer.
                if (currentFilePath.Contains("publish\\"))
                {
                    skippedFiles.Add(currentFilePath);
                    continue;
                }
                // Now includes plugins:
                /*                if (line.Contains("plugins\\"))
                                {
                                    skippedFiles.Add(line);
                                    continue;
                                }*/
                if (currentFilePath.Contains("unobfuscated\\"))
                {
                    skippedFiles.Add(currentFilePath);
                    continue;
                }
                if (currentFilePath.Contains("Mapping.txt"))
                {
                    skippedFiles.Add(currentFilePath);
                    continue;
                }
                /*
                Now including DF PDB for debug purposes.
                if (currentFilePath.Contains(".pdb"))
                {
                    skippedFiles.Add(currentFilePath);
                    continue;
                }
                */
                if (currentFilePath.Contains("settings.json"))
                {
                    skippedFiles.Add(currentFilePath);
                    continue;
                }

                // grab all of the file details for processing:
                var fileName = Path.GetFileName(currentFilePath);
                var fileNameStripped = currentFilePath.Replace(inputPath, string.Empty);
#pragma warning disable CS8604 // Possible null reference argument.
                currentDirectoryInfo = new DirectoryInfo(Path.GetDirectoryName(currentFilePath));
#pragma warning restore CS8604 // Possible null reference argument.

                parentDirectoryStripped = currentDirectoryInfo?.Parent?.FullName.Replace(remover, string.Empty);
                currentDirectoryStripped = currentDirectoryInfo?.FullName.Replace(remover, string.Empty) ?? string.Empty;
                currentDirectoryLevel = GetDirectoryLevel(currentDirectoryStripped);

                var currentFolders = currentDirectoryLevel != 0 ? GetFolderList(currentDirectoryStripped) : null;

                // this is a subverted path, since were manipulating the root:
                var finalFilePath = !string.IsNullOrWhiteSpace(fileNameStripped) ? fileNameStripped : fileName;

                var idString = finalFilePath.Replace("-", "_").Replace("\\", ".");

                // remove period from the beging of any id string:
                if (idString[0] == '.')
                {
                    idString = idString.Remove(0, 1);
                }

                // generate new guid (not sure if this needs a base)
                var componentGuid = Guid.NewGuid();

                var previousDirectory = previousDirectoryInfo?.FullName ?? string.Empty;
                if (previousDirectory.EndsWith("\\"))
                    previousDirectory = previousDirectory.Substring(0, previousDirectory.Length - 1);

                // determine's if we are in adjacent folder
                var parentIsPreviousDirectory = currentDirectoryInfo?.Parent?.FullName == previousDirectoryInfo?.FullName;
                var parentSharesRoot = currentDirectoryInfo?.Parent?.FullName == previousDirectoryInfo?.Parent?.FullName;

                // this is the sub-folder logic, runs if the files are in a sub directory:
                if (currentDirectoryInfo?.FullName != previousDirectory)
                {
                    // second loop, 
                    if (previousDirectoryLevel > 0)
                    {
                        // check if our levels are the same or less, if they are different then we need to remove some directories:
                        var previousFolders = GetFolderList(previousDirectoryStripped);
                        int removedPreviousFolders = 0;

                        if (previousFolders != null && currentFolders != null)
                        {
                            foreach (var removeFolder in currentFolders)
                            {
                                if (previousFolders.Contains(removeFolder))
                                {
                                    removedPreviousFolders++;
                                    previousFolders.Remove(removeFolder);
                                }
                            }
                        }

                        if (previousFolders != null)
                        {
                            if (!parentSharesRoot)
                            {
                                if (removedPreviousFolders == 0)
                                {
                                    foreach (var folderToRemove in previousFolders)
                                    {
                                        SaveString($"{DoPossibleTab("</Directory>", previousDirectoryLevel)}");
                                    }
                                }
                                else
                                {
                                    for (int i = removedPreviousFolders; i > 0; i--)
                                    {
                                        SaveString($"{DoPossibleTab("</Directory>", previousDirectoryLevel)}");
                                    }
                                }
                            }
                            else
                            {
                                foreach (var folderToRemove in previousFolders)
                                {
                                    SaveString($"{DoPossibleTab("</Directory>", previousDirectoryLevel)}");
                                }
                            }
                        }
                    }

                    // is this a child of a folder we already created?
                    // if it is, we need to strip out the parent folders and remove them from our current list and/or count
                    // to determine how many Directory elements that we will create



                    if (parentIsPreviousDirectory && !string.IsNullOrWhiteSpace(previousDirectoryStripped))
                    {
                        // remove parent from subfolders
                        // check if the previous directory is a parent to the current directory
                        var strippedDirectory = currentDirectoryStripped?.Replace(previousDirectoryStripped, string.Empty);
                        currentFolders = GetFolderList(strippedDirectory);
                    }

                    var subFolderNames = currentFolders != null ? currentFolders.ToList() : new List<string>();

                    // could be root or subfolder is the same:
                    if (parentSharesRoot)
                    {
                        // remove parent from folder, we already did this:
                        var previousFolders = GetFolderList(previousDirectoryStripped);

                        if (previousFolders != null && currentFolders != null)
                        {
                            foreach (var removeFolder in previousFolders)
                            {
                                if (currentFolders.Contains(removeFolder))
                                    currentFolders.Remove(removeFolder);
                            }
                        }
                    }


                    if (currentFolders != null)
                    {
                        List<string> prev = new List<string>();

                        // no previous directory, just create as many folders as we need
                        foreach (var folder in currentFolders)
                        {
                            var prevString = string.Join(".", prev);
                            var folderId = GetFolderId(folder, currentDirectoryStripped ?? string.Empty);
                            if (!foldersCreated.Contains(folderId))
                            {
                                SaveString($"{DoPossibleTab(string.Empty, currentDirectoryLevel)}<Directory Id=\"{folderId}\" Name=\"{folder}\">");
                                prev.Add(folder);
                                foldersCreated.Add(folderId);
                            }
                        }
                    }
                }

                // output file component with ancillary requirements
                DoPossibleTab(previousDirectoryStripped, currentDirectoryLevel);
                SaveString($"\t\t\t<Component Id=\"{idString}\" Guid=\"{componentGuid}\">");
                DoPossibleTab(previousDirectoryStripped, currentDirectoryLevel);
                SaveString($"\t\t\t\t<RegistryValue Root=\"HKCU\" Key=\"Software\\Vault of Kundarak\\Dungeon Finder\\Uninstall\" Name=\"[INSTALLFOLDER]{finalFilePath}\" Type=\"string\" Value=\"[INSTALLFOLDER]{finalFilePath}\" KeyPath =\"yes\" />");
                DoPossibleTab(previousDirectoryStripped, currentDirectoryLevel);
                SaveString($"\t\t\t\t<File Id=\"File.{idString}\" Source=\"$(var.sourceExePath){finalFilePath}\" Checksum =\"yes\" />");
                DoPossibleTab(previousDirectoryStripped, currentDirectoryLevel);
                SaveString($"\t\t\t</Component>");

                // save to output compenent listing
                outputFiles.Add(idString);

                // looping, setting the current directory to the last:
                if (currentDirectoryInfo != null && currentDirectoryInfo != previousDirectoryInfo)
                {
                    previousDirectoryInfo = currentDirectoryInfo;
                    previousDirectoryStripped = currentDirectoryStripped;
                    previousDirectoryLevel = currentDirectoryLevel;
                }
            }

            // close up remaining directories:
            for (int i = (currentDirectoryLevel - 1); i >= 0; i--)
            {
                SaveString($"{DoPossibleTab("</Directory>", i)}");
            }

            SaveString($"\t<Component Id=\"RemoveAppFolders\" Guid=\"{removeFoldersGuid}\">");
            SaveString("\t\t<RegistryValue Root=\"HKCU\" Key=\"Software\\Vault of Kundarak\\Dungeon Finder\\Uninstall\" Name=\"RemoveAppFolders\" Type=\"string\" Value=\"d3bf5251-3a41-47bc-a880-105cd884d37f\" KeyPath =\"yes\" />");
            // this is your removal, needs to be placed somewhere to delete the extra folders:
            foreach (var outputFile in foldersCreated)
            {
                SaveString($"\t\t<RemoveFolder Directory=\"{outputFile}\" On=\"uninstall\" />");
            }
            SaveString("\t</Component>");

            // finalize the end of the xml element for directory and fragment
            SaveString("\t</DirectoryRef>\r\n</Fragment>");

            // this is your component List to reference all of the files:
            SaveString("<Fragment>\r\n\t<ComponentGroup Id=\"ApplicationFilesComponent\">");
            foreach (var outputFile in outputFiles)
            {
                SaveString($"\t\t\t<ComponentRef Id=\"{outputFile}\" />");
            }
            SaveString("\t\t\t<ComponentRef Id=\"RemoveAppFolders\" />");
            SaveString("\t\t</ComponentGroup>\r\n\t</Fragment>\r\n</Wix>");

            if (!_silent)
                Console.WriteLine($"Skiped files: {skippedFiles.Count()}");

            // pause? okay this is easier
            bool confirm = false;
            if (!autoConfirm)
            {
                Console.WriteLine("Would you like to overwrite the current ApplicationFilesComponents.wxs? Enter Y, to proceed:");
                Console.WriteLine(Environment.NewLine + "Waiting on you, captain ...");
                var userQuestion = Console.ReadLine();
                confirm = (userQuestion != string.Empty && userQuestion?.ToLowerInvariant() == "Y".ToLowerInvariant());
            }

            if (autoConfirm || confirm)
            {
                var output = string.Join(Environment.NewLine, outputStrings);
                var outputFile = Path.Combine(outputDirectory, "ApplicationFilesComponents.wxs");

                File.WriteAllText(outputFile, output);
                Console.WriteLine($"Wix template created: {outputFile}.");
            }
        }

        private static string GetFolderId(string folder, string subFolderNames)
        {
            string result = "ApplicationFilesFolder.";

            //var folderIdString = "ApplicationFilesFolder." + (subfolders != null ? string.Join(".", subfolders)
            //: currentDirectoryInfo?.Name.Replace("-", "_").Replace("\\", "."));

            var list = GetFolderList(subFolderNames);

            if (list != null)
            {
                List<string> folders = new List<string>();

                foreach (var subFolder in list)
                {
                    folders.Add(subFolder.Replace("-", "_").Replace("\\", "."));
                    if (subFolder == folder)
                    {
                        return result + string.Join(".", folders);
                    }
                }
            }

            return result + folder;
        }

        private static List<string>? GetFolderList(string? currentDirectoryStripped)
        {
            if (currentDirectoryStripped == null)
                return null;

            List<string> results = new List<string>();

            foreach (var item in currentDirectoryStripped.Split('\\'))
            {
                if (item.Length > 0)
                    results.Add(item);
            }

            return results;
        }

        // count the slashes and return the number as the "level"
        // the input string should already be stripped down to the base output directory of 'win-x64'
        private static int GetDirectoryLevel(string? currentDirectoryStripped)
        {
            if (currentDirectoryStripped == null)
                return 0;
            int count = 0;
            foreach (var c in currentDirectoryStripped)
            {
                if (c == '\\')
                    count++;
            }
            return count;
        }
    }
}