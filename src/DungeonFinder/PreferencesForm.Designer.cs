﻿using DungeonFinder.Properties;

namespace DungeonFinder
{
    partial class PreferencesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreferencesForm));
            btnCancel = new System.Windows.Forms.Button();
            btnSave = new System.Windows.Forms.Button();
            txtBxPreferencesPath = new System.Windows.Forms.TextBox();
            lblName = new System.Windows.Forms.Label();
            btnBrowse = new System.Windows.Forms.Button();
            btnClear = new System.Windows.Forms.Button();
            lblError = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(245, 62);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(63, 23);
            btnCancel.TabIndex = 8;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnSave
            // 
            btnSave.Location = new System.Drawing.Point(12, 62);
            btnSave.Name = "btnSave";
            btnSave.Size = new System.Drawing.Size(103, 23);
            btnSave.TabIndex = 9;
            btnSave.Text = "Save Preferences";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // txtBxPreferencesPath
            // 
            txtBxPreferencesPath.Location = new System.Drawing.Point(110, 12);
            txtBxPreferencesPath.Name = "txtBxPreferencesPath";
            txtBxPreferencesPath.Size = new System.Drawing.Size(155, 23);
            txtBxPreferencesPath.TabIndex = 11;
            // 
            // lblName
            // 
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(12, 16);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(92, 15);
            lblName.TabIndex = 10;
            lblName.Text = "Preferences File:";
            // 
            // btnBrowse
            // 
            btnBrowse.Location = new System.Drawing.Point(271, 11);
            btnBrowse.Name = "btnBrowse";
            btnBrowse.Size = new System.Drawing.Size(37, 25);
            btnBrowse.TabIndex = 12;
            btnBrowse.Text = "...";
            btnBrowse.UseVisualStyleBackColor = true;
            btnBrowse.Click += btnBrowse_Click;
            // 
            // btnClear
            // 
            btnClear.Location = new System.Drawing.Point(176, 62);
            btnClear.Name = "btnClear";
            btnClear.Size = new System.Drawing.Size(63, 23);
            btnClear.TabIndex = 13;
            btnClear.Text = "Clear";
            btnClear.UseVisualStyleBackColor = true;
            btnClear.Click += btnClear_Click;
            // 
            // lblError
            // 
            lblError.AutoSize = true;
            lblError.Location = new System.Drawing.Point(12, 44);
            lblError.Name = "lblError";
            lblError.Size = new System.Drawing.Size(0, 15);
            lblError.TabIndex = 14;
            // 
            // PreferencesForm
            // 
            AcceptButton = btnSave;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(326, 97);
            Controls.Add(lblError);
            Controls.Add(btnClear);
            Controls.Add(btnBrowse);
            Controls.Add(txtBxPreferencesPath);
            Controls.Add(lblName);
            Controls.Add(btnSave);
            Controls.Add(btnCancel);
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MaximumSize = new System.Drawing.Size(342, 136);
            MinimizeBox = false;
            MinimumSize = new System.Drawing.Size(342, 136);
            Name = "PreferencesForm";
            SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            Text = "Change Preferences";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtBxPreferencesPath;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblError;
    }
}