﻿using DungeonFinder.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonFinder
{
    public class AdvancedOptionsMenuStrip : ContextMenuStrip
    {
        public ShortcutTag CurrentShortcutTag { get; private set; }
        public GroupTag CurrentGroup { get; private set; }

        // hollow constructor
        public AdvancedOptionsMenuStrip(IContainer container) : base()
        {
        }

        protected override void OnOpening(CancelEventArgs e)
        {
            base.OnOpening(e);

            if (CurrentShortcutTag != null)
            {
                if (CurrentShortcutTag.Shortcut.ShowInTray)
                {
                    Items["hideThisShortcutFromTrayToolStripMenuItem"].Visible = true;
                    Items["showThisShortcutInTrayToolStripMenuItem"].Visible = false;
                } 
                else
                {
                    Items["hideThisShortcutFromTrayToolStripMenuItem"].Visible = false;
                    Items["showThisShortcutInTrayToolStripMenuItem"].Visible = true;
                }
            }

            if (CurrentGroup != null)
            {
                if (CurrentGroup.Group.ShowInTray)
                {
                    Items["hideThisShortcutFromTrayToolStripMenuItem"].Visible = true;
                    Items["showThisShortcutInTrayToolStripMenuItem"].Visible = false;
                }
                else
                {
                    Items["hideThisShortcutFromTrayToolStripMenuItem"].Visible = false;
                    Items["showThisShortcutInTrayToolStripMenuItem"].Visible = true;
                }
            }
        }

        internal void SetShortcutTag(object d)
        {
            if (d is ShortcutTag)
            {
                CurrentShortcutTag = d as ShortcutTag;
                Items["getCommandLineArgumentsToolStripMenuItem"].Visible = true;
                Items["copyShortcutIdShortcutToolStripMenuItem"].Visible = true;
                CurrentGroup = null;

                // If Shortcut has a Preferences File Set, Show Update
                if (!string.IsNullOrWhiteSpace(CurrentShortcutTag.Shortcut.Preferences))
                {
                    Items["assignCustomPreferencesFileToolStripMenuItem"].Visible = false;
                    Items["updateCustomPreferencesFileToolStripMenuItem"].Visible = true;
                }
                else
                {
                    Items["assignCustomPreferencesFileToolStripMenuItem"].Visible = true;
                    Items["updateCustomPreferencesFileToolStripMenuItem"].Visible = false;
                }
            }
            if (d is GroupTag)
            {
                CurrentGroup = d as GroupTag;
                Items["getCommandLineArgumentsToolStripMenuItem"].Visible = false;
                Items["copyShortcutIdShortcutToolStripMenuItem"].Visible = false;
                CurrentShortcutTag = null;

                // Hide Preferences Options for Group Shortcuts
                Items["assignCustomPreferencesFileToolStripMenuItem"].Visible = false;
                Items["updateCustomPreferencesFileToolStripMenuItem"].Visible = false;
            }
        }
    }
}
