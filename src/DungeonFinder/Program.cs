﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Newtonsoft.Json;
using DungeonFinder.ServiceClients;
using AutoUpdaterDotNET;
using System.Threading.Tasks;
using CommandLine;
using DungeonFinder.Models;
using System.Collections.Generic;
using Shortcut = DungeonFinder.Models.Shortcut;
using DungeonFinder.Properties;

namespace DungeonFinder
{
    static class Program
    {
        public static TrayForm TrayIcon;

        public static MainForm MainForm;

        public static LoginForm LoginForm;

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static bool _silentRun = false;
#pragma warning disable CS0414 // The field 'Program._suppressUpdatePop' is assigned but its value is never used
        private static bool _suppressUpdatePop = true;
#pragma warning restore CS0414 // The field 'Program._suppressUpdatePop' is assigned but its value is never used

        private static List<ShortcutTag> _commandLineLaunchRequests = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "Mo said so.")]
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                _log.Debug("Dungeon Finder starting...");

                // skip highlander if we have more then 1 argument from command line:
                if (args?.Length < 1)
                {
                    Thread.CurrentThread.Name = "DungeonFinder";

                    var processName = Process.GetCurrentProcess().ProcessName;

                    if (Process.GetProcesses().Count(p => p.ProcessName == processName) > 1)
                    {
                        // attempt to send it a message to show the ui... someday.  should probably be
                        // done via named pipes and that's just more than we want to deal with.
                        _log.Debug("Duplicate application started - Highlander enabled.");
                        TextOutputWindow.ShowDialog("Dungeon Finder is already running, please open it from the system tray.", "OK");

                        return;
                    }
                }

                string appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string rootFolder = Path.Combine(appDataFolder, "Dungeon Finder");
#if DEBUG
                rootFolder = Environment.CurrentDirectory;
#endif
                string mainSettingsPath = Path.Combine(rootFolder, "settings.json");

                if (!File.Exists(mainSettingsPath))
                {
                    // generate a new settings file:
                    Settings newSettings = new Settings();

                    File.WriteAllText(mainSettingsPath, JsonConvert.SerializeObject(newSettings, Formatting.Indented));
                    newSettings = null;
                }

                SettingsFactoryHost.DefaultSettingsFactory = new SettingsFactory(mainSettingsPath);
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                if (CheckFirstRunImportFromVoK(settings))
                    // reload settings, since we imported
                    settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                if (EnsureShortcutsHaveGuid(settings))
                    settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                ServiceClientHost.Instance = new TurbineServiceClient(SettingsFactoryHost.DefaultSettingsFactory);

                // parse command line args, requires settings to be parsed first:
                ParseCommandLineOptions(args);

                AutoUpdater.CheckForUpdateEvent += AutoUpdater_CheckForUpdateEvent;

                if (settings.CheckForUpdates)
                    DoUpdateCheck();

                MainForm = new MainForm(args);

                if (_commandLineLaunchRequests != null)
                {
                    _silentRun = true; // hide tray icon
                    foreach (var launch in _commandLineLaunchRequests)
                    {
                        MainForm.Play(launch.Subscription, launch.Shortcut);
                        Thread.Sleep(400);
                    }
                    Thread.Sleep(1000);
                    Environment.Exit(0);
                }
                else
                {
                    _suppressUpdatePop = false;

                    // silent means don't show right away.
                    if (!_silentRun)
                    {
                        MainForm.Show();
                    }

                    LoginForm = new LoginForm();

                    using (TrayIcon = new TrayForm())
                    {
                        Application.Run();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Fatal exception in Dungeon Finder", ex);

#if DEBUG
                TextOutputWindow.ShowDialog("Untrapped error in DF\r\n" + ex.ToString(), "Close app");
#endif
            }
        }

        private static bool EnsureShortcutsHaveGuid(Settings settings)
        {
            // if a new settings file is created, it will not have a version so we can assume this is a first-run:
            if (settings.Version < 2)
            {
                var groupingShortcuts = settings.Groupings != null ? settings.Groupings.SelectMany(g => g.Members).Select(r => r.Shortcut).ToList() : null;

                // go through shortcuts in subscriptions and add guids
                foreach (var sa in settings.StoredAccounts)
                {
                    var shortcuts = sa.Subscriptions.Select(s => s.Shortcuts).FirstOrDefault();
                    foreach (var sc in shortcuts)
                    {
                        if (string.IsNullOrWhiteSpace(sc.Guid))
                            sc.Guid = Guid.NewGuid().ToString();

                        if (groupingShortcuts != null && groupingShortcuts.Count > 0)
                        {
                            var currentGroupShortcut = groupingShortcuts.Where(s => s.Server == sc.Server && sc.DisplayName == sc.DisplayName).FirstOrDefault();
                            if (currentGroupShortcut != null)
                            {
                                currentGroupShortcut.Guid = sc.Guid;
                            }
                        }
                    }
                }
                // upgrade version so it stops
                settings.Version = 2;
                SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            }
            // ensure groupings will not crash when loading a new config:
            if (settings.Version < 3)
            {
                var groups = settings.Groupings != null ? settings.Groupings.ToList() : null;
                if (groups != null)
                {
                    foreach (var group in groups)
                    {
                        if (string.IsNullOrWhiteSpace(group.Guid))
                            group.Guid = Guid.NewGuid().ToString();
                    }
                }
                else
                {
                    settings.Groupings = new List<LaunchGroup>();
                }

                // upgrade version so it stops
                settings.Version = 3;
                SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
                return true;
            }

            // For older settings files, we will prompt the user and ask to what path they want to upgrade:
            // 1, update the settings file to enable auto updates.
            // 2, do not update the settings file to perform auto updates, and upgrade the version without changing the settings file:
            if (settings.Version < 4)
            {
                // may always be false due to previous settings file defaults, so we will ask:
                if (settings.CheckForUpdates == false)
                {
                    using (TextOutputWindow changeUpdateSettingWindow = new TextOutputWindow("Would you like to enable the auto-update setting? This change will make the application check for a new version when starting. \r\n\r\nYou will be asked this dialog question, only once. \r\n\r\nIf you would like to change the setting in the future, the new setting is \"Check for updates on Start\" within the settings menu.", "Yes", "No"))
                    {
                        changeUpdateSettingWindow.ShowDialog();
                        if (changeUpdateSettingWindow.DialogResult == DialogResult.OK)
                        {
                            settings.CheckForUpdates = true;
                        }
                    }
                }

                settings.Version = 4;
                SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
                return true;
            }
            return false;
        }

        private static bool CheckFirstRunImportFromVoK(Settings settings)
        {
            bool importWasPerformed = false;
            var appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            // if a new settings file is created, it will not have a version so we can assume this is a first-run:
            if (settings.Version == null || settings.Version < 1)
            {
                // check if there is another config file under %AppData%/VoK
                string vokSettingsPath = Path.Combine(appDataFolder, "VoK\\settings.json");

                if (File.Exists(vokSettingsPath))
                {
                    // ask the user for input, do they want to import the settings or start fresh.
                    using (TextOutputWindow window = new TextOutputWindow("Import Launcher settings from VoK Launcher?", "Yes", "No"))
                    {
                        window.ShowDialog();

                        if (window.DialogResult == DialogResult.OK)
                        {
                            // load in the settings
                            SettingsFactoryHost.DefaultSettingsFactory.ImportSettingsFile(vokSettingsPath);
                            importWasPerformed = true;
                        }
                    }
                }

                // set the base to one, since this is the first run
                settings.Version = 1;
                SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            }
            return importWasPerformed;
        }

        public static void DoUpdateCheck()
        {
#if !DEBUG
            var myAssembly = Assembly.GetExecutingAssembly();
            AutoUpdater.Synchronous = true;
            AutoUpdater.Start("https://s3.amazonaws.com/downloads.dungeonhelper.com/public/DF/AutoUpdate.xml", myAssembly);
#else
            using (TextOutputWindow noUpdateCheck = new TextOutputWindow(true, "Ok", "Not performing auto-update check in debug mode, disable auto-update in settings to stop getting alerts."))
            {
                noUpdateCheck.ShowDialog();
            }
#endif
        }

        private static void AutoUpdater_CheckForUpdateEvent(UpdateInfoEventArgs args)
        {
#if !DEBUG
            if (!args.IsUpdateAvailable && !_suppressUpdatePop)
                MessageBox.Show("No Updates available.");
            else if (args.IsUpdateAvailable)
                AutoUpdater.ShowUpdateForm(args);
#endif
        }

        public static void ParseCommandLineOptions(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args)
                .WithParsed(RunOptions)
                .WithNotParsed(HandleParseError);
        }

        public static void RunOptions(CommandLineOptions opts)
        {
            // silent is a singleton, prevents the mainform from showing on first launch.
            if (opts?.Silent != 0x0)
            {
                _silentRun = true;
            }

            // group launch, ugh
            if (opts?.GroupId.Length > 0)
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                var group = settings.Groupings.Where(g => g.Guid == opts.GroupId).FirstOrDefault();
                if (group != null)
                {
                    // init list
                    _commandLineLaunchRequests = new List<ShortcutTag>();

                    foreach (var member in group.Members)
                    {
                        _commandLineLaunchRequests.Add(new ShortcutTag() { Shortcut = member.Shortcut, Subscription = member.Subscription });
                    }
                }
                else
                {
                    var msg = "Could not locate Group ID in settings file.";
                    _log.Error(msg + "Invalid Group ID in command line arguments.");
                    //ERROR_BAD_ARGUMENTS 160(0xA0)
                    MessageBox.Show(msg, "Invalid Group ID in command line arguments.", MessageBoxButtons.OK);
                    Environment.Exit(160);
                }
            }
            // lookup shortcut by id
            else if (opts?.ShortcutId.Length > 0)
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                var subscriptionMatch = settings?.StoredAccounts.SelectMany(r => r.Subscriptions.Where(r => r.Shortcuts.Any(a => a.Guid == opts.ShortcutId))).FirstOrDefault();
                var shortcut = subscriptionMatch.Shortcuts.Where(e => e.Guid == opts.ShortcutId).FirstOrDefault();

                if (shortcut != null && subscriptionMatch != null)
                {
                    // init list
                    _commandLineLaunchRequests = new List<ShortcutTag>();
                    _commandLineLaunchRequests.Add(new ShortcutTag() { Subscription = subscriptionMatch, Shortcut = shortcut });
                }
            }
            // subscription, servername are mutualy required
            else if (opts?.SubscriptionId.Length > 0 && opts?.ServerName.Length > 0)
            {
                // must have a sub and a server to launch
                // creat new launch if the sub exists in the settings file and we also have a shortcut in that subscription with that server name.
                CommandLineLaunchRequest newLaunchRequest = new CommandLineLaunchRequest()
                {
                    ServerName = opts.ServerName,
                    SubscriptionId = opts.SubscriptionId
                };

                // charactername is extra sugar ontop of subscription and servername, but mutually requires the other variables.
                if (opts?.CharacterName.Length > 0)
                {
                    newLaunchRequest.CharacterName = opts.CharacterName;
                }

                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                var idMatch = settings?.StoredAccounts.Where(sa => sa.Subscriptions.Any(s => s.Id == newLaunchRequest.SubscriptionId)).Select(sa => sa.Subscriptions).FirstOrDefault()[0] ?? null;

                // init list
                _commandLineLaunchRequests = new List<ShortcutTag>();

                // we can can try to pull the shortcut now
                if (idMatch != null)
                {
                    ShortcutTag newLaunchShortcutTag = new ShortcutTag() { Subscription = idMatch };

                    if (!string.IsNullOrEmpty(newLaunchRequest.CharacterName))
                    {
                        newLaunchShortcutTag.Shortcut = idMatch.Shortcuts.Where(s => s.Server == newLaunchRequest.ServerName && s.Character == newLaunchRequest.CharacterName).FirstOrDefault();
                    }
                    else
                    {
                        newLaunchShortcutTag.Shortcut = idMatch.Shortcuts.Where(s => s.Server == newLaunchRequest.ServerName).FirstOrDefault();
                    }

                    if (newLaunchShortcutTag.Shortcut != null)
                    {
                        _commandLineLaunchRequests.Add(newLaunchShortcutTag);
                    }
                }
            }
        }

        public static void HandleParseError(IEnumerable<Error> errors)
        {
            string msg = "Warning: please change your command line arguments, there were errors: ";
            foreach (var error in errors)
            {
                msg += error.ToString() + "\n\r";
            }
            TextOutputWindow.ShowDialog(msg, "Close warning");
            Console.WriteLine(msg);
        }

        public static void Exit()
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            if (TrayIcon != null)
                TrayIcon.Visible = false;
            System.Windows.Forms.Application.Exit();
        }
    }
}
