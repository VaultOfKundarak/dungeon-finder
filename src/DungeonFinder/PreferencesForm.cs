﻿using DungeonFinder.Common;
using DungeonFinder.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shortcut = DungeonFinder.Models.Shortcut;

namespace DungeonFinder
{
    /// <summary>
    ///  Preferences files allow custom settings for each game client that is launched.
    ///  This form sets the preferences file that will be used to launch the game.
    /// </summary>
    public partial class PreferencesForm : Form
    {
        private GameId currentGame;
        private string currentPreferences = string.Empty;
        private object preferencesContainer;

        public PreferencesForm(GameId game, Subscription sub)
        {
            InitializeComponent();
            currentGame = game;
            currentPreferences = sub.Preferences;
            preferencesContainer = sub;
            if (currentPreferences?.Length > 0)
            {
                txtBxPreferencesPath.Text = currentPreferences;
            }
        }

        public PreferencesForm(GameId game, Shortcut sc)
        {
            InitializeComponent();
            currentGame = game;
            currentPreferences = sc.Preferences;
            preferencesContainer = sc;
            if (currentPreferences?.Length > 0)
            {
                txtBxPreferencesPath.Text = currentPreferences;
            }
        }

        private string AttemptLocatingDDOPreferencesPath()
        {
            //This PC > Documents > Dungeons and Dragons Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); ;

            string ddoPath = Path.Combine(path, "Dungeons and Dragons Online");
            if (Directory.Exists(ddoPath))
            {
                return ddoPath;
            }

            return path;
        }

        private string AttemptLocatingLOTROPreferencesPath()
        {
            //This PC > Documents > The Lord of the Rings Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); ;

            string lotroPath = Path.Combine(path, "The Lord of the Rings Online");
            if (Directory.Exists(lotroPath))
            {
                return lotroPath;
            }

            return path;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Windows Explorer Dialogue Box Stuff
            var startingFolder = "";

            if (currentGame == GameId.DDO)
            {
                // Last set preferences file folder or "My Documents" or "Documents\Dungeons and Dragons Online":
                startingFolder = !string.IsNullOrWhiteSpace(currentPreferences) ? new FileInfo(currentPreferences).Directory.FullName : AttemptLocatingDDOPreferencesPath();
            }
            else if (currentGame == GameId.LOTRO)
            {
                // Last set preferences file folder or "My Documents" or "Documents\The Lord of the Rings Online":
                startingFolder = !string.IsNullOrWhiteSpace(currentPreferences) ? new FileInfo(currentPreferences).Directory.FullName : AttemptLocatingLOTROPreferencesPath();
            }

            using (var browse = new System.Windows.Forms.OpenFileDialog() { InitialDirectory = startingFolder, Filter = "Preferences Files (*.ini)|*.ini|All files (*.*)|*.*" })
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                if (settings == null) return;

                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.FileName))
                {
                    var selectedFile = Path.GetFullPath(browse.FileName);
                    currentPreferences = selectedFile;
                    txtBxPreferencesPath.Text = selectedFile;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SavePreferences())
                this.Close();
        }

        private bool SavePreferences()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings == null)
            {
                lblError.Text = "Trouble with Settings file, please restart Dungeon Finder.";
                return false;
            }

            currentPreferences = txtBxPreferencesPath.Text;

            // If there is a selected preferences file, make sure it exists or cancel save.
            // Ensures that if the text box gets changed, we don't save a non-usable string
            if (!string.IsNullOrWhiteSpace(currentPreferences) && !File.Exists(currentPreferences))
            {
                // Error
                lblError.Text = "File does not exist, select the preferences file again.";
                return false;
            }

            if (preferencesContainer is Subscription)
            {
                (preferencesContainer as Subscription).Preferences = currentPreferences;
            }
            else if (preferencesContainer is Shortcut)
            {
                (preferencesContainer as Shortcut).Preferences = currentPreferences;
            }

            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            return true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtBxPreferencesPath.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
