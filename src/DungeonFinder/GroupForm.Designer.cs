﻿
namespace DungeonFinder
{
    partial class GroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupForm));
            this.lstBxShortcutNames = new System.Windows.Forms.ListBox();
            this.lstBxSelectedShortcuts = new System.Windows.Forms.ListBox();
            this.lblShortcutNames = new System.Windows.Forms.Label();
            this.lblSelected = new System.Windows.Forms.Label();
            this.btnGroupAdd = new System.Windows.Forms.Button();
            this.btnGroupRemove = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.txtBxName = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstBxShortcutNames
            // 
            this.lstBxShortcutNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBxShortcutNames.FormattingEnabled = true;
            this.lstBxShortcutNames.ItemHeight = 15;
            this.lstBxShortcutNames.Location = new System.Drawing.Point(12, 50);
            this.lstBxShortcutNames.Name = "lstBxShortcutNames";
            this.lstBxShortcutNames.Size = new System.Drawing.Size(245, 124);
            this.lstBxShortcutNames.TabIndex = 0;
            // 
            // lstBxSelectedShortcuts
            // 
            this.lstBxSelectedShortcuts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBxSelectedShortcuts.FormattingEnabled = true;
            this.lstBxSelectedShortcuts.ItemHeight = 15;
            this.lstBxSelectedShortcuts.Location = new System.Drawing.Point(12, 234);
            this.lstBxSelectedShortcuts.Name = "lstBxSelectedShortcuts";
            this.lstBxSelectedShortcuts.Size = new System.Drawing.Size(245, 124);
            this.lstBxSelectedShortcuts.TabIndex = 1;
            // 
            // lblShortcutNames
            // 
            this.lblShortcutNames.AutoSize = true;
            this.lblShortcutNames.Location = new System.Drawing.Point(12, 32);
            this.lblShortcutNames.Name = "lblShortcutNames";
            this.lblShortcutNames.Size = new System.Drawing.Size(111, 15);
            this.lblShortcutNames.TabIndex = 2;
            this.lblShortcutNames.Text = "Available Shortcuts:";
            // 
            // lblSelected
            // 
            this.lblSelected.AutoSize = true;
            this.lblSelected.Location = new System.Drawing.Point(12, 216);
            this.lblSelected.Name = "lblSelected";
            this.lblSelected.Size = new System.Drawing.Size(109, 15);
            this.lblSelected.TabIndex = 3;
            this.lblSelected.Text = "Shortcuts in Group:";
            // 
            // btnGroupAdd
            // 
            this.btnGroupAdd.Location = new System.Drawing.Point(12, 180);
            this.btnGroupAdd.Name = "btnGroupAdd";
            this.btnGroupAdd.Size = new System.Drawing.Size(245, 23);
            this.btnGroupAdd.TabIndex = 4;
            this.btnGroupAdd.Text = "Add above Shortcut to Group";
            this.btnGroupAdd.UseVisualStyleBackColor = true;
            this.btnGroupAdd.Click += new System.EventHandler(this.btnGroupAdd_Click);
            // 
            // btnGroupRemove
            // 
            this.btnGroupRemove.Location = new System.Drawing.Point(12, 364);
            this.btnGroupRemove.Name = "btnGroupRemove";
            this.btnGroupRemove.Size = new System.Drawing.Size(245, 23);
            this.btnGroupRemove.TabIndex = 5;
            this.btnGroupRemove.Text = "Remove above Shortcut from Group";
            this.btnGroupRemove.UseVisualStyleBackColor = true;
            this.btnGroupRemove.Click += new System.EventHandler(this.btnGroupRemove_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 433);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(103, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save Grouping";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(194, 433);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(63, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(78, 15);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Group Name:";
            // 
            // txtBxName
            // 
            this.txtBxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxName.Location = new System.Drawing.Point(96, 6);
            this.txtBxName.Name = "txtBxName";
            this.txtBxName.Size = new System.Drawing.Size(161, 23);
            this.txtBxName.TabIndex = 9;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(12, 390);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 15);
            this.lblStatus.TabIndex = 10;
            // 
            // GroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(269, 468);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.txtBxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnGroupRemove);
            this.Controls.Add(this.btnGroupAdd);
            this.Controls.Add(this.lblSelected);
            this.Controls.Add(this.lblShortcutNames);
            this.Controls.Add(this.lstBxSelectedShortcuts);
            this.Controls.Add(this.lstBxShortcutNames);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(285, 403);
            this.Name = "GroupForm";
            this.Text = "Group Shortcut Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstBxShortcutNames;
        private System.Windows.Forms.ListBox lstBxSelectedShortcuts;
        private System.Windows.Forms.Label lblShortcutNames;
        private System.Windows.Forms.Label lblSelected;
        private System.Windows.Forms.Button btnGroupAdd;
        private System.Windows.Forms.Button btnGroupRemove;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtBxName;
        private System.Windows.Forms.Label lblStatus;
    }
}