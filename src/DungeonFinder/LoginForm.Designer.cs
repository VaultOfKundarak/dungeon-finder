﻿
namespace DungeonFinder
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.pAddAccount = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAccountLogout = new System.Windows.Forms.Button();
            this.btnAccountLogin = new System.Windows.Forms.Button();
            this.lSubscription = new System.Windows.Forms.Label();
            this.cbPlaySubscriptions = new System.Windows.Forms.ComboBox();
            this.lGame = new System.Windows.Forms.Label();
            this.rbAccountLotro = new System.Windows.Forms.RadioButton();
            this.rbAccountDdo = new System.Windows.Forms.RadioButton();
            this.lServer = new System.Windows.Forms.Label();
            this.cbPlayServers = new System.Windows.Forms.ComboBox();
            this.tbAccountCharacterName = new System.Windows.Forms.TextBox();
            this.lCharacterName = new System.Windows.Forms.Label();
            this.tbAccountPassword = new System.Windows.Forms.TextBox();
            this.lPassword = new System.Windows.Forms.Label();
            this.tbAccountUsername = new System.Windows.Forms.TextBox();
            this.lUsername = new System.Windows.Forms.Label();
            this.pAddAccount.SuspendLayout();
            this.SuspendLayout();
            // 
            // pAddAccount
            // 
            this.pAddAccount.Controls.Add(this.btnClear);
            this.pAddAccount.Controls.Add(this.btnAccountLogout);
            this.pAddAccount.Controls.Add(this.btnAccountLogin);
            this.pAddAccount.Controls.Add(this.lSubscription);
            this.pAddAccount.Controls.Add(this.cbPlaySubscriptions);
            this.pAddAccount.Controls.Add(this.lGame);
            this.pAddAccount.Controls.Add(this.rbAccountLotro);
            this.pAddAccount.Controls.Add(this.rbAccountDdo);
            this.pAddAccount.Controls.Add(this.lServer);
            this.pAddAccount.Controls.Add(this.cbPlayServers);
            this.pAddAccount.Controls.Add(this.tbAccountCharacterName);
            this.pAddAccount.Controls.Add(this.lCharacterName);
            this.pAddAccount.Controls.Add(this.tbAccountPassword);
            this.pAddAccount.Controls.Add(this.lPassword);
            this.pAddAccount.Controls.Add(this.tbAccountUsername);
            this.pAddAccount.Controls.Add(this.lUsername);
            this.pAddAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pAddAccount.Location = new System.Drawing.Point(0, 0);
            this.pAddAccount.Margin = new System.Windows.Forms.Padding(0);
            this.pAddAccount.Name = "pAddAccount";
            this.pAddAccount.Size = new System.Drawing.Size(363, 240);
            this.pAddAccount.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnClear.Location = new System.Drawing.Point(9, 191);
            this.btnClear.Margin = new System.Windows.Forms.Padding(0);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(80, 38);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAccountLogout
            // 
            this.btnAccountLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnAccountLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccountLogout.Enabled = false;
            this.btnAccountLogout.FlatAppearance.BorderSize = 0;
            this.btnAccountLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAccountLogout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnAccountLogout.Location = new System.Drawing.Point(274, 191);
            this.btnAccountLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnAccountLogout.Name = "btnAccountLogout";
            this.btnAccountLogout.Size = new System.Drawing.Size(80, 38);
            this.btnAccountLogout.TabIndex = 15;
            this.btnAccountLogout.Text = "Logout";
            this.btnAccountLogout.UseVisualStyleBackColor = false;
            this.btnAccountLogout.Click += new System.EventHandler(this.btnAccountLogout_Click);
            // 
            // btnAccountLogin
            // 
            this.btnAccountLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnAccountLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccountLogin.FlatAppearance.BorderSize = 0;
            this.btnAccountLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAccountLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnAccountLogin.Location = new System.Drawing.Point(194, 191);
            this.btnAccountLogin.Margin = new System.Windows.Forms.Padding(0);
            this.btnAccountLogin.Name = "btnAccountLogin";
            this.btnAccountLogin.Size = new System.Drawing.Size(80, 38);
            this.btnAccountLogin.TabIndex = 14;
            this.btnAccountLogin.Text = "Login";
            this.btnAccountLogin.UseVisualStyleBackColor = false;
            this.btnAccountLogin.Click += new System.EventHandler(this.btnAccountLogin_Click);
            // 
            // lSubscription
            // 
            this.lSubscription.AutoSize = true;
            this.lSubscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lSubscription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lSubscription.Location = new System.Drawing.Point(3, 157);
            this.lSubscription.Name = "lSubscription";
            this.lSubscription.Size = new System.Drawing.Size(86, 17);
            this.lSubscription.TabIndex = 12;
            this.lSubscription.Text = "Subscription";
            this.lSubscription.Visible = false;
            // 
            // cbPlaySubscriptions
            // 
            this.cbPlaySubscriptions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlaySubscriptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbPlaySubscriptions.FormattingEnabled = true;
            this.cbPlaySubscriptions.Location = new System.Drawing.Point(122, 152);
            this.cbPlaySubscriptions.Name = "cbPlaySubscriptions";
            this.cbPlaySubscriptions.Size = new System.Drawing.Size(222, 24);
            this.cbPlaySubscriptions.TabIndex = 13;
            this.cbPlaySubscriptions.Visible = false;
            // 
            // lGame
            // 
            this.lGame.AutoSize = true;
            this.lGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lGame.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lGame.Location = new System.Drawing.Point(3, 10);
            this.lGame.Name = "lGame";
            this.lGame.Size = new System.Drawing.Size(46, 17);
            this.lGame.TabIndex = 1;
            this.lGame.Text = "Game";
            // 
            // rbAccountLotro
            // 
            this.rbAccountLotro.AutoSize = true;
            this.rbAccountLotro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbAccountLotro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.rbAccountLotro.Location = new System.Drawing.Point(185, 8);
            this.rbAccountLotro.Name = "rbAccountLotro";
            this.rbAccountLotro.Size = new System.Drawing.Size(75, 21);
            this.rbAccountLotro.TabIndex = 3;
            this.rbAccountLotro.TabStop = true;
            this.rbAccountLotro.Text = "LOTRO";
            this.rbAccountLotro.UseVisualStyleBackColor = true;
            this.rbAccountLotro.CheckedChanged += new System.EventHandler(this.rbAccountLotro_CheckedChanged);
            // 
            // rbAccountDdo
            // 
            this.rbAccountDdo.AutoSize = true;
            this.rbAccountDdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbAccountDdo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.rbAccountDdo.Location = new System.Drawing.Point(122, 8);
            this.rbAccountDdo.Name = "rbAccountDdo";
            this.rbAccountDdo.Size = new System.Drawing.Size(57, 21);
            this.rbAccountDdo.TabIndex = 2;
            this.rbAccountDdo.TabStop = true;
            this.rbAccountDdo.Text = "DDO";
            this.rbAccountDdo.UseVisualStyleBackColor = true;
            this.rbAccountDdo.CheckedChanged += new System.EventHandler(this.rbAccountDdo_CheckedChanged);
            // 
            // lServer
            // 
            this.lServer.AutoSize = true;
            this.lServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lServer.Location = new System.Drawing.Point(3, 96);
            this.lServer.Name = "lServer";
            this.lServer.Size = new System.Drawing.Size(50, 17);
            this.lServer.TabIndex = 8;
            this.lServer.Text = "Server";
            // 
            // cbPlayServers
            // 
            this.cbPlayServers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlayServers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbPlayServers.FormattingEnabled = true;
            this.cbPlayServers.Location = new System.Drawing.Point(122, 93);
            this.cbPlayServers.Name = "cbPlayServers";
            this.cbPlayServers.Size = new System.Drawing.Size(222, 24);
            this.cbPlayServers.TabIndex = 9;
            // 
            // tbAccountCharacterName
            // 
            this.tbAccountCharacterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbAccountCharacterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountCharacterName.Location = new System.Drawing.Point(122, 123);
            this.tbAccountCharacterName.Name = "tbAccountCharacterName";
            this.tbAccountCharacterName.Size = new System.Drawing.Size(222, 23);
            this.tbAccountCharacterName.TabIndex = 11;
            // 
            // lCharacterName
            // 
            this.lCharacterName.AutoSize = true;
            this.lCharacterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lCharacterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lCharacterName.Location = new System.Drawing.Point(3, 126);
            this.lCharacterName.Name = "lCharacterName";
            this.lCharacterName.Size = new System.Drawing.Size(111, 17);
            this.lCharacterName.TabIndex = 10;
            this.lCharacterName.Text = "Character Name";
            // 
            // tbAccountPassword
            // 
            this.tbAccountPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbAccountPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountPassword.Location = new System.Drawing.Point(122, 64);
            this.tbAccountPassword.Name = "tbAccountPassword";
            this.tbAccountPassword.PasswordChar = '*';
            this.tbAccountPassword.Size = new System.Drawing.Size(222, 23);
            this.tbAccountPassword.TabIndex = 7;
            // 
            // lPassword
            // 
            this.lPassword.AutoSize = true;
            this.lPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lPassword.Location = new System.Drawing.Point(3, 67);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(69, 17);
            this.lPassword.TabIndex = 6;
            this.lPassword.Text = "Password";
            // 
            // tbAccountUsername
            // 
            this.tbAccountUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbAccountUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountUsername.Location = new System.Drawing.Point(122, 35);
            this.tbAccountUsername.Name = "tbAccountUsername";
            this.tbAccountUsername.Size = new System.Drawing.Size(222, 23);
            this.tbAccountUsername.TabIndex = 5;
            // 
            // lUsername
            // 
            this.lUsername.AutoSize = true;
            this.lUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lUsername.Location = new System.Drawing.Point(3, 37);
            this.lUsername.Name = "lUsername";
            this.lUsername.Size = new System.Drawing.Size(73, 17);
            this.lUsername.TabIndex = 4;
            this.lUsername.Text = "Username";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(363, 240);
            this.Controls.Add(this.pAddAccount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Text = "Manual Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
            this.pAddAccount.ResumeLayout(false);
            this.pAddAccount.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pAddAccount;
        private System.Windows.Forms.TextBox tbAccountCharacterName;
        private System.Windows.Forms.Label lCharacterName;
        private System.Windows.Forms.TextBox tbAccountPassword;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.TextBox tbAccountUsername;
        private System.Windows.Forms.Label lUsername;
        private System.Windows.Forms.Label lServer;
        private System.Windows.Forms.ComboBox cbPlayServers;
        private System.Windows.Forms.RadioButton rbAccountDdo;
        private System.Windows.Forms.RadioButton rbAccountLotro;
        private System.Windows.Forms.Label lGame;
        private System.Windows.Forms.Label lSubscription;
        private System.Windows.Forms.ComboBox cbPlaySubscriptions;
        private System.Windows.Forms.Button btnAccountLogin;
        private System.Windows.Forms.Button btnAccountLogout;
        private System.Windows.Forms.Button btnClear;
    }
}