﻿using log4net;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using System.Xml;
using DungeonFinder.ServiceClients;
using DungeonFinder.Common;
using DungeonFinder.Models;
using Shortcut = DungeonFinder.Models.Shortcut;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace DungeonFinder
{
    public class Launcher
    {
        private static Dictionary<GameId, bool> _patchingDict { get; } = new Dictionary<GameId, bool>();

        private static Dictionary<GameId, object> _patchingMutexes { get; } = new Dictionary<GameId, object>();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static Launcher()
        {
            // prepopulate the dictionaries
            foreach (GameId game in Enum.GetValues(typeof(GameId)))
            {
                _patchingMutexes.Add(game, new object());
                _patchingDict.Add(game, false);
            }
        }

        public static string GetClientArguments(Subscription sub, Shortcut shortcut, LauncherConfiguration launcherData, GameInformation gameInfo, string clientFolder, string loginTicket, string language, GameServer server)
        {
            try
            {
                string serverNameNoFlags = Extensions.StripServerFlags(shortcut.Server);
                GameWorld world = gameInfo.Worlds.First(w => w.Name.Contains(serverNameNoFlags));

                // get a login ticket
                string loginTicketUrl = launcherData.Settings["WorldQueue.LoginQueue.URL"];
                string loginTicketParams = launcherData.Settings["WorldQueue.TakeANumber.Parameters"];

                string firstQueue = server.QueueUrls[0];
                loginTicketParams = string.Format(loginTicketParams,
                    sub.Id,
                    HttpUtility.UrlEncode(loginTicket),
                    firstQueue);

                if (!string.IsNullOrEmpty(firstQueue) && firstQueue != ";")
                {
                    string loginTicketResult;
                    try
                    {
                        loginTicketResult = HttpPost(loginTicketUrl, loginTicketParams);
                    }
                    catch (Exception ex)
                    {
                        Exception newEx = new Exception("Error getting login ticket.", ex);
                        throw newEx;
                    }

                    XmlDocument loginTicketDoc = new XmlDocument();
                    loginTicketDoc.LoadXml(loginTicketResult);

                    var hresult = loginTicketDoc["Result"]["HResult"];
                    if (hresult != null && hresult.InnerText != "0x00000000")
                    {
                        // login queue failed.
                        _log.Warn("unable to access login queue.  hresult: " + hresult.ToString());
                        return null;
                    }

                    long queueNumber = Convert.ToInt64(loginTicketDoc["Result"]["QueueNumber"].InnerText.Substring(2), 16);
                    long nowServing = Convert.ToInt64(loginTicketDoc["Result"]["NowServingNumber"].InnerText.Substring(2), 16);

                    while (nowServing < queueNumber)
                    {
                        nowServing = GetNowServingNumber(world.StatusServerUrl);
                        // only sleep when it's not our number:
                        if (nowServing != queueNumber)
                            Thread.Sleep(1000);
                    }
                }

                string loginServers = server.LoginServers[0];

                string template = launcherData.Settings["GameClient.WIN32.ArgTemplate"];
                string authServerUrl = launcherData.Settings["GameClient.Arg.authserverurl"];
                string supportUrl = TryGetSetting(launcherData, "GameClient.Arg.supporturl");
                string bugUrl = TryGetSetting(launcherData, "GameClient.Arg.bugurl");
                string ticketLifetime = TryGetSetting(launcherData, "GameClient.Arg.glsticketlifetime");
                string serviceUrl = TryGetSetting(launcherData, "GameClient.Arg.supportserviceurl");

                string clientParams = template.Replace("{SUBSCRIPTION}", sub.Id);
                clientParams = clientParams.Replace("{LOGIN}", loginServers);
                clientParams = clientParams.Replace("{GLS}", loginTicket);
                clientParams = clientParams.Replace("{CHAT}", world.ChatServerUrl);
                clientParams = clientParams.Replace("{LANG}", language);
                clientParams = clientParams.Replace("{AUTHSERVERURL}", authServerUrl);
                clientParams = clientParams.Replace("{GLSTICKETLIFETIME}", ticketLifetime);

                if (!string.IsNullOrWhiteSpace(supportUrl))
                    clientParams = clientParams.Replace("{SUPPORTURL}", supportUrl);
                else
                    clientParams = clientParams.Replace("{SUPPORTURL}", "http://google.com");

                if (!string.IsNullOrWhiteSpace(serviceUrl))
                    clientParams = clientParams.Replace("{SUPPORTSERVICEURL}", serviceUrl);
                else
                    clientParams = clientParams.Replace("{SUPPORTSERVICEURL}", "http://google.com");

                if (!string.IsNullOrWhiteSpace(bugUrl))
                    clientParams = clientParams.Replace("{BUGURL}", bugUrl);
                else
                    clientParams = clientParams.Replace("{BUGURL}", "http://google.com");

                if (!string.IsNullOrWhiteSpace(shortcut.Character))
                    clientParams += " -u " + shortcut.Character;

                if (!string.IsNullOrWhiteSpace(shortcut.Preferences))
                    clientParams += " --prefs \"" + shortcut.Preferences + "\"";
                else if (!string.IsNullOrWhiteSpace(sub.Preferences))
                    clientParams += " --prefs \"" + sub.Preferences + "\"";

                return clientParams;
            }
            catch (Exception ex)
            {
                _log.Error("Error in GetClientArguments", ex);
            }

            return null;
        }

        private static string TryGetSetting(LauncherConfiguration launcherConfig, string setting)
        {
            if (launcherConfig?.Settings?.ContainsKey(setting) ?? false)
                return launcherConfig.Settings[setting];

            return null;
        }

        /// <summary>
        /// total ripoff of Mogwai's code here, with permission.  he said it's MIT licensed in any case.
        /// </summary>
        public static void Launch(Subscription sub, Shortcut shortcut, LauncherConfiguration launcherData, GameInformation gameInfo, string clientFolder, string loginTicket, string language, bool use64bit = false, GameServer selectedServer = null)
        {
            if (IsPatching(sub.Game))
            {
                // abort
                MessageBox.Show("Currently patching, please try again after patching is complete.", "Dungeon Finder");
                return;
            }

            var clientParams = GetClientArguments(sub, shortcut, launcherData, gameInfo, clientFolder, loginTicket, language, selectedServer);
            if (string.IsNullOrWhiteSpace(clientParams))
            {
                MessageBox.Show("Unable to launch the game, and the error has been logged. Check the server status.", "Dungeon Finder");
                return;
            }

            var clientFilename = launcherData.Settings["GameClient.WIN32.Filename"];

            if (use64bit)
                clientFilename = "x64\\" + launcherData.Settings["GameClient.WIN64.Filename"];

            //Directory.SetCurrentDirectory(clientFolder);

            Process client = new Process();
            client.StartInfo.FileName = clientFilename;
            client.StartInfo.Arguments = clientParams;
            client.StartInfo.UseShellExecute = true;
            client.StartInfo.CreateNoWindow = false;
            client.StartInfo.ErrorDialog = true;
            client.StartInfo.WorkingDirectory = clientFolder;
            client.Start();
            client.PriorityBoostEnabled = true;
        }

        public static bool ProccessExists(GameId gameId)
        {
            string procName = GetProcessName(gameId);
            var running = Process.GetProcessesByName(procName);
            if (running?.Length > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// thread-blocking method that will ensure the game is patched.  spawns a new process to do the patching if one isn't running.
        /// </summary>
        public static void PatchClient(string clientPath, GameId gameId, string patchServer, Action<char> stdOutCallback, Action finishedProcessingCallback)
        {
            if (IsPatching(gameId))
            {
                // wait for it to finish, then return
                while (IsPatching(gameId))
                    Thread.Sleep(500);

                return;
            }

            // lock the mutex
            lock (_patchingMutexes[gameId])
            {
                _patchingDict[gameId] = true;
                _log.Debug($"Patching starting for {gameId} at {clientPath}...");

                try
                {
                    Directory.SetCurrentDirectory(clientPath);
                    // 32 bit only for patchclient.dll pinvoke:
                    string sysFolder = Environment.GetFolderPath(Environment.SpecialFolder.SystemX86); // % WinDir%\SysWOW64\
                    string runDllPath = Path.Combine(sysFolder, "rundll32.exe");
                    string dllPath = Path.Combine(clientPath, "patchclient.dll");

                    bool highRes = File.Exists(Path.Combine(clientPath, "client_highres.dat"));

                    // check for currently running processes - we can't patch if the game is running

                    if (ProccessExists(gameId))
                    {
                        string message = "Cannot Patch - client is currently running.";
                        message.ToCharArray().ToList().ForEach(c => stdOutCallback?.Invoke(c));
                        finishedProcessingCallback?.Invoke();
                        _patchingDict[gameId] = false;
                        return;
                    }

                    // Set a time-out value of one hour
                    int timeOut = 1000 * 60 * 60;

                    // Create a new process info structure.
                    ProcessStartInfo pInfo = new ProcessStartInfo();

                    // Set file name to open.
                    pInfo.FileName = sysFolder + @"\rundll32.exe";
                    // Patch(int, int, string, int);
                    string patchString = patchServer + " --productcode " + Enum.GetName(typeof(GameId), gameId) + " --language English";

                    if (highRes)
                        patchString += " --highres";

                    pInfo.Arguments = $"\"{dllPath}\", Patch 0 0 {patchString}";

                    pInfo.UseShellExecute = false;
                    pInfo.RedirectStandardOutput = true;
                    pInfo.RedirectStandardError = true;

                    // Start the process.
                    Process p = Process.Start(pInfo);

                    int charCode;

                    string outputBuffer = string.Empty;

                    while (!p.StandardOutput.EndOfStream && stdOutCallback != null)
                    {
                        charCode = p.StandardOutput.Read();
                        stdOutCallback((char)charCode);
                        outputBuffer += (char)charCode;
                    }

                    // Wait for the process to exit or time out.
                    p.WaitForExit(timeOut);

                    // Check to see if the process is still running.
                    if (p.HasExited == false)
                    {
                        // Process is still running.
                        // Test to see if the process is hung up.
                        if (p.Responding)
                            // Process was responding; close the main window.
                            p.CloseMainWindow();
                        else
                            // Process was not responding; force the process to close.
                            p.Kill();
                    }

                    // checks the last part of the output for an error:
                    if (outputBuffer.Length > 0 && outputBuffer.Substring(outputBuffer.Length - 32).Contains("0x80"))
                    {
                        "\r\n\r\n\tTo fix this patching problem, please close and re-open this application (Dungeon Finder) as an administrator."
                            .ToCharArray().ToList().ForEach(c => stdOutCallback?.Invoke(c));
                    }

                    outputBuffer = string.Empty;

                    _log.Debug($"Patching complete for {gameId} at {clientPath}");

                }
                catch (ThreadAbortException)
                {
                    // we got killed.  let it die silently
                    _log.Debug($"Patching thread for {gameId} was aborted.");
                }
                catch (Exception ex)
                {
                    _log.Error($"Exception while patching {gameId}", ex);
                }
                finally
                {
                    _patchingDict[gameId] = false;
                }

            }

            finishedProcessingCallback?.Invoke();
        }

        /// <summary>
        /// This method will always be retried from a loop operation proceeding.
        /// </summary>
        /// <param name="statusUrl">URL to a servers status API endpoint</param>
        /// <returns>Current queue number</returns>
        private static long GetNowServingNumber(string statusUrl)
        {
            XmlDocument doc = new XmlDocument();

            // retry up to 2 times, if there is an exception:
            long? nowServingQueueNumber = null;
            int attempts = 0;

            while (attempts < 2 && nowServingQueueNumber == null)
            {
                try
                {
                    doc.Load(statusUrl);
                    nowServingQueueNumber = Convert.ToInt64(doc["Status"]["nowservingqueuenumber"].InnerText.Substring(2), 16);
                }
                // xml parsing errors:
                catch (XmlException xe)
                {
                    // no idea why they require input for certain endpoints:
                    if (xe.Message.Contains("The 'br' start tag") ||
                        xe.Message.Contains("Data at the root level is invalid"))
                    {
                        // failure we can't recover from
                        break;
                    }
                    // this is the generic catch all error
                    if (xe.Message == "Root element is missing.")
                    {
                        // this can be a quirky message that doesn't always pop up, so just retry
                        // there was an error on the page or request, (or server error, or server is experiencing a high load, etc?)
                        // swallowing error here to allow for retry
                    }
                }
                // https://learn.microsoft.com/en-us/dotnet/api/system.net.http.httprequesterror?view=net-8.0
                // http layer error
                catch (System.Net.Http.HttpRequestException he)
                {
                    switch (he.HttpRequestError)
                    {
                        case System.Net.Http.HttpRequestError.SecureConnectionError:
                        case System.Net.Http.HttpRequestError.HttpProtocolError:
                        case System.Net.Http.HttpRequestError.ExtendedConnectNotSupported:
                        case System.Net.Http.HttpRequestError.NameResolutionError:
                        default:
                            throw new Exception("Networking error, cannot proceed with status request.");

                        // these might work subsquently? we will break to continue the retry loop
                        case System.Net.Http.HttpRequestError.ResponseEnded:
                        case System.Net.Http.HttpRequestError.Unknown:
                        case System.Net.Http.HttpRequestError.ProxyTunnelError:
                        case System.Net.Http.HttpRequestError.InvalidResponse:
                        case System.Net.Http.HttpRequestError.ConfigurationLimitExceeded:
                        case System.Net.Http.HttpRequestError.ConnectionError:
                        case System.Net.Http.HttpRequestError.VersionNegotiationError:
                        case System.Net.Http.HttpRequestError.UserAuthenticationError:
                            break;
                    }
                }
                catch (Exception)
                {
                    // swallowing error here (not doing anything with it)
                }

                // if we haven't found the value yet, add delay to let server catch up before we make a subsequent request
                if (nowServingQueueNumber == null)
                    Thread.Sleep(300);

                ++attempts;
            }

            if (nowServingQueueNumber == null)
                throw new Exception("Error getting Server status.");

            return nowServingQueueNumber.Value;
        }

        private static string HttpPost(string uri, string body)
        {
            RestClient client = new RestClient(uri);
            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Content-Length", body.Length.ToString());

            byte[] bytes = Encoding.ASCII.GetBytes(body);
            // request.AddParameter(new Parameter())
            request.AddParameter("throwaway", body, ParameterType.RequestBody);
            var response = client.Execute(request);
            return response.Content;
        }

        public static void WaitForPatchingComplete(GameId game)
        {
            while (IsPatching(game))
                Thread.Sleep(100);
        }


        public static bool IsPatching(GameId game)
        {
            if (!_patchingDict.ContainsKey(game))
                return false;

            return _patchingDict[game];
        }

        public static string GetProcessName(GameId game)
        {
            MemberInfo memberInfo = typeof(GameId).GetMember(game.ToString()).FirstOrDefault();
            ProcessNameAttribute attr = (ProcessNameAttribute)memberInfo?.GetCustomAttribute(typeof(ProcessNameAttribute));
            return attr?.Name;
        }

    }
}
