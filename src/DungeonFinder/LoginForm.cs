﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DungeonFinder.Common;
using DungeonFinder.ServiceClients;
using DungeonFinder.Properties;
using DungeonFinder.Models;
using Shortcut = DungeonFinder.Models.Shortcut;
using System.Text.RegularExpressions;

namespace DungeonFinder
{
    public partial class LoginForm : Form
    {

        [DllImport("user32.dll")]
        internal static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        private readonly Dictionary<GameId, LauncherConfiguration> _launcherInfo = new Dictionary<GameId, LauncherConfiguration>();
        private readonly Dictionary<GameId, GameInformation> _gameInfo = new Dictionary<GameId, GameInformation>();

        private GameId _gameType = GameId.DDO;
        private UserSession _loginSession;

        private bool windowExpanded = false;

        public LoginForm()
        {
            InitializeComponent();

            rbAccountDdo.Checked = true; // default to DDO option

            cbPlaySubscriptions.DisplayMember = "DisplayName";

            SendMessage(tbAccountCharacterName.Handle, 0x1501, 1, "(Optional)");

            LoadGameInfo();

            // shrink window and move buttons by 30
            Size = new Size(Size.Width, Size.Height - 30);
            btnClear.Location = new Point(btnClear.Location.X, btnClear.Location.Y - 30);
            btnAccountLogin.Location = new Point(btnAccountLogin.Location.X, btnAccountLogin.Location.Y - 30);
            btnAccountLogout.Location = new Point(btnAccountLogout.Location.X, btnAccountLogout.Location.Y - 30);
        }

        private void PopulateServerDropdown()
        {
            // get the servers for the game
            var gameInfo = ServiceClientHost.Instance.GetGameInformation(_gameType);

            cbPlayServers.Items.Clear(); // start with empty list

            if (gameInfo != null && gameInfo.Servers.Count > 0)
            {
                foreach (var server in gameInfo.Servers)
                    cbPlayServers.Items.Add(server.Name);
            }
            else
            {
                // if we have server info from the api, use that, or else we use static resources
                string[] gameServers = Extensions.GetServerNamesFromResources(_gameType);

                if (gameServers?.Length > 0)
                {
                    foreach (var server in gameServers)
                        cbPlayServers.Items.Add(server);
                }
                else
                {
                    // add a blank entry to select
                    cbPlayServers.Items.Add(string.Empty);
                }
            }
            cbPlayServers.SelectedIndex = 0; // Default
        }

        private void LoadGameInfo()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.AppendLine("DDO");
                var ddoInfo = ServiceClientHost.Instance.GetGameInformation(GameId.DDO, true);
                if (ddoInfo != null)
                {
                    _gameInfo[GameId.DDO] = ddoInfo;
                    _launcherInfo[GameId.DDO] = ServiceClientHost.Instance.GetLauncherConfiguration(ddoInfo);
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            sb.AppendLine();

            try
            {
                sb.AppendLine("LOTRO");
                var lotroInfo = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO, true);
                if (lotroInfo != null)
                {
                    _gameInfo[GameId.LOTRO] = lotroInfo;
                    _launcherInfo[GameId.LOTRO] = ServiceClientHost.Instance.GetLauncherConfiguration(lotroInfo);
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            //lblServerStatus.Text = sb.ToString();
        }

        private string GetClientGamePath(GameId gameId, Settings settings, LauncherConfiguration launcherInfo)
        {
            string clientPath = string.Empty;

            // Validate the GameId
            if (Enum.IsDefined(typeof(GameId), gameId))
            {
                // if Game is valid, get path from settings:
                if (gameId == GameId.DDO)
                    clientPath = settings?.DdoPath != string.Empty ? settings?.DdoPath : string.Empty;
                else if (gameId == GameId.LOTRO)
                    clientPath = settings?.LotroPath != string.Empty ? settings?.LotroPath : string.Empty;

                // Validate the Path
                if (string.IsNullOrEmpty(clientPath))
                {
                    // bail out, we don't have a folder set to launch from.
                    MessageBox.Show($"Your settings path is empty, please set the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }
                if (!Directory.Exists(clientPath))
                {
                    // bail out, The folder is incorrect.
                    MessageBox.Show($"The path in your settings is invalid or does not exist, please change the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }

                var clientExePath = Path.Combine(clientPath, launcherInfo.Settings["GameClient.WIN32.Filename"]);

                // check if the client executable exists
                if (!File.Exists(clientExePath))
                {
                    //MessageBox.Show("There was an issue locating the client executable.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    //return null;
                }

                var clientPatchDllPath = Path.Combine(clientPath, @"patchclient.dll");

                // check if the patchclient.dll file exists
                if (!File.Exists(clientPatchDllPath))
                {
                    MessageBox.Show("There was an issue locating the client patcher.", $"Cannot Patch game {gameId}.", MessageBoxButtons.OK);
                    // disable auto-patching?
                    // return null;
                    settings.PatchBeforeLaunch = false;
                }
            }
            else
            {
                // Invalid GameId // bail out, The folder is incorrect.
                MessageBox.Show("Could not determine a valid Game to launch.", "Please update your subscription information, under your account.", MessageBoxButtons.OK);
                return null;
            }
            return clientPath;
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                ResetForm(); // clear the form on close
                Hide();
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }

            base.OnFormClosing(e);
        }

        private void PopulateSubscriptionDropdown()
        {
            // expand window and move buttons by 30
            Size = new Size(Size.Width, Size.Height + 30);
            btnClear.Location = new Point(btnClear.Location.X, btnClear.Location.Y + 30);
            btnAccountLogin.Location = new Point(btnAccountLogin.Location.X, btnAccountLogin.Location.Y + 30);
            btnAccountLogout.Location = new Point(btnAccountLogout.Location.X, btnAccountLogout.Location.Y + 30);
            windowExpanded = true;

            if (_loginSession == null)
                return;

            var subscriptionList = _loginSession.Subscriptions;
            cbPlaySubscriptions.Items.Clear(); // start with empty list

            var relevantSubs = subscriptionList.Where(s => s.Game == _gameType).ToList();
            relevantSubs.ForEach(s => cbPlaySubscriptions.Items.Add(
                new Subscription() { DisplayName = s.Description, Game = s.Game, Id = s.Id, Name = s.Description }));

            if (cbPlaySubscriptions.Items.Count > 1)
            {
                cbPlaySubscriptions.Visible = true;
                lSubscription.Visible = true;
            }
        }

        private void LaunchGame(Subscription sub, UserSession _loginSession, Settings settings)
        {
            Shortcut shortcut = new Shortcut();
            shortcut.Server = cbPlayServers.Text;
            shortcut.Character = tbAccountCharacterName.Text;
            shortcut.DisplayName = tbAccountUsername.Text;

            LaunchRequest newLaunch = new LaunchRequest() { Subscription = sub, Shortcut = shortcut };

            try
            {
                newLaunch.LoginTicket = _loginSession.Ticket;
            }
            catch
            {
                MessageBox.Show($"There was an error connecting to the auth service. Please check your internet connection.", $"Error authenticating {_loginSession.Username}.", MessageBoxButtons.OK);
                return;
            }

            if (!_launcherInfo.ContainsKey(sub.Game))
            {
                MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", $"ERROR: Could not connect to the {sub.Game} servers.", MessageBoxButtons.OK);
                return;
            }

            newLaunch.LauncherConfiguration = _launcherInfo[sub.Game];
            newLaunch.GameInformation = _gameInfo[sub.Game];
            // must come after settings:
            newLaunch.ClientFolder = GetClientGamePath(sub.Game, settings, newLaunch.LauncherConfiguration);
            newLaunch.Use64Bit = settings.Use64BitClients;

            // find the server from game info and add it to launch
            string serverNameNoFlags = Extensions.StripServerFlags(shortcut.Server);
            newLaunch.Server = _gameInfo[sub.Game].Servers.Where(s => s.Name.Contains(serverNameNoFlags)).FirstOrDefault();

            // This must be valid, to launch the game. Errors are show in a MessageBox, from within GetClientGamePath
            if (newLaunch.ClientFolder == null)
                return;

            if (settings.PatchBeforeLaunch)
            {
                if (!Launcher.IsPatching(newLaunch.GameInformation.Game) &&
                    !Launcher.ProccessExists(newLaunch.GameInformation.Game))
                {
                    if (Program.TrayIcon != null)
                    {
                        Program.TrayIcon.ToggleTrayIcon();
                        if (settings.EnableNotifications)
                            Program.TrayIcon.PopupNotification($"Patching {newLaunch.GameInformation.Game}",
                                "Dungeon Finder Auto Patcher");
                    }

                    Launcher.PatchClient(newLaunch.ClientFolder, newLaunch.GameInformation.Game,
                        newLaunch.GameInformation.PatchServer, null, null);

                    if (Program.TrayIcon != null)
                    {
                        Program.TrayIcon.ToggleTrayIcon();
                        if (settings.EnableNotifications)
                            Program.TrayIcon.PopupNotification($"Done patching {newLaunch.GameInformation.Game}",
                                "Dungeon Finder Auto Patcher");
                    }
                }
            }

            newLaunch.Language = "EN";

            LaunchQueue.LaunchClient(newLaunch);
        }

        private void rbAccountDdo_CheckedChanged(object sender, EventArgs e)
        {
            _gameType = GameId.DDO;
            PopulateServerDropdown();
        }

        private void rbAccountLotro_CheckedChanged(object sender, EventArgs e)
        {
            _gameType = GameId.LOTRO;
            PopulateServerDropdown();
        }

        private void btnAccountLogout_Click(object sender, EventArgs e)
        {
            ResetSession();
        }

        private void ResetForm()
        {
            ResetSession();

            // clear auth fields and character name
            tbAccountUsername.Text = "";
            tbAccountPassword.Text = "";
            tbAccountCharacterName.Text = "";
        }

        private void ResetSession()
        {
            _loginSession = null;
            btnAccountLogout.Enabled = false; // disable Logout button

            // make auth fields writable
            tbAccountUsername.Enabled = true;
            tbAccountPassword.Enabled = true;

            // hide subscription section
            lSubscription.Visible = false;
            cbPlaySubscriptions.Visible = false;
            cbPlaySubscriptions.Items.Clear();

            if (windowExpanded == true) // only resize window if last action expanded it
            {
                // shrink window and move buttons by 30
                Size = new Size(Size.Width, Size.Height - 30);
                btnClear.Location = new Point(btnClear.Location.X, btnClear.Location.Y - 30);
                btnAccountLogin.Location = new Point(btnAccountLogin.Location.X, btnAccountLogin.Location.Y - 30);
                btnAccountLogout.Location = new Point(btnAccountLogout.Location.X, btnAccountLogout.Location.Y - 30);
                windowExpanded = false;
            }

            btnAccountLogin.Text = "Login";
        }

        private void SetFormLoggedIn()
        {
            // make auth fields read only
            tbAccountUsername.Enabled = false;
            tbAccountPassword.Enabled = false;
            btnAccountLogout.Enabled = true;
            btnAccountLogin.Text = "Play";
        }

        private void btnAccountLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbAccountUsername.Text) || string.IsNullOrWhiteSpace(tbAccountPassword.Text))
                return;

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (_loginSession == null)
            {
                // start testing - eventually
                _loginSession = ServiceClientHost.Instance.Authenticate(tbAccountUsername.Text, tbAccountPassword.Text, out string message, _gameType);

                if (_loginSession == null || message == "Username and/or Password incorrect.")
                {
                    MessageBox.Show("Username and/or Password is incorrect.",
                        "Please enter your credentials and try again.", MessageBoxButtons.OK);
                    return;
                }

                SetFormLoggedIn();
                PopulateSubscriptionDropdown();
            }

            if (cbPlaySubscriptions.Items.Count == 1)
            {
                LaunchGame((Subscription)cbPlaySubscriptions.Items[0], _loginSession, settings);
                return;
            }

            if (cbPlaySubscriptions.SelectedItem != null)
            {
                var sub = (Subscription)cbPlaySubscriptions.SelectedItem;
                if (sub != null)
                    LaunchGame(sub, _loginSession, settings);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}
