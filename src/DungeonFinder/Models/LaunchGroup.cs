﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Models
{
    // A group contains shortcuts that have character names.
    // Grouped shortcuts must not share a subscription.
    public class LaunchGroup
    {
        [JsonProperty("guid")]
        public string Guid { get; set; }

        public string Name { get; set; }

        [JsonProperty("showInTray")]
        public bool ShowInTray { get; set; } = true;

        public List<ShortcutTag> Members { get; set; }
    }
}
