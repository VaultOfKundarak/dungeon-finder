﻿namespace DungeonFinder.Models
{
    public class CommandLineLaunchRequest
    {
        public string SubscriptionId { get; set; }
        public string ServerName { get; set; }
        public string CharacterName { get; set; }
    }
}