﻿using DungeonFinder.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Models
{
    public class Subscription
    {
        [JsonProperty("game")]
        public GameId Game { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("defaultServer")]
        public string DefaultServer { get; set; }

        [JsonProperty("preferences")]
        public string Preferences { get; set; } = string.Empty;

        [JsonProperty("shortcuts")]
        public List<Shortcut> Shortcuts { get; set; } = new List<Shortcut>();

        public override string ToString()
        {
            var newToString = string.Empty;

            if (DisplayName?.Length > 0)
                newToString = DisplayName;
            else
                newToString = Name.Substring(0,8) + Id[0].ToString() + Id[1].ToString() + Id[2].ToString() + Id[3].ToString();

            if (newToString?.Length > 0)
                return newToString;
            else
                return base.ToString();
        }
    }
}
