﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Models
{
    public class ShortcutTag
    {
        public Subscription Subscription { get; set; }
        public Shortcut Shortcut { get; set; }
        public override string ToString()
        {
            string displayString = "";
            StoredAccount matchingAccount = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings().StoredAccounts.FirstOrDefault(n => n.Subscriptions.Any(o => o.Id == Subscription.Id)); // used for shortcut display
            
            if (matchingAccount.DisplayName != matchingAccount.Username) // if custom display name
            {
                displayString += matchingAccount.DisplayName;

                if (matchingAccount.Subscriptions.Where(s => s.Game == Subscription.Game).Count() > 1) // if more than one sub
                {
                    displayString += ": " + Subscription.ToString();
                }

                displayString += " - ";
            }
            else
            {
                displayString += Subscription.ToString() + " - ";
            }

            displayString += Shortcut.ToString();

            return displayString;
        }
    }
}
