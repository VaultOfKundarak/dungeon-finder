﻿using System.Collections.Generic;

namespace DungeonFinder.Models
{
    public class GameServer
    {
        public string Name { get; set; }
        public bool PVP { get; set; }
        public bool PVPWorldEvent { get; set; }
        public bool Hardcore { get; set; }
        public bool WorldFull { get; set; }
        public bool Legendary { get; set; }
        public double WaitHint { get; set; }
        public int NowServingQueueNumber { get; set; }
        public int LastAssignedQueueNumber { get; set; }

        public bool Online
        {
            get
            {
                return WorldFull != true && LastAssignedQueueNumber > 0x0;
            }
        }

        public List<string> QueueUrls { get; set; }
        public List<string> LoginServers { get; set; }

        public GameServer() { }
    }
}