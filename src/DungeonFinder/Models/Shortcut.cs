﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Models
{
    public class Shortcut
    {
        [JsonProperty("guid")]
        public string Guid { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("character")]
        public string Character { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("preferences")]
        public string Preferences { get; set; } = string.Empty;

        [JsonProperty("showInTray")]
        public bool ShowInTray { get; set; } = true;

        public string GetDisplayName()
        {
            if (!string.IsNullOrWhiteSpace(DisplayName))
                return DisplayName;

            return Server + (!string.IsNullOrWhiteSpace(Character) ? " - " + Character : "");
        }

        public override string ToString()
        {
            var newToString = GetDisplayName();

            if (newToString?.Length > 0)
                return newToString;
            else
                return base.ToString();
        }
    }
}
