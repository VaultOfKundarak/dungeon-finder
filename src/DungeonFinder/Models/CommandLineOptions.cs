﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Models
{
    public class CommandLineOptions
    {
        [Option(shortName: 'v', longName: "silent", Required = false, HelpText = "Do not show main UI when staring the application.", Default = 0)]
        public int Silent { get; set; }

        [Option(shortName: 'u', longName: "subscriptionid", Required = false, HelpText = "Subscription id string.", Default = "")]
        public string SubscriptionId { get; set; }

        [Option(shortName: 's', longName: "server", Required = false, HelpText = "Server name string.", Default = "")]
        public string ServerName { get; set; }

        [Option(shortName: 'c', longName: "character", Required = false, HelpText = "Character name string.", Default = "")]
        public string CharacterName { get; set; }

        [Option(shortName: 'i', longName: "shortcutid", Required = false, HelpText = "Shortcut id string.", Default = "")]
        public string ShortcutId { get; set; }

        [Option(shortName: 'g', longName: "groupid", Required = false, HelpText = "Group id string.", Default = "")]
        public string GroupId { get; set; }
    }
}
