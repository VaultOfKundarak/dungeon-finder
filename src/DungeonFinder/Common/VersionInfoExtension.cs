﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DungeonFinder.Common
{
    public static class VersionInfoExtension
    {
        public static Dictionary<string, string> GetVersionInfo()
        {
            // get all dll and Exe files within the folderPath
            // return the file name and version as a list
            Dictionary<string, string> versions = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(AssemblyDirectory))
            {
                List<string> versionFiles = Directory.EnumerateFiles(AssemblyDirectory, "*.exe", SearchOption.AllDirectories).ToList();
                versionFiles.AddRange(Directory.EnumerateFiles(AssemblyDirectory, "*.dll", SearchOption.AllDirectories));

                foreach (var v in versionFiles)
                {
                    var versionFile = Path.GetFileName(v);
                    if (versions.ContainsKey(versionFile))
                    {
                        versionFile = v;
                    }
                    versions.Add(versionFile, FileVersionInfo.GetVersionInfo(v).FileVersion);
                }
            }

            return versions;
        }

        private static string AssemblyDirectory
        {
            get
            {
                string location = Assembly.GetExecutingAssembly().Location;
                UriBuilder uri = new UriBuilder(location);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}
