﻿using DungeonFinder.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Common
{
    public class GamePath
    {
        [JsonProperty("gameId")]
        [JsonConverter(typeof(StringEnumConverter))]
        public GameId GameId { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }
    }
}
