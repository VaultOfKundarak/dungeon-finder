﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonFinder.Common
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ProcessNameAttribute : Attribute
    {
        public ProcessNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
