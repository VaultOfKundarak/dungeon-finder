﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.Common
{
    public static class LogHelper
    {
        public static string LogFilePath { get; set; }

        public static string SessionTag { get; set; } = string.Empty;
    }
}
