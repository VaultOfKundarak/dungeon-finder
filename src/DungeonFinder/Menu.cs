﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using DungeonFinder.Properties;
using System.Collections.Generic;
using DungeonFinder.Common;
using DungeonFinder.Models;
using Shortcut = DungeonFinder.Models.Shortcut;

namespace DungeonFinder
{
    internal static class Menu
    {
        private static Action _onChange;

        public static ContextMenuStrip Initialize(Action onChange)
        {
            DrawMenu();
            _onChange = onChange;
            SettingsFactoryHost.DefaultSettingsFactory.OnChange += DefaultSettingsFactory_OnChange;

            return MainMenu;
        }

        private static void DefaultSettingsFactory_OnChange(object sender, EventArgs e)
        {
            DrawMenu();
            _onChange();
        }

        public static ContextMenuStrip MainMenu { get; private set; }

        private static void DrawMenu()
        {
            MainMenu = new ContextMenuStrip();

            ToolStripMenuItem trayItem;

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            var allShortcuts = new List<Tuple<Subscription, Shortcut>>();

            var allGroups = new List<LaunchGroup>();

            foreach (var account in settings.StoredAccounts)
            {
                foreach (var sub in account.Subscriptions)
                {
                    foreach (var shortcut in sub.Shortcuts)
                    {
                        if (shortcut.ShowInTray == false)
                        {
                            continue; // skip this iteration
                        }
                        
                        string text = shortcut.DisplayName;

                        if (string.IsNullOrWhiteSpace(text))
                        {
                            text = string.Empty;

                            if (settings.StoredAccounts.Count > 1)
                            {
                                if (account.DisplayName != account.Username) // if custom display name
                                {
                                    text += account.DisplayName;

                                    if (account.Subscriptions.Count > 1)
                                    {
                                        text += ": " + sub.DisplayName + " - ";
                                    }
                                    else
                                    {
                                        text += " - ";
                                    }
                                }
                                else
                                {
                                    text += sub.DisplayName + " - ";
                                }
                            }

                            text += shortcut.Server;

                            if (!string.IsNullOrWhiteSpace(shortcut.Character))
                                text += " - " + shortcut.Character;
                        }

                        allShortcuts.Add(Tuple.Create(sub, new Shortcut() { Character = shortcut.Character, Server = shortcut.Server, DisplayName = text, Guid = shortcut.Guid, Preferences = shortcut.Preferences }));
                    }
                }
            }

            allShortcuts = allShortcuts.OrderBy(tup => tup.Item2.GetDisplayName()).ToList();

            bool anyTrayShortcuts = false;

            foreach (var tuple in allShortcuts)
            {
                // add shortcuts to tray menu:
                if (tuple.Item2.ShowInTray)
                {
                    anyTrayShortcuts = true;
                    Image icon = null;
                    if (tuple.Item1.Game == GameId.DDO)
                        icon = Resources.ddo_16x16;
                    else if (tuple.Item1.Game == GameId.LOTRO)
                        icon = Resources.lotro_16x16;
                    trayItem = new ToolStripMenuItem { Text = tuple.Item2.DisplayName, Image = icon };
                    trayItem.Click += LaunchShortcut_Click;
                    trayItem.Tag = Tuple.Create(tuple.Item1, tuple.Item2);
                    MainMenu.Items.Add(trayItem);
                }
            }

            if (settings != null && settings.Groupings != null && settings?.Groupings.Count > 0)
            {
                allGroups = settings?.Groupings;

                allGroups = allGroups.OrderBy(launchGroup => launchGroup.Name).ToList();

                foreach (var group in allGroups)
                {
                    // add groups to tray menu:
                    if (group.ShowInTray)
                    {
                        anyTrayShortcuts = true;
                        Image icon = Resources.group_36x36;
                        trayItem = new ToolStripMenuItem { Text = "Group: " + group.Name, Image = icon };
                        trayItem.Click += LaunchGroup_Click;
                        trayItem.Tag = group;
                        MainMenu.Items.Add(trayItem);
                    }
                }
            }

            // only add extra separator if we have a shortcut:
            if (anyTrayShortcuts)
            {
                MainMenu.Items.Add(new ToolStripSeparator());
            }

            // Open the Login Form
            trayItem = new ToolStripMenuItem { Text = @"Manual Login" };
            trayItem.Click += LoginForm_Click;
            MainMenu.Items.Add(trayItem);

            MainMenu.Items.Add(new ToolStripSeparator());

            // Open the Main Form
            trayItem = new ToolStripMenuItem { Text = @"Open Main UI" };
            trayItem.Click += MainForm_Click;
            MainMenu.Items.Add(trayItem);

            //MainMenu.Items.Add(new ToolStripSeparator());

            // Exit
            trayItem = new ToolStripMenuItem { Text = @"Exit" };
            trayItem.Click += Exit_Click;
            MainMenu.Items.Add(trayItem);
        }

        private static void LaunchGroup_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tray = (ToolStripMenuItem)sender;

            if (tray.Tag != null)
            {
                var launch = tray.Tag as LaunchGroup;
                foreach (var member in launch.Members)
                {
                    Thread t = new Thread(() => Program.MainForm.Play(member.Subscription, member.Shortcut));
                    t.Name = "TrayPlay";
                    t.IsBackground = true;
                    t.Start();
                }
            }
        }

        private static void LaunchShortcut_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tray = (ToolStripMenuItem)sender;

            if (tray.Tag != null)
            {
                var tup = tray.Tag as Tuple<Subscription, Shortcut>;
                Thread t = new Thread(() => Program.MainForm.Play(tup?.Item1, tup?.Item2));
                t.Name = "TrayPlay";
                t.IsBackground = true;
                t.Start();
            }
        }

        private static void MainForm_Click(object sender, EventArgs e)
        {
            Program.MainForm.WindowState = FormWindowState.Normal;
            Program.MainForm.Show();
            Program.MainForm.BringToFront();
        }

        private static void LoginForm_Click(object sender, EventArgs e)
        {
            Program.LoginForm.WindowState = FormWindowState.Normal;
            Program.LoginForm.Show();
            Program.LoginForm.BringToFront();
        }

        private static void Exit_Click(object sender, EventArgs e)
        {
            Program.Exit();
        }
    }
}