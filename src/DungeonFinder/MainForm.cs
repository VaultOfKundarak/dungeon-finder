﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using Microsoft.Win32;
using Newtonsoft.Json;
using DungeonFinder.Common;
using DungeonFinder.ServiceClients;
using DungeonFinder.Properties;
using DungeonFinder.Models;
using Shortcut = DungeonFinder.Models.Shortcut;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Windows.Documents;

namespace DungeonFinder
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "RabidSquirrel.")]
    public partial class MainForm : Form
    {
        enum NavTab
        {
            Home,
            Accounts,
            Settings
        }

        enum SettingsSubTab
        {
            Settings
        }

        private NavTab _currentTab;
        private SettingsSubTab _settingsSubTab;

        private readonly Color _navActive = Color.FromArgb(16, 79, 153); // #104F99
        private readonly Color _navInactive = Color.FromArgb(15, 36, 79); // #???
        private readonly Color _turquoise = Color.FromArgb(51, 252, 254); // #2FD5E7
        private const string RegistryApplicationName = "Vault of Kundarak";

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        private readonly ToolTip addAccountsToolTip = new ToolTip();

        // default to past time
        private static DateTime _lastPatchCheckTime = new DateTime();

        private bool discardSettingsChanges = false;
        private string settingsSnapshot;
        private bool suppressPopupNotification = false; // Suppress "still running" popup notification after the first message

        // Click/drag support for reorganizing accounts
        private Point initialMouseLocation;
        private StoredAccount draggedAccount;
        private bool isOrganizing = false;
        private Control draggedControl;

        // cached character groupings:
        private List<LaunchGroup> CachedGroups;

        [DllImport("user32.dll")]
        internal static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        internal static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        public bool IsActive { get; private set; }

        private readonly Dictionary<GameId, LauncherConfiguration> _launcherInfo = new Dictionary<GameId, LauncherConfiguration>();
        private readonly Dictionary<GameId, GameInformation> _gameInfo = new Dictionary<GameId, GameInformation>();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        string[] args1;

        public MainForm(string[] args)
        {
            InitializeComponent();

            args1 = args;

            pAccounts.Dock = DockStyle.Fill;
            pHome.Dock = DockStyle.Fill;
            pSettings.Dock = DockStyle.Fill;
            pUserSettings.Dock = DockStyle.Fill;
            pEditAccount.Dock = DockStyle.Fill;
            pMainMenu.Dock = DockStyle.Fill;
            scAccountDetails.Dock = DockStyle.Fill;
            _currentTab = NavTab.Accounts;
            lblServerStatus.Text = "";

            new ToolTip().SetToolTip(btnAccounts, "Launch & Accounts");
            new ToolTip().SetToolTip(btnHome, "Contact Links");
            new ToolTip().SetToolTip(btnSettings, "Settings");
            new ToolTip().SetToolTip(btnFindDdo, "Browse for Folder Path");
            new ToolTip().SetToolTip(btnFindLotro, "Browse for Folder Path");
            new ToolTip().SetToolTip(btnPlay, "Launch this account's game client with the selected server");
            new ToolTip().SetToolTip(btnDDOPatchNow, "Manually Patch the DDO Client");
            new ToolTip().SetToolTip(btnLOTROPatchNow, "Manually Patch the LOTRO Client");
            new ToolTip().SetToolTip(btnWeb, "Open the Vault of Kundarak Item Web site");
            new ToolTip().SetToolTip(btnDiscord, "Join Vault of Kundarak discord server");
            new ToolTip().SetToolTip(btnMyDDO, "Open the Vault of Kundarak MyDDO website");
            new ToolTip().SetToolTip(btnYouTube, "Open the Vault of Kundarak YouTube channel");
            new ToolTip().SetToolTip(btnAddShortcut, "Add a new shortcut with the selected server and character name.");

            PopulateUserSettings();
            PopulateGroups();
            RenderMenus();
            LoadGameInfo();
            PopulateAccounts();

            // clean up the size, as we design with things way expanded
            // to make them easier to see &  find
            Width = 880;
            Height = 500;

            SendMessage(tbAddShortcutCharacter.Handle, 0x1501, 1, "(character name)");
            SendMessage(tbShortcutDisplayName.Handle, 0x1501, 1, "(display name)");
        }

        private void RenderMenus()
        {
            pUnsavedChanges.Visible = false;
            if (CheckForSettingsChanges())
                return;
            pHome.Visible = false;
            pMessages.Visible = false;
            pAddAccount.Visible = false;
            pAccounts.Visible = false;
            pSettings.Visible = false;
            pUserSettings.Visible = false;
            scAccountDetails.Visible = false;
            btnAccounts.BackColor = _navInactive;
            btnHome.BackColor = _navInactive;
            btnSettings.BackColor = _navInactive;
            btnAddShortcut.Text = "Create Shortcut";

            switch (_currentTab)
            {
                case NavTab.Accounts:
                    // left side
                    pAccounts.Visible = true;
                    btnAccounts.BackColor = _navActive;
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                    if (settings?.StoredAccounts?.Count > 0)
                    {
                        var firstAccount = settings?.StoredAccounts.FirstOrDefault();
                        if (firstAccount?.Subscriptions.Count > 0)
                        {
                            SelectSubscription(firstAccount.Subscriptions[0]);
                        }
                    }

                    break;
                case NavTab.Home:
                    pHome.Visible = true;
                    btnHome.BackColor = _navActive;
                    break;
                case NavTab.Settings:
                    pSettings.Visible = true;
                    btnSettings.BackColor = _navActive;

                    switch (_settingsSubTab)
                    {
                        case SettingsSubTab.Settings:
                            settingsSnapshot = GetSettingsSnapshot(); // Load initial settings snapshot
                            pUserSettings.Visible = true;
                            break;
                    }

                    break;
            }
        }

        private void ToggleTrayIcon()
        {
            // change the icon:
            if (Program.TrayIcon != null)
                Program.TrayIcon.ToggleTrayIcon();
        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            _currentTab = NavTab.Accounts;
            RenderMenus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (!settings.MinimizeToTray)
            {
                Program.Exit();
            }
            else
            {
                NotifyStillRunning();
                Hide();
            }
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            NotifyStillRunning();

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (!settings.MinimizeToTray)
            {
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                Hide();
            }
        }

        private void NotifyStillRunning()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings.EnableNotifications && !suppressPopupNotification)
            {
                Program.TrayIcon.PopupNotification("Dungeon Finder is still running in the System Tray", "Dungeon Finder");
                suppressPopupNotification = true;
            }
        }

        private static Thread _autoPatchThread = null;
        private static CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public static void LaunchPatchThread()
        {
            if (_autoPatchThread != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource();
            }
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (settings.CheckForPatchMinutes == null)
                return;

            _autoPatchThread = new Thread(RunPatch);
            _autoPatchThread.IsBackground = true;
            _autoPatchThread.Name = "Patcher";
            _autoPatchThread.Start();
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async static void RunPatch()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            bool run = true;

            var token = _cancellationTokenSource.Token;

            // wait 10 seconds after startup
            token.WaitHandle.WaitOne(10000);

            while (run && !token.IsCancellationRequested)
            {
                try
                {
                    // always get a fresh copy of the settings
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                    // check for this first in case they turned it off since we last slept
                    if (settings.CheckForPatchMinutes == null)
                        return;

                    var _checkForPatchMilliseconds = settings.CheckForPatchMinutes.Value * 60000;

                    if (DateTime.UtcNow >= _lastPatchCheckTime.AddMilliseconds(_checkForPatchMilliseconds))
                    {
                        _lastPatchCheckTime = DateTime.UtcNow;

                        if (!string.IsNullOrWhiteSpace(settings.DdoPath) && !Launcher.ProccessExists(GameId.DDO))
                        {
                            if (Program.TrayIcon != null)
                            {
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("Checking for DDO patches...", "Dungeon Finder Auto Patcher");
                                System.Windows.Forms.Application.DoEvents();
                            }

                            var ddoInfo = ServiceClientHost.Instance.GetGameInformation(GameId.DDO);
                            Launcher.PatchClient(settings.DdoPath, GameId.DDO, ddoInfo.PatchServer, null, null);

                            if (Program.TrayIcon != null)
                            {
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("DDO Patch complete.", "Dungeon Finder Auto Patcher");
                                System.Windows.Forms.Application.DoEvents();
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(settings.LotroPath) && !Launcher.ProccessExists(GameId.LOTRO))
                        {
                            if (Program.TrayIcon != null)
                            {
                                Program.TrayIcon.ToggleTrayIcon(true);
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("Checking for LOTRO patches...", "Dungeon Finder Auto Patcher");
                            }

                            var lotroInfo = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO);
                            Launcher.PatchClient(settings.DdoPath, GameId.LOTRO, lotroInfo.PatchServer, null, null);

                            if (Program.TrayIcon != null)
                            {
                                Program.TrayIcon.ToggleTrayIcon(false);
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("LOTRO Patch complete", "Dungeon Finder Auto Patcher");
                            }
                        }
                    }

                    Thread.Sleep(_checkForPatchMilliseconds);
                }
                catch (ThreadAbortException)
                {
                    run = false;
                    throw;
                }
                catch
                {
                    // swallow
                }
            }
        }

        //private void WaitForIconToLoad()
        //{
        //    Thread t = new Thread(() =>
        //    {
        //        while (Program.TrayIcon == null)
        //            Thread.Sleep(500);
        //    });
        //    t.IsBackground = true;
        //    t.Start();
        //}

        private void scHeader_Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, null);
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, null);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pMessages.Dock = DockStyle.Fill;
            pAddAccount.Dock = DockStyle.Fill;
            IsActive = true;
            LaunchPatchThread();
        }

        private void PopulateGroups()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (settings.Groupings != null)
            {
                CachedGroups = settings.Groupings;
            }
            else
            {
                CachedGroups = new List<LaunchGroup>();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
            IsActive = false;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }

            base.OnFormClosing(e);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            _currentTab = NavTab.Home;
            RenderMenus();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            // hide message panel, if visible
            _currentTab = NavTab.Settings;
            PopulateUserSettings();
            RenderMenus();
            pUserSettings.Visible = true;
        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            // if the settings file does not contain a valid path to "A" launcher, then we need to warn the user to fix settings:
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (string.IsNullOrWhiteSpace(settings.DdoPath) && string.IsNullOrWhiteSpace(settings.LotroPath))
            {
                var message = "Please verify the path to your game is valid within the settings menu.";
                // show dialog with instructions to fix game path
                ShowMessage(message);
                return;
            }

            scAccountDetails.Visible = false;
            pEditAccount.Visible = false;
            pAddAccount.Visible = true;

            pAddAccount.BringToFront();
        }

        private void ShowMessage(string messageText)
        {
            scAccountDetails.Visible = false;
            pEditAccount.Visible = false;

            lblMessage.Text = messageText;
            lblMessage.Visible = true;
            pMessages.Visible = true;

            pMessages.BringToFront();
        }

        // To make this Game agnostic, we may have to try both DDO and LOTRO auth servers:
        private void btnAccountSave_Click(object sender, EventArgs e)
        {
            if (tbAccountUsername?.Text.Length > 0 && tbAccountPassword?.Text.Length > 0)
            {
                if (tbAccountDisplayName.Text.Length <= 0)
                    tbAccountDisplayName.Text = tbAccountUsername.Text;

                var gameIds = new List<GameId>() { GameId.DDO, GameId.LOTRO };
                // bad random
                RandomNumberGenerator.Shuffle(CollectionsMarshal.AsSpan(gameIds));

                var userProfile = ServiceClientHost.Instance.Authenticate(tbAccountUsername.Text, tbAccountPassword.Text, out string message, gameIds[0]); // try to authenticate with first auth server

                // if we are not using the correct credentials, error out here:
                if (message != null && (message == "Username and/or Password incorrect." || message.StartsWith("BIS:")))
                {
                    ShowMessage(message);
                    return;
                }

                // need a try catch here to try both DDO and LOTRO
                if (userProfile == null)
                {
                    userProfile = ServiceClientHost.Instance.Authenticate(tbAccountUsername.Text, tbAccountPassword.Text, out message, gameIds[1]); // try to authenticate with second auth server
                    if (userProfile == null)
                    {
                        ShowMessage(message);
                        // check the error and report somehow?
                        return;
                    }
                }

                // If the subscriptions come back, pop-up subscription panel with pre-populated drop-downs to select the correct data.
                if (userProfile?.Subscriptions.Count > 0)
                {
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                    var existingAccount = settings.StoredAccounts.FirstOrDefault(s => s.Username == tbAccountUsername.Text);

                    if (existingAccount != null)
                        settings.StoredAccounts.Remove(existingAccount);

                    StoredAccount sa = new StoredAccount();
                    sa.DecryptedPassword = tbAccountPassword.Text;
                    sa.Username = tbAccountUsername.Text;
                    sa.DisplayName = tbAccountDisplayName.Text;

                    sa.Subscriptions = userProfile.Subscriptions.Select(s => new Subscription() { DisplayName = s.Description, Game = s.Game, Id = s.Id, Name = s.Description }).ToList();

                    settings.StoredAccounts.Add(sa);
                    SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);

                    tbAccountDisplayName.Text = "";
                    tbAccountPassword.Text = "";
                    tbAccountUsername.Text = "";
                    pAddAccount.Visible = false;

                    PopulateAccounts();
                }
                else
                {
                    // report errors?
                    ShowMessage("No subscriptions found.");
                }
            }
        }

        private void PopulateAccounts()
        {
            var vScrollValue = pAccounts.VerticalScroll.Value;
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            pAccounts.Controls.Clear();

            Button addAccount = new Button();
            addAccount.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            addAccount.FlatStyle = FlatStyle.Flat;
            addAccount.FlatAppearance.BorderSize = 0;
            addAccount.Margin = new Padding(0);
            addAccount.Padding = new Padding(0);
            addAccount.Location = new Point(0, 0);
            addAccount.Text = "Add Account";
            addAccount.Font = new Font("Neo Sans", 12, FontStyle.Bold, GraphicsUnit.Point);
            addAccount.Size = new Size(220, 50);
            addAccount.ForeColor = Color.White;
            addAccount.TextAlign = ContentAlignment.MiddleCenter;
            addAccount.Cursor = Cursors.Hand;
            addAccount.UseVisualStyleBackColor = false;
            addAccount.TextImageRelation = TextImageRelation.ImageBeforeText;
            addAccount.Click += btnAddAccount_Click;
            addAccountsToolTip.SetToolTip(addAccount, "Add Account");
            addAccount.Paint += paintBorderInsideButton;
            pAccounts.Controls.Add(addAccount);

            GenerateAccountElements(settings.StoredAccounts);
            pAccounts.VerticalScroll.Value = vScrollValue;
            pAccounts.PerformLayout();
        }

        private void GenerateAccountElements(List<StoredAccount> accounts)
        {
            int subVerticalOffset = 0;

            if (accounts == null || accounts.Count < 1)
            {
                Label info = new Label();
                info.Name = "lblInfo";
                info.Text = "No accounts configured.";
                info.Font = new Font("Neo Sans", 10, FontStyle.Bold, GraphicsUnit.Point);
                info.Size = new Size(220, 30);
                info.ForeColor = Color.White;
                info.Location = new Point(0, 0);
                info.FlatStyle = FlatStyle.Flat;
                info.TextAlign = ContentAlignment.TopLeft;
                info.Margin = new Padding(0);
                pAccounts.Controls.Add(info);
                return;
            }

            if (isOrganizing)
            {
                Panel organizingBuffer = new Panel()
                {
                    Location = new Point(0, 0),
                    Size = new Size(210, 0),
                    BackColor = pAccounts.BackColor,
                    Tag = "divider",
                    Cursor = Cursors.Hand,
                    Margin = new Padding(0, 0, 0, 0)
                };
                pAccounts.Controls.Add(organizingBuffer);
            }

            foreach (var sa in accounts)
            {
                subVerticalOffset = 0;

                Panel accountContainer = new Panel();
                accountContainer.Size = new Size(210, 30); // no gap between accounts
                accountContainer.Location = new Point(0, 0);
                accountContainer.Tag = sa;
                accountContainer.ForeColor = Color.White;
                accountContainer.Margin = new Padding(0, 0, 0, 0);

                Button accountButton = new Button();
                accountButton.Name = "btn" + sa.Username;
                accountButton.Text = sa.DisplayName;
                accountButton.Font = new Font("Neo Sans", 10, FontStyle.Bold, GraphicsUnit.Point);
                accountButton.Size = new Size(230 - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth, 30);
                accountButton.Location = new Point(0, 0);
                accountButton.Click += btnAccount_Click;
                accountButton.MouseDown += btnAccount_MouseDown;
                accountButton.Cursor = Cursors.Hand;
                accountButton.FlatStyle = FlatStyle.Flat;
                accountButton.FlatAppearance.BorderSize = 0;
                accountButton.TextAlign = ContentAlignment.MiddleLeft;
                accountButton.Tag = sa;
                accountButton.UseVisualStyleBackColor = false;
                accountButton.Margin = new Padding(0);
                accountContainer.Controls.Add(accountButton);
                subVerticalOffset += accountButton.Height;

                int visibleSubscriptions = 0;

                foreach (var sub in sa.Subscriptions)
                {
                    // skipping hidden subscription:
                    if ((sub.Game == GameId.DDO && cbHideDDOSub.Checked == true) || (sub.Game == GameId.LOTRO && cbHideLOTROSub.Checked == true))
                    {
                        continue; // skip this iteration
                    }

                    visibleSubscriptions++;
                    accountContainer.Height = 30 + (visibleSubscriptions * 30); // increase size by one line + one line for each visible subscription

                    var subName = sub.DisplayName;
                    Button b = new Button();
                    b.Name = "btn" + sub.Id;
                    b.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    b.Size = new Size(210, 30);
                    b.Cursor = Cursors.Hand;
                    b.FlatStyle = FlatStyle.Flat;
                    b.FlatAppearance.BorderSize = 0;
                    b.Location = new Point(5, subVerticalOffset);
                    // Limit the length of the subName string based on its drawn bounding rectangle
                    //  (better support for variable-width fonts)
                    while (TextRenderer.MeasureText(subName, b.Font).Width > b.Width - 28 /* accounting for icon and padding */)
                        subName = subName.Substring(0, subName.Length - 1);
                    b.Text = subName;

                    if (sub.Game == GameId.DDO)
                        b.Image = Resources.ddo_20x20;
                    else
                        b.Image = Resources.lotro_20x20;

                    b.ImageAlign = ContentAlignment.MiddleLeft;
                    b.TextAlign = ContentAlignment.MiddleLeft;
                    b.TextImageRelation = TextImageRelation.ImageBeforeText;
                    b.UseVisualStyleBackColor = false;
                    b.Margin = new Padding(0);
                    b.Click += btnSubscription_Click;
                    b.Tag = sub;

                    accountContainer.Controls.Add(b);
                    subVerticalOffset += 30;
                }

                if (draggedAccount != null && draggedAccount.Username == sa.Username)
                {
                    accountContainer.Name = "draggedAccount";
                    this.Controls.Add(accountContainer);
                    accountContainer.BringToFront();
                    draggedControl = accountContainer;
                }
                else
                {
                    pAccounts.Controls.Add(accountContainer);
                }

                if (isOrganizing)
                {
                    Panel organizingBuffer = new Panel()
                    {
                        Location = new Point(0, 0),
                        Size = new Size(210, 0),
                        BackColor = pAccounts.BackColor,
                        Tag = "divider",
                        Cursor = Cursors.Hand,
                        Margin = new Padding(0, 0, 0, 0)
                    };
                    pAccounts.Controls.Add(organizingBuffer);
                }
            }

            // MouseUp must be handled for pAccounts and its child controls
            //  to commit changes made to the account organization
            CommitOnMouseUp(pAccounts);

            // Properly display vertical scrollbar
            pAccounts.HorizontalScroll.Maximum = 0;
            pAccounts.AutoScroll = false;
            pAccounts.VerticalScroll.Visible = false;
            pAccounts.AutoScroll = true;
            pAccounts.PerformLayout();
        }

        /// <summary>
        /// Recursively add a MouseUp handler for this control and all of its child controls.
        /// </summary>
        /// <param name="control"></param>
        private void CommitOnMouseUp(Control control)
        {
            control.MouseUp += CommitMouseUp;
            foreach (Control child in control.Controls)
            {
                CommitOnMouseUp(child);
            }
        }

        private void EditShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                ShortcutTag d = (ShortcutTag)btn.Tag;
                Shortcut sc = d.Shortcut;

                cbMakeDefault.Checked = false;
                cbPlayServers.Text = sc.Server;
                tbAddShortcutCharacter.Text = sc.Character;
                tbShortcutDisplayName.Text = sc.DisplayName;
                btnCancelShortcut.Visible = true;
                btnAddShortcut.Text = "Update Shortcut";
                btnAddShortcut.Tag = btn.Tag;
            }
        }

        private void UpdateShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                ShortcutTag d = (ShortcutTag)btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Subscription;

                var itemIndex = sub.Shortcuts.FindIndex(s => s == sc);
                sc.Character = tbAddShortcutCharacter.Text;
                sc.Server = cbPlayServers.Text;
                sc.DisplayName = tbShortcutDisplayName.Text;
                cbMakeDefault.Checked = false;
                cbPlayServers.Text = sub.DefaultServer;

                if (itemIndex > -1)
                    sub.Shortcuts[itemIndex] = sc;
                else
                    // removed?
                    sub.Shortcuts.Add(sc);

                UpdateGroupShortcut(sub, sc);
            }

            tbAddShortcutCharacter.Text = string.Empty;
            tbShortcutDisplayName.Text = string.Empty;
            btnAddShortcut.Text = "Create Shortcut";
            btnAddShortcut.Tag = null;
            btnCancelShortcut.Visible = false;
            SaveSettings();
            PopulateSubscriptionElements();
        }

        private void DeleteShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                ShortcutTag d = (ShortcutTag)btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Subscription;
                RemoveShortcutFromGroups(sc);
                sub.Shortcuts.Remove(sc);
            }

            SaveSettings();
            PopulateSubscriptionElements();
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (sender as Button)?.Tag as StoredAccount;
            SelectAccount(sa);
        }

        /// <summary>
        /// Return the control's location relative to the screen's bounding rectangle.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private Point PointToScreen(Control control)
        {
            Point pointToScreen = new Point(0, 0);
            Control thisNode = control;
            while (thisNode != this)
            {
                pointToScreen.Offset(thisNode.Location);
                thisNode = thisNode.Parent;
            }
            pointToScreen.Offset(this.Location);
            return pointToScreen;
        }

        private void btnAccount_MouseDown(object sender, MouseEventArgs e)
        {
            draggedAccount = (sender as Button)?.Tag as StoredAccount;
            initialMouseLocation = Cursor.Position;
            tmrOrganizeAccounts.Start();
        }

        /// <summary>
        /// Commits the new account organization and saves the StoredAccounts settings.
        /// </summary>
        private void CommitOrganization()
        {
            tmrOrganizeAccounts.Stop();
            if (draggedControl != null)
                draggedControl.Dispose();

            if (isOrganizing)
            {
                isOrganizing = false;

                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                List<StoredAccount> newSA = new List<StoredAccount>();

                // Divider check
                bool positionVerified = false;
                foreach (Control control in pAccounts.Controls)
                {
                    if (control is Panel)
                    {
                        if (control.Tag.ToString() == "divider" && control.BackColor == Color.FromArgb(85, 117, 153))
                        {
                            positionVerified = true;
                        }
                    }
                }

                if (positionVerified)
                {
                    foreach (Control control in pAccounts.Controls)
                    {
                        if (control is Panel)
                        {
                            if (control.Tag.ToString() == "divider" && control.Height != 0)
                            {
                                newSA.Add(draggedAccount);
                            }
                            else
                            {
                                if (control.Tag as StoredAccount != null && control.Tag as StoredAccount != draggedAccount)
                                {
                                    newSA.Add(control.Tag as StoredAccount);
                                }
                            }
                        }
                    }

                    // Ensure that the new StoredAccounts list is the same length as the old one
                    if (newSA.Count == settings.StoredAccounts.Count)
                    {
                        settings.StoredAccounts = newSA;
                        SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
                    }
                }

                draggedAccount = null;
                PopulateAccounts();
            }

            draggedAccount = null;
        }

        /// <summary>
        /// Discard any changes made to the account organization.
        /// </summary>
        private void DiscardOrganization()
        {
            tmrOrganizeAccounts.Stop();
            if (draggedControl != null)
                draggedControl.Dispose();
            draggedAccount = null;
            isOrganizing = false;
            PopulateAccounts();
        }

        private void CommitMouseUp(object sender, MouseEventArgs e)
        {
            CommitOrganization();
        }

        private void MainForm_Deactivate(object sender, EventArgs e)
        {
            // Discard any changes to account organization if the form loses focus
            DiscardOrganization();
        }

        private void tmrOrganizeAccounts_Tick(object sender, EventArgs e)
        {
            if (!isOrganizing && Math.Abs(Cursor.Position.Y - initialMouseLocation.Y) > 10)
            {
                // The cursor has clicked and dragged an account more than 10 pixels
                // Start organization
                isOrganizing = true;
                PopulateAccounts();
            }

            if (isOrganizing)
            {
                if (!pAccounts.Capture)
                    pAccounts.Capture = true;

                Point pointToScreen = PointToScreen(pAccounts);
                if (Cursor.Position.Y >= pointToScreen.Y + pAccounts.Height - 30)
                {
                    // The cursor has moved within 30 pixels of the bottom of the pAccounts control
                    // Scroll down
                    int delta = Math.Min(Cursor.Position.Y - (pointToScreen.Y + pAccounts.Height - 30), 30);
                    pAccounts.VerticalScroll.Value += delta;
                }
                else if (Cursor.Position.Y <= pointToScreen.Y + 30)
                {
                    // The cursor has moved within 30 pixels of the top of the pAccounts control
                    // Scroll up
                    int delta = Math.Max(Cursor.Position.Y - (pointToScreen.Y + 30), -30);
                    pAccounts.VerticalScroll.Value = Math.Max(pAccounts.VerticalScroll.Value + delta, 0);
                }

                // The draggedControl should follow the cursor
                draggedControl.Location = new Point(Cursor.Position.X - this.Location.X + 10, Cursor.Position.Y - this.Location.Y + 10);

                Control closestDivider = null;
                int closestDistance = 1000;

                // Find the divider control that's closest to the cursor's position:
                foreach (Control dividerPanel in pAccounts.Controls)
                {
                    if (dividerPanel is Panel)
                    {
                        if (dividerPanel.Tag.ToString() == "divider")
                        {
                            pointToScreen = PointToScreen(dividerPanel);
                            pointToScreen.Offset(0, dividerPanel.Height / 2);
                            if (Math.Abs(Cursor.Position.Y - pointToScreen.Y) < closestDistance)
                            {
                                closestDistance = Math.Abs(Cursor.Position.Y - pointToScreen.Y);
                                closestDivider = dividerPanel;
                            }
                        }
                    }
                }

                if (closestDivider == null)
                    return;

                // Set the closest divider control's Height and BackColor:
                foreach (Control dividerPanel in pAccounts.Controls)
                {
                    if (dividerPanel is Panel && dividerPanel.Tag.ToString() == "divider")
                    {
                        if (dividerPanel == closestDivider)
                        {
                            dividerPanel.Height = 60;
                            dividerPanel.BackColor = Color.FromArgb(85, 117, 153);
                        }
                        else
                        {
                            dividerPanel.Height = 0;
                            dividerPanel.BackColor = pAccounts.BackColor;
                        }
                    }
                }
            }
        }

        private void SelectAccount(StoredAccount sa)
        {
            pEditAccount.Tag = sa;
            tbEditUsername.Text = sa.Username;
            SendMessage(tbEditPassword.Handle, 0x1501, 1, "(enter to change)");
            tbEditDisplayName.Text = sa.DisplayName;

            pEditAccount.Visible = true;
            pAddAccount.Visible = false;
            scAccountDetails.Visible = false;
            btnCreateGroup.Visible = false;
        }

        private void SelectSubscription(Subscription sub)
        {
            // show main panel
            scAccountDetails.Visible = true;
            pAddAccount.Visible = false;
            pEditAccount.Visible = false;

            scAccountDetails.BringToFront();

            var gameInfo = ServiceClientHost.Instance.GetGameInformation(sub.Game);

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (gameInfo?.Servers.Count == 0)
                gameInfo = ServiceClientHost.Instance.GetGameInformation(sub.Game, true);

            bool gameIsMissingPath = true;

            switch (sub.Game)
            {
                case GameId.LOTRO:
                    gameIsMissingPath = string.IsNullOrWhiteSpace(settings.LotroPath);
                    break;
                case GameId.DDO:
                    gameIsMissingPath = string.IsNullOrWhiteSpace(settings.DdoPath);
                    break;
                default:
                    break;
            }

            if (gameInfo == null || gameIsMissingPath)
            {
                ShowMessage($"There is an error in the game path for {sub.Game}");
                return;
            }

            scAccountDetails.Tag = sub;
            lblSubscriptionInfo.Text = "Subscription Id: " + sub.Id;
            tbEditSubDisplayName.Text = sub.DisplayName;

            cbPlayServers.Items.Clear();

            if (gameInfo != null && gameInfo.Servers.Count > 0)
            {
                foreach (var server in gameInfo.Servers)
                    cbPlayServers.Items.Add(server.Name);
            }
            else
            {
                string[] gameServers = Extensions.GetServerNamesFromResources(sub.Game);

                if (gameServers?.Length > 0)
                {
                    foreach (var server in gameServers)
                        cbPlayServers.Items.Add(server);
                }
                else
                {
                    cbPlayServers.Items.Add(string.Empty);
                }
            }

            if (!string.IsNullOrWhiteSpace(sub.DefaultServer))
                cbPlayServers.SelectedItem = sub.DefaultServer;

            PopulateSubscriptionElements();
        }

        private void PopulateSubscriptionElements()
        {
            // generate UI for the selected sub
            if (!(scAccountDetails.Tag is Subscription))
                return;

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            pShortcuts.Controls.Clear();

            Subscription selectedSub = (Subscription)scAccountDetails.Tag;

            // create a label
            Label lbl = new Label();
            lbl.Text = "Shortcuts (Click to Play!)";
            lbl.Font = new Font("Neo Sans", 12, FontStyle.Bold, GraphicsUnit.Point);
            lbl.AutoSize = true;
            lbl.ForeColor = Color.Black;
            pShortcuts.Controls.Add(lbl);

            //pShortcuts.Dock = DockStyle.Fill;

            pShortcuts.Height = 284;
            pShortcuts.Width = 584;

            int yStart = 25;
            int scY_offset = yStart;
            int scX_offset = 5;
            int shortcutWidth = 187;
            int shortcutHeight = 30;
            var shortcuts = selectedSub.Shortcuts.OrderBy(s => s.GetDisplayName());

            // finds subscriptions that have a shortcut
            var subscriptionsWithShortcuts = settings?.StoredAccounts.Where(sa => sa.Subscriptions.Any(sub => sub.Shortcuts.Count > 0))
                .SelectMany(a => a.Subscriptions.Where(r => r.Shortcuts.Count > 0));

            // only test if we have more then one sub
            if (subscriptionsWithShortcuts?.Count() > 1)
            {
                // if the sub matches, enable the group button
                if (subscriptionsWithShortcuts.Any(s => s.Id == selectedSub.Id))
                {
                    btnCreateGroup.Visible = true;
                }
                else
                {
                    // no match, disable
                    btnCreateGroup.Visible = false;
                }
            }
            else
            {
                // not enough subs, disable
                btnCreateGroup.Visible = false;
            }

            // first add groups with a different color:
            foreach (LaunchGroup group in CachedGroups)
            {
                // list groups that contain the current sub:
                if (group.Members.Any(r => r.Subscription.Id == selectedSub.Id))
                {
                    Button groupButton = new Button();
                    groupButton.Text = "Group: " + group.Name;
                    groupButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    groupButton.Size = new Size(shortcutWidth, shortcutHeight);
                    groupButton.Cursor = Cursors.Hand;
                    groupButton.FlatStyle = FlatStyle.Flat;
                    groupButton.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    groupButton.FlatAppearance.BorderSize = 0;
                    groupButton.BackColor = Color.DarkOliveGreen;
                    groupButton.ForeColor = _turquoise;
                    groupButton.Tag = group;
                    groupButton.Click += GroupButton_Click;

                    if ((scY_offset + SystemInformation.HorizontalScrollBarHeight) > pShortcuts.Height)
                    {
                        scY_offset = yStart;
                        scX_offset = scX_offset + groupButton.Width + 65;
                        if (settings.ShowAdvancedLaunchOptions)
                            scX_offset += 30;

                        if (scX_offset > 400)
                        {
                            pShortcuts.AutoScroll = false;
                            pShortcuts.VerticalScroll.Value = 0;
                            pShortcuts.VerticalScroll.Visible = false;
                            pShortcuts.VerticalScroll.Enabled = false;
                            pShortcuts.AutoScroll = true;
                        }
                    }

                    groupButton.Location = new Point(scX_offset, scY_offset);
                    pShortcuts.Controls.Add(groupButton);

                    Button editButton = new Button();
                    editButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    editButton.Size = new Size(24, 30);
                    editButton.Margin = new Padding(0, 0, 4, 0);
                    editButton.Padding = new Padding(0);
                    editButton.FlatStyle = FlatStyle.Flat;
                    editButton.Cursor = Cursors.Hand;
                    editButton.FlatAppearance.BorderSize = 0;
                    editButton.BackColor = _navInactive;
                    editButton.ForeColor = _turquoise;
                    editButton.Location = new Point(groupButton.Size.Width + groupButton.Location.X + 4, scY_offset);
                    editButton.Image = Resources.box_edit_16;
                    editButton.ImageAlign = ContentAlignment.MiddleCenter;
                    editButton.Tag = group;
                    editButton.Click += EditButton_Click;
                    pShortcuts.Controls.Add(editButton);

                    // add a delete button too
                    Button deleteButton = new Button();
                    deleteButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    deleteButton.Size = new Size(24, 30);
                    deleteButton.Margin = new Padding(0, 0, 4, 0);
                    deleteButton.Padding = new Padding(0, 3, 3, 0);
                    deleteButton.FlatStyle = FlatStyle.Flat;
                    deleteButton.Cursor = Cursors.Hand;
                    deleteButton.FlatAppearance.BorderSize = 0;
                    deleteButton.BackColor = _navInactive;
                    deleteButton.ForeColor = _turquoise;
                    deleteButton.Location = new Point(editButton.Size.Width + editButton.Location.X + 4, scY_offset);
                    deleteButton.Tag = groupButton.Tag = group;
                    deleteButton.Image = Resources.trash_can_16;
                    deleteButton.ImageAlign = ContentAlignment.MiddleCenter;
                    deleteButton.Click += DeleteGroup_Click;
                    pShortcuts.Controls.Add(deleteButton);

                    if (settings.ShowAdvancedLaunchOptions)
                    {
                        // add a button to show the advanced launch options menu
                        Button argumentsButton = new Button();
                        argumentsButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                        argumentsButton.Size = new Size(24, 30);
                        argumentsButton.Margin = new Padding(0, 0, 4, 0);
                        argumentsButton.Padding = new Padding(0, 3, 3, 0);
                        argumentsButton.FlatStyle = FlatStyle.Flat;
                        argumentsButton.Cursor = Cursors.Hand;
                        argumentsButton.FlatAppearance.BorderSize = 0;
                        argumentsButton.BackColor = _navInactive;
                        argumentsButton.ForeColor = _turquoise;
                        argumentsButton.Location = new Point(deleteButton.Size.Width + deleteButton.Location.X + 4, scY_offset);
                        argumentsButton.Tag = new GroupTag() { Group = group };
                        argumentsButton.Image = Resources.cmd_16;
                        argumentsButton.ImageAlign = ContentAlignment.MiddleCenter;
                        argumentsButton.Click += AdvancedOptionsButton_Click;
                        addAccountsToolTip.SetToolTip(argumentsButton, "Show Advanced Launch Options");
                        pShortcuts.Controls.Add(argumentsButton);
                    }

                    scY_offset += groupButton.Height + 5;
                }
            }

            foreach (var sc in shortcuts)
            {
                Button shortcutButton = new Button();
                shortcutButton.Text = sc.GetDisplayName();
                shortcutButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                shortcutButton.Size = new Size(shortcutWidth, shortcutHeight);
                shortcutButton.Cursor = Cursors.Hand;
                shortcutButton.FlatStyle = FlatStyle.Flat;
                shortcutButton.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                shortcutButton.FlatAppearance.BorderSize = 0;
                shortcutButton.BackColor = _navInactive;
                shortcutButton.ForeColor = _turquoise;
                shortcutButton.Tag = new ShortcutTag() { Subscription = selectedSub, Shortcut = sc };
                shortcutButton.Click += ShortcutButton_Click;

                if ((scY_offset + SystemInformation.HorizontalScrollBarHeight) > pShortcuts.Height)
                {
                    scY_offset = yStart;
                    scX_offset = scX_offset + shortcutButton.Width + 65;
                    if (settings.ShowAdvancedLaunchOptions)
                        scX_offset += 30;

                    if (scX_offset > 400)
                    {
                        pShortcuts.AutoScroll = false;
                        pShortcuts.VerticalScroll.Value = 0;
                        pShortcuts.VerticalScroll.Visible = false;
                        pShortcuts.VerticalScroll.Enabled = false;
                        pShortcuts.AutoScroll = true;
                    }
                }

                shortcutButton.Location = new Point(scX_offset, scY_offset);
                pShortcuts.Controls.Add(shortcutButton);

                // add an edit button
                Button editShortcut = new Button();
                editShortcut.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                editShortcut.Size = new Size(24, 30);
                editShortcut.Margin = new Padding(0, 0, 4, 0);
                editShortcut.Padding = new Padding(0);
                editShortcut.FlatStyle = FlatStyle.Flat;
                editShortcut.Cursor = Cursors.Hand;
                editShortcut.FlatAppearance.BorderSize = 0;
                editShortcut.BackColor = _navInactive;
                editShortcut.ForeColor = _turquoise;
                editShortcut.Location = new Point(shortcutButton.Size.Width + shortcutButton.Location.X + 4, scY_offset);
                editShortcut.Image = Resources.box_edit_16;
                editShortcut.ImageAlign = ContentAlignment.MiddleCenter;
                editShortcut.Tag = new ShortcutTag() { Subscription = selectedSub, Shortcut = sc };
                editShortcut.Click += EditShortcut_Click;

                pShortcuts.Controls.Add(editShortcut);

                // add a delete button too
                Button deleteShortcut = new Button();
                deleteShortcut.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                deleteShortcut.Size = new Size(24, 30);
                deleteShortcut.Margin = new Padding(0, 0, 4, 0);
                deleteShortcut.Padding = new Padding(0, 3, 3, 0);
                deleteShortcut.FlatStyle = FlatStyle.Flat;
                deleteShortcut.Cursor = Cursors.Hand;
                deleteShortcut.FlatAppearance.BorderSize = 0;
                deleteShortcut.BackColor = _navInactive;
                deleteShortcut.ForeColor = _turquoise;
                deleteShortcut.Location = new Point(editShortcut.Size.Width + editShortcut.Location.X + 4, scY_offset);
                deleteShortcut.Tag = new ShortcutTag() { Subscription = selectedSub, Shortcut = sc };
                deleteShortcut.Image = Resources.trash_can_16;
                deleteShortcut.ImageAlign = ContentAlignment.MiddleCenter;
                deleteShortcut.Click += DeleteShortcut_Click;
                pShortcuts.Controls.Add(deleteShortcut);

                if (settings.ShowAdvancedLaunchOptions)
                {
                    // add a button to show the advanced launch options menu
                    Button argumentsButton = new Button();
                    argumentsButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    argumentsButton.Size = new Size(24, 30);
                    argumentsButton.Margin = new Padding(0, 0, 4, 0);
                    argumentsButton.Padding = new Padding(0, 3, 3, 0);
                    argumentsButton.FlatStyle = FlatStyle.Flat;
                    argumentsButton.Cursor = Cursors.Hand;
                    argumentsButton.FlatAppearance.BorderSize = 0;
                    argumentsButton.BackColor = _navInactive;
                    argumentsButton.ForeColor = _turquoise;
                    argumentsButton.Location = new Point(editShortcut.Size.Width + deleteShortcut.Location.X + 4, scY_offset);
                    argumentsButton.Tag = new ShortcutTag() { Subscription = selectedSub, Shortcut = sc };
                    argumentsButton.Image = Resources.cmd_16;
                    argumentsButton.ImageAlign = ContentAlignment.MiddleCenter;
                    argumentsButton.Click += AdvancedOptionsButton_Click;
                    addAccountsToolTip.SetToolTip(argumentsButton, "Show Advanced Launch Options");
                    pShortcuts.Controls.Add(argumentsButton);
                }

                scY_offset += shortcutButton.Height + 5;
            }
        }

        private void btnCreateGroup_Click(object sender, EventArgs e)
        {
            var characterShortcuts = GetSubscriptions();

            GroupForm groupForm = new GroupForm(characterShortcuts);

            groupForm.ShowDialog();

            var group = groupForm.Tag as LaunchGroup;

            if (group != null)
            {
                // force to show up in tray upon creation:
                group.ShowInTray = true;
                if (!CachedGroups.Contains(group))
                {
                    CachedGroups.Add(group);
                    SaveGroups();
                }
            }

            // if current sub is present in the selected group, add the button to the top of the shortcut list:
            PopulateSubscriptionElements();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            var group = (sender as Button)?.Tag as LaunchGroup;

            if (group != null)
            {
                var characterShortcuts = GetSubscriptions();

                // show the group form:
                GroupForm groupForm = new GroupForm(group, characterShortcuts);

                groupForm.ShowDialog();

                var newGroup = groupForm?.Tag as LaunchGroup;

                if (newGroup != null)
                {
                    // if changed, do this:
                    if (CachedGroups.Contains(group))
                    {
                        // transfer visibility
                        newGroup.ShowInTray = group.ShowInTray;
                        CachedGroups.Remove(group);
                        CachedGroups.Add(newGroup);
                        SaveGroups();
                        PopulateSubscriptionElements();
                    }
                }
            }
        }

        private void DeleteGroup_Click(object sender, EventArgs e)
        {
            // remove the group from the cached group and redraw
            var group = (sender as Button)?.Tag as LaunchGroup;
            if (group != null)
            {
                CachedGroups.Remove(group);
                SaveGroups();
                PopulateSubscriptionElements();
            }
        }

        private void SaveGroups()
        {
            // save to settings?
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            settings.Groupings = CachedGroups;
            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
        }

        private void GroupButton_Click(object sender, EventArgs e)
        {
            var group = (sender as Button)?.Tag as LaunchGroup;

            if (group != null)
            {
                foreach (var launch in group.Members)
                {
                    Play(launch.Subscription, launch.Shortcut);
                }
            }
        }

        private List<Subscription> GetSubscriptions()
        {
            var subs = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings()?.StoredAccounts;

            if (subs.Count > 0)
            {
                return subs.SelectMany(sa => sa.Subscriptions.Select(s => s)).ToList();
            }

            return null;
        }

        private void AdvancedOptionsButton_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                Point lowerLeftOfButton = new Point(0, btn.Height);
                lowerLeftOfButton = btn.PointToScreen(lowerLeftOfButton);
                cmsAdvancedOptions.SetShortcutTag(btn.Tag);
                cmsAdvancedOptions.Show(lowerLeftOfButton);
            }
        }

        private void ShortcutButton_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                ShortcutTag d = (ShortcutTag)btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Subscription;

                Thread t = new Thread(() => Play(sub, sc));
                t.IsBackground = true;
                t.Name = "ShortcutPlay";
                t.Start();
            }
        }

        private void btnSubscription_Click(object sender, EventArgs e)
        {
            if ((sender as Button).Tag is Subscription sub)
                SelectSubscription(sub);
        }

        private void btnSubscription_MouseUp(object sender, MouseEventArgs e)
        {
            CommitOrganization();
        }

        private void ResetAccountAddFields()
        {
            tbAccountDisplayName.Text = "";
            tbAccountPassword.Text = "";
            tbAccountUsername.Text = "";
        }

        private void btnAccountCancel_Click(object sender, EventArgs e)
        {
            ResetAccountAddFields();
            pAddAccount.Visible = false;
        }

        private void tbAccountUsername_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbAccountDisplayName.Text))
                tbAccountDisplayName.Text = tbAccountUsername.Text;
        }

        private void btnMessagesOkay_Click(object sender, EventArgs e)
        {
            pMessages.Visible = false;
        }

        private void btnRefreshStatus_Click(object sender, EventArgs e)
        {
            btnRefreshStatus.Enabled = false;
            lblServerStatus.Text = "Loading...";
            LoadGameInfo();
            btnRefreshStatus.Enabled = true;
        }

        private void LoadGameInfo()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.AppendLine("DDO");
                var ddoInfo = ServiceClientHost.Instance.GetGameInformation(GameId.DDO, true);
                if (ddoInfo != null)
                {
                    _gameInfo[GameId.DDO] = ddoInfo;
                    _launcherInfo[GameId.DDO] = ServiceClientHost.Instance.GetLauncherConfiguration(ddoInfo);
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            sb.AppendLine();

            try
            {
                sb.AppendLine("LOTRO");
                var lotroInfo = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO, true);
                if (lotroInfo != null)
                {
                    _gameInfo[GameId.LOTRO] = lotroInfo;
                    _launcherInfo[GameId.LOTRO] = ServiceClientHost.Instance.GetLauncherConfiguration(lotroInfo);
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            lblServerStatus.Text = sb.ToString();
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        private async void btnPlay_Click(object sender, EventArgs e)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            if (string.IsNullOrWhiteSpace(cbPlayServers.Text))
                return;

            Subscription sub = (Subscription)scAccountDetails.Tag;
            Shortcut sc = new Shortcut() { Server = cbPlayServers.Text, Character = tbAddShortcutCharacter.Text };

            if (cbMakeDefault.Checked)
            {
                sub.DefaultServer = cbPlayServers.Text;
                SaveSettings();
            }

            Thread t = new Thread(() => Play(sub, sc));
            t.IsBackground = true;
            t.Name = "DirectPlay";
            t.Start();
        }

        private void btnRemoveAccount_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (StoredAccount)pEditAccount.Tag;
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            // this is wishful thinking and prone to future bugs
            settings.StoredAccounts.Remove(sa);

            pEditAccount.Visible = false;

            PopulateAccounts();
        }

        private void btnEditAccountCancel_Click(object sender, EventArgs e)
        {
            // don't leave passwords partially entered hanging around in memory
            tbEditPassword.Text = "";

            pEditAccount.Visible = false;
        }

        private void btnEditAccountSave_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (StoredAccount)pEditAccount.Tag;

            if (!string.IsNullOrWhiteSpace(tbEditPassword.Text))
                sa.DecryptedPassword = tbEditPassword.Text;

            sa.DisplayName = tbEditDisplayName.Text;

            // this SHOULD be a pointer to the root object that this StoredAccount belongs to, but
            // there are all sorts of risks with assuming that.  /sigh.
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            pEditAccount.Visible = false;

            // call repopulate to force changes to display name to propagate
            PopulateAccounts();
        }

        private void btnEditSubSave_Click(object sender, EventArgs e)
        {
            Subscription sub = (Subscription)scAccountDetails.Tag;

            sub.DisplayName = tbEditSubDisplayName.Text;

            if (cbMakeDefault.Checked && cbPlayServers?.Text.Length > 0)
            {
                sub.DefaultServer = cbPlayServers.Text;
                cbMakeDefault.Checked = false;
            }

            UpdateGroupSubscription(sub);
            SaveSettings();
            PopulateAccounts();
            SelectSubscription(sub);
        }

        private void RemoveShortcutFromGroups(Shortcut sc)
        {
            LaunchGroup group = CachedGroups.Where(m => m.Members.Any(s => s.Shortcut.Guid == sc.Guid)).FirstOrDefault();

            if (group != null)
            {
                // remove shortcut/sub from members of group or delete if there is only two
                if (group.Members.Count > 2)
                {
                    // remove
                    var groupMember = group.Members.Where(s => s.Shortcut.Guid == sc.Guid).FirstOrDefault();
                    if (groupMember != null)
                    {
                        group.Members.Remove(groupMember);
                    }
                }
                else
                {
                    // delete group
                    CachedGroups.Remove(group);
                }
                SaveGroups();
            }
        }

        private void UpdateGroupSubscription(Subscription sub)
        {
            // extract group
            LaunchGroup group = CachedGroups.Where(m => m.Members.Any(s => s.Subscription.Id == sub.Id)).FirstOrDefault();

            if (group != null)
            {
                var groupSub = group.Members.Where(s => s.Subscription.Id == sub.Id).FirstOrDefault();
                if (groupSub != null && groupSub.Subscription.DisplayName != sub.DisplayName)
                {
                    // check and update sub name in group if different:
                    groupSub.Subscription.DisplayName = sub.DisplayName;
                    // re-save to settings
                    SaveGroups();
                }
            }
        }

        private void UpdateGroupShortcut(Subscription subscription, Shortcut shortcut)
        {
            // extract group
            LaunchGroup group = CachedGroups.Where(m => m.Members.Any(s => s.Subscription.Id == subscription.Id)).FirstOrDefault();

            if (group != null)
            {
                var groupSub = group.Members.Where(s => s.Subscription.Id == subscription.Id).FirstOrDefault();
                if (groupSub != null && groupSub.Shortcut.DisplayName != shortcut.DisplayName)
                {
                    // check and update sub name in group if different:
                    groupSub.Shortcut.DisplayName = shortcut.DisplayName;
                    SaveGroups();
                }
            }
        }

        private void tbnAddUpdateShortcut_Click(object sender, EventArgs e)
        {
            // Update existing shortcut
            if (btnAddShortcut.Text.Contains("Update"))
            {
                UpdateShortcut_Click(sender, e);
                return;
            }

            Subscription sub = (Subscription)scAccountDetails.Tag;

            if (string.IsNullOrWhiteSpace(cbPlayServers.Text))
                return;

            Shortcut sc = new Shortcut();
            sc.Guid = Guid.NewGuid().ToString();
            sc.Server = cbPlayServers.Text;
            sc.Character = tbAddShortcutCharacter.Text;
            sc.DisplayName = tbShortcutDisplayName.Text;

            sub.Shortcuts.Add(sc);

            if (cbMakeDefault.Checked)
            {
                sub.DefaultServer = cbPlayServers.Text;
            }

            SaveSettings();

            tbAddShortcutCharacter.Text = "";
            tbShortcutDisplayName.Text = "";
            PopulateAccounts();
            SelectSubscription(sub);

            // restore the selected server
            cbPlayServers.Text = sc.Server;
        }

        private string GetClientGamePath(GameId gameId, Settings settings, LauncherConfiguration launcherInfo)
        {
            string clientPath = string.Empty;

            // Validate the GameId
            if (Enum.IsDefined(typeof(GameId), gameId))
            {
                // if Game is valid, get path from settings:
                if (gameId == GameId.DDO)
                    clientPath = settings?.DdoPath != string.Empty ? settings?.DdoPath : string.Empty;
                else if (gameId == GameId.LOTRO)
                    clientPath = settings?.LotroPath != string.Empty ? settings?.LotroPath : string.Empty;

                // Validate the Path
                if (string.IsNullOrEmpty(clientPath))
                {
                    // bail out, we don't have a folder set to launch from.
                    MessageBox.Show($"Your settings path is empty, please set the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }
                if (!Directory.Exists(clientPath))
                {
                    // bail out, The folder is incorrect.
                    MessageBox.Show($"The path in your settings is invalid or does not exist, please change the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }

                var clientExePath = Path.Combine(clientPath, launcherInfo.Settings["GameClient.WIN32.Filename"]);

                // check if the client executable exists
                if (!File.Exists(clientExePath))
                {
                    //MessageBox.Show("There was an issue locating the client executable.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    //return null;
                }

                var clientPatchDllPath = Path.Combine(clientPath, @"patchclient.dll");

                // check if the patchclient.dll file exists
                if (!File.Exists(clientPatchDllPath))
                {
                    MessageBox.Show("There was an issue locating the client patcher.", $"Cannot Patch game {gameId}.", MessageBoxButtons.OK);
                    // disable auto-patching?
                    // return null;
                    settings.PatchBeforeLaunch = false;
                }
            }
            else
            {
                // Invalid GameId // bail out, The folder is incorrect.
                MessageBox.Show("Could not determine a valid Game to launch.", "Please update your subscription information, under your account.", MessageBoxButtons.OK);
                return null;
            }
            return clientPath;
        }

        public void Play(Subscription sub, Shortcut shortcut, bool getArgumentsOnly = false)
        {
            try
            {
                if (sub == null || shortcut == null)
                {
                    // fix password/account
                    MessageBox.Show("Please re-enter your account information, for this subscription.",
                        "Please update your subscription information, under your account.", MessageBoxButtons.OK);
                    return;
                }

                // go find the account for this sub
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                if (settings.MinimizeBeforeLaunch)
                    Hide_MainForm();

                var account = settings.StoredAccounts.Where(sa => sa.Subscriptions.Any(s => s.Id == sub.Id)).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(account?.DecryptedPassword))
                {
                    MessageBox.Show($"Please re-enter your account information, for this account {account?.Username}.",
                        "Please check your account or the password, in the settings menu.", MessageBoxButtons.OK);
                    return;
                }

                LaunchRequest newLaunch = GetLaunch(sub, shortcut, account);

                if (newLaunch != null)
                {
                    if (!getArgumentsOnly)
                        LaunchQueue.LaunchClient(newLaunch);
                    else
                    {
                        var arguments = Launcher.GetClientArguments(newLaunch.Subscription, newLaunch.Shortcut, newLaunch.LauncherConfiguration, newLaunch.GameInformation,
                            newLaunch.ClientFolder, newLaunch.LoginTicket, newLaunch.Language, newLaunch.Server);

                        if (string.IsNullOrWhiteSpace(arguments))
                        {
                            MessageBox.Show("Unable to get client arguments, and the error has been logged. Check the server status.", "Dungeon Finder");
                            return;
                        }

                        Thread thread = new Thread(() => Clipboard.SetText(arguments));
                        if (OperatingSystem.IsWindows())
                            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
                        thread.Start();
                        thread.Join();
                        MessageBox.Show("dndclient.exe arguments have been copied to your clipboard.  Be careful, they contain sensitive account information!", "Dungeon Finder");
                    }
                }
                else
                    throw new Exception("Could not deteremine Launch reason.");
            }
            catch (Exception ex)
            {
                _log.Error("Error in MainForm.Play", ex);
            }
        }

        private LaunchRequest GetLaunch(Subscription sub, Shortcut shortcut, StoredAccount account)
        {
            LaunchRequest newLaunch = new LaunchRequest() { Subscription = sub, Shortcut = shortcut };

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            try
            {
                newLaunch.LoginTicket = ServiceClientHost.Instance.Authenticate(account.Username, account.DecryptedPassword, out string message, sub.Game).Ticket;
                // capture soft errors:
                if (message?.Length > 0)
                {
                    MessageBox.Show($"Please check your account or the password, in the settings menu.\n\nThe server returned a possible error message: {message}", $"Error authenticating {account.Username}.", MessageBoxButtons.OK);
                    return null;
                }
            }
            catch
            {
                MessageBox.Show($"There was an error connecting to the auth service. Please check your internet connection.", $"Error authenticating {account.Username}.", MessageBoxButtons.OK);
                return null;
            }

            if (!_launcherInfo.ContainsKey(sub.Game))
            {
                MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", $"ERROR: Could not connect to the {sub.Game} servers.", MessageBoxButtons.OK);
                return null;
            }

            newLaunch.LauncherConfiguration = _launcherInfo[sub.Game];
            newLaunch.GameInformation = _gameInfo[sub.Game];
            // must come after settings:
            newLaunch.ClientFolder = GetClientGamePath(sub.Game, settings, newLaunch.LauncherConfiguration);
            newLaunch.Use64Bit = settings.Use64BitClients;

            // find the server from game info and add it to launch
            string serverNameNoFlags = Extensions.StripServerFlags(shortcut.Server);
            newLaunch.Server = _gameInfo[sub.Game].Servers.Where(s => s.Name.Contains(serverNameNoFlags)).FirstOrDefault();

            // This must be valid, to launch the game. Errors are show in a MessageBox, from within GetClientGamePath
            if (newLaunch.ClientFolder == null)
                return null;

            if (settings.PatchBeforeLaunch)
            {
                if (!Launcher.IsPatching(newLaunch.GameInformation.Game) &&
                    !Launcher.ProccessExists(newLaunch.GameInformation.Game))
                {
                    if (Program.TrayIcon != null)
                    {
                        Program.TrayIcon.ToggleTrayIcon();
                        if (settings.EnableNotifications)
                            Program.TrayIcon.PopupNotification($"Patching {newLaunch.GameInformation.Game}",
                                "Dungeon Finder Auto Patcher");
                    }

                    Launcher.PatchClient(newLaunch.ClientFolder, newLaunch.GameInformation.Game,
                        newLaunch.GameInformation.PatchServer, null, null);

                    if (Program.TrayIcon != null)
                    {
                        Program.TrayIcon.ToggleTrayIcon();
                        if (settings.EnableNotifications)
                            Program.TrayIcon.PopupNotification($"Done patching {newLaunch.GameInformation.Game}",
                                "Dungeon Finder Auto Patcher");
                    }
                }
            }

            newLaunch.Language = "EN";
            return newLaunch;
        }

        private void SaveSettings()
        {
            // this SHOULD be a pointer to the root object that this StoredAccount belongs to, but
            // there are all sorts of risks with assuming that.  /sigh.
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            settings.DdoPath = txtBxDDOPath?.Text;
            settings.LotroPath = txtBxLOTROPath?.Text;
            settings.PatchBeforeLaunch = cbAutoPatch.Checked;
            settings.MinimizeToTray = cbMinimizeToTray.Checked;
            settings.EnableNotifications = cbEnablePatchNotifications.Checked;
            settings.MinimizeBeforeLaunch = cbMinimizeOnLaunch.Checked;
            settings.CheckForPatchMinutes = null;
            settings.Use64BitClients = cb64bit.Checked;
            settings.ShowAdvancedLaunchOptions = cbShowAdvancedOptionsButton.Checked;
            settings.AllowDiagnostics = cbAllowDiagnostics.Checked;
            settings.HideDDOSub = cbHideDDOSub.Checked;
            settings.HideLOTROSub = cbHideLOTROSub.Checked;
            settings.CheckForUpdates = cbUpdateOnStart.Checked;

            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            LaunchPatchThread();

            settingsSnapshot = GetSettingsSnapshot(); // Update the settings snapshot
        }

        private void btnUserSettingsSave_Click(object sender, EventArgs e)
        {
            SaveSettings();
            PopulateAccounts();
            PopulateUserSettings();
        }

        private void PopulateUserSettings()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings != null)
            {
                if (settings.DdoPath?.Length > 0)
                    txtBxDDOPath.Text = settings.DdoPath;
                else
                    txtBxDDOPath.Text = "";
                if (settings.LotroPath?.Length > 0)
                    txtBxLOTROPath.Text = settings.LotroPath;
                else
                    txtBxLOTROPath.Text = "";
                cbAutoPatch.Checked = settings.PatchBeforeLaunch;
                cbMinimizeToTray.Checked = settings.MinimizeToTray;
                cbEnablePatchNotifications.Checked = settings.EnableNotifications;
                cbMinimizeOnLaunch.Checked = settings.MinimizeBeforeLaunch;
                cb64bit.Checked = settings.Use64BitClients;
                cbShowAdvancedOptionsButton.Checked = settings.ShowAdvancedLaunchOptions;
                cbAllowDiagnostics.Checked = settings.AllowDiagnostics;
                cbHideDDOSub.Checked = settings.HideDDOSub;
                cbHideLOTROSub.Checked = settings.HideLOTROSub;
                cbUpdateOnStart.Checked = settings.CheckForUpdates;

                if (OperatingSystem.IsWindows())
                {
                    RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                    var regValue = registryKey.GetValue(RegistryApplicationName);
                    cbStartWithWindows.Checked = (regValue != null);
                }
            }

            if (settings.Groupings != null)
            {
                CachedGroups = settings.Groupings;
            }
        }

        private string GetSettingsSnapshot()
        {
            // Setting snapshots allow for future settings (textboxes and checkboxes)
            //  to be added without any change to the snapshot code
            string ss = "";
            foreach (Control ctr in pUserSettings.Controls)
            {
                if (!(ctr is CheckBox))
                    continue;
                ss += ((CheckBox)ctr).Checked ? "1" : "0";
            }
            foreach (Control ctr in pUserSettings.Controls)
            {
                if (!(ctr is TextBox))
                    continue;
                ss += ((TextBox)ctr).Text;
            }
            return ss;
        }

        private bool CheckForSettingsChanges()
        {
            if (discardSettingsChanges)
            {
                // Handle 'Discard' button pressed
                discardSettingsChanges = false;
                return false;
            }
            if (!pUserSettings.Visible)
                return false;
            if (settingsSnapshot == null)
                return false;

            // Check for any changes on the pUserSettings control
            if (settingsSnapshot != "" && settingsSnapshot != GetSettingsSnapshot())
            {
                // Something was changed
                pUnsavedChanges.Visible = true;
                pUnsavedChanges.BringToFront();
                pUnsavedChanges.Location = new Point(0, this.Height / 2 - pUnsavedChanges.Height / 2);
                pUnsavedChanges.Width = this.Width;
                pUnsavedChangesDialog.Location = new Point(pUnsavedChanges.Width / 2 - pUnsavedChangesDialog.Width / 2, pUnsavedChanges.Height / 2 - pUnsavedChangesDialog.Height / 2);
                return true; // Return 'true' if there are unsaved changes
            }
            return false;
        }

        private void GoToWebsite(string siteUrl)
        {
            try
            {
                Process.Start(siteUrl);
            }
            catch
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    siteUrl = siteUrl.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {siteUrl}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", siteUrl);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", siteUrl);
                }
                else
                {
                    throw;
                }
            }
        }

        private void btnWeb_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.WebsiteUrl);
        }

        private void btnDiscord_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.DiscordUrl);
        }

        private void btnGitlab_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.GitlabUrl);
        }

        private async void btnDDOPatchNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_gameInfo.ContainsKey(GameId.DDO))
                {
                    var temp = ServiceClientHost.Instance.GetGameInformation(GameId.DDO);

                    if (temp == null)
                    {
                        // most likely down for maintenance or internet connectivity issues
                        MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", "ERROR: Could not patch the DDO game client files.", MessageBoxButtons.OK);
                        return;
                    }

                    _gameInfo.Add(GameId.DDO, temp);
                }

                var gameInfo = _gameInfo[GameId.DDO];

                if (gameInfo == null)
                    LoadGameInfo();

                gameInfo = _gameInfo[GameId.DDO];
                if (gameInfo == null)
                {
                    MessageBox.Show("Unable to get game information from SSG servers.  The game is most likely down for some reason or another.");
                    return;
                }

                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                var launcherInfo = ServiceClientHost.Instance.GetLauncherConfiguration(gameInfo);
                if (settings != null && launcherInfo != null)
                {
                    var clientPath = GetClientGamePath(gameInfo.Game, settings, launcherInfo);
                    if (!string.IsNullOrWhiteSpace(clientPath))
                    {
                        ToggleTrayIcon();
                        using (TextOutputWindow window = new TextOutputWindow(true))
                        {

                            window.Show();
                            Launcher.PatchClient(clientPath, gameInfo.Game, gameInfo.PatchServer, window.AppendPatchChar, window.EnableButtons);

                            while (window.DialogResult != DialogResult.OK)
                            {
                                await Task.Delay(500);
                            }
                        }

                        ToggleTrayIcon();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem patching the client: " + ex.ToString());
            }
        }

        private async void btnLOTROPatchNow_Click(object sender, EventArgs e)
        {
            if (!_gameInfo.ContainsKey(GameId.LOTRO))
            {
                var temp = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO);

                if (temp == null)
                {
                    // most likely down for maintenance or internet connectivity issues
                    MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", "ERROR: Could not patch the LOTRO game client files.", MessageBoxButtons.OK);
                    return;
                }

                _gameInfo.Add(GameId.LOTRO, temp);
            }

            var gameInfo = _gameInfo[GameId.LOTRO];
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            var launcherInfo = ServiceClientHost.Instance.GetLauncherConfiguration(gameInfo);
            if (settings != null && launcherInfo != null)
            {
                var clientPath = GetClientGamePath(gameInfo.Game, settings, launcherInfo);
                if (!string.IsNullOrWhiteSpace(clientPath))
                {
                    using (TextOutputWindow window = new TextOutputWindow(true))
                    {
                        ToggleTrayIcon();
                        window.Show();
                        Launcher.PatchClient(clientPath, gameInfo.Game, gameInfo.PatchServer, window.AppendPatchChar, window.EnableButtons);

                        while (window.DialogResult != DialogResult.OK)
                        {
                            await Task.Delay(500);
                        }

                        ToggleTrayIcon();
                    }
                }
            }
        }
        private string AttemptLocatingLOTROPath()
        {
            //C:\Program Files (x86)\StandingStoneGames\The Lord of the Rings Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

            string testPath = Path.Combine(path, "StandingStoneGames");

            if (Directory.Exists(testPath))
            {
                var lotroPath = Path.Combine(testPath, "The Lord of the Rings Online");
                if (Directory.Exists(lotroPath))
                {
                    return lotroPath;
                }
            }

            return path;
        }

        private string AttemptLocatingDDOPath()
        {
            //C:\Program Files (x86)\StandingStoneGames\Dungeons & Dragons Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

            string testPath = Path.Combine(path, "StandingStoneGames");

            if (Directory.Exists(testPath))
            {
                var ddoPath = Path.Combine(testPath, "Dungeons & Dragons Online");
                if (Directory.Exists(ddoPath))
                {
                    return ddoPath;
                }
            }

            return path;
        }

        private void btnFindDdo_Click(object sender, EventArgs e)
        {
            var startingFolder = txtBxDDOPath.Text.Length > 0 ? txtBxDDOPath.Text : AttemptLocatingDDOPath();

            using (var browse = new FolderBrowserDialog() { SelectedPath = startingFolder })
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                if (settings == null) return;

                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.SelectedPath))
                {
                    var selectedFolder = Path.GetFullPath(browse.SelectedPath);
                    if (settings.LotroPath == selectedFolder) return;
                    settings.DdoPath = selectedFolder;
                    txtBxDDOPath.Text = selectedFolder;
                    SaveSettings();
                }
            }
        }

        private void btnFindLotro_Click(object sender, EventArgs e)
        {
            var startingFolder = txtBxLOTROPath.Text.Length > 0 ? txtBxLOTROPath.Text : AttemptLocatingLOTROPath();

            using (var browse = new FolderBrowserDialog() { SelectedPath = startingFolder })
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                if (settings == null) return;

                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.SelectedPath))
                {
                    var selectedFolder = Path.GetFullPath(browse.SelectedPath);
                    if (settings.LotroPath == selectedFolder) return;
                    settings.LotroPath = selectedFolder;
                    txtBxLOTROPath.Text = selectedFolder;
                    SaveSettings();
                }
            }
        }

        private void btnMyDDO_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.MyDdoUrl);
        }

        public void Hide_MainForm()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new System.Windows.Forms.MethodInvoker(delegate
                {
                    Hide();
                }));
            }
            else
                Hide();
        }

        /// <summary>
        /// Adds a "turquoise" "border" .... inside of a button ...
        /// </summary>
        private void paintBorderInsideButton(object sender, PaintEventArgs e)
        {
            int borderSize = 2;
            ButtonBorderStyle buttonBorderStyle = ButtonBorderStyle.Solid;
            Rectangle borderRectangle = new Rectangle(e.ClipRectangle.Location, e.ClipRectangle.Size);
            borderRectangle.Inflate(-10, -10);
            Color borderColor = Color.FromArgb(255, 51, 252, 254);
            ControlPaint.DrawBorder(e.Graphics, borderRectangle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle);
        }

        private void btnCancelShortcut_Click(object sender, EventArgs e)
        {
            btnAddShortcut.Text = "Create Shortcut";
            btnAddShortcut.Tag = null;
            tbAddShortcutCharacter.Text = string.Empty;
            tbShortcutDisplayName.Text = string.Empty;
            btnCancelShortcut.Visible = false;
        }

        private void btnMainSettings_Click(object sender, EventArgs e)
        {
            PopulateUserSettings();

            _currentTab = NavTab.Settings;
            _settingsSubTab = SettingsSubTab.Settings;
            RenderMenus();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Program.DoUpdateCheck();
        }

        private void btnYouTube_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.YouTubeUrl);
        }

        private void cbStartWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            if (OperatingSystem.IsWindows())
            {
                RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (cbStartWithWindows.Checked)
                {
                    // Add the value in the registry so that the application runs at startup
                    registryKey.SetValue(RegistryApplicationName, System.Windows.Forms.Application.ExecutablePath);
                }
                else
                {
                    // Remove the value from the registry so that the application doesn't start
                    registryKey.DeleteValue(RegistryApplicationName, false);
                }
            }
        }

        private void btnVersionInfo_Click(object sender, EventArgs e)
        {
            // always get a fresh copy of the settings
            var versions = JsonConvert.SerializeObject(VersionInfoExtension.GetVersionInfo());
            Thread thread = new Thread(() => Clipboard.SetText(versions));
            if (OperatingSystem.IsWindows())
                thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
            MessageBox.Show("Version details have been copied to your clipboard.", "Dungeon Finder");
        }

        private void btnSaveUnsavedChanges_Click(object sender, EventArgs e)
        {
            SaveSettings();
            PopulateAccounts();
            PopulateUserSettings();
            RenderMenus();
        }

        private void btnDiscardUnsavedChanges_Click(object sender, EventArgs e)
        {
            discardSettingsChanges = true;
            RenderMenus();
        }

        private void tbAccountPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            // if enter and password length > 0, click save
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (tbAccountPassword?.Text.Length > 0)
                {
                    btnAccountSave_Click(sender, null);
                }
            }
        }

        private void lblSubscriptionInfo_Click(object sender, EventArgs e)
        {
            if (!(scAccountDetails.Tag is Subscription))
                return;

            string id = (scAccountDetails.Tag as Subscription).Id;

            Thread thread = new Thread(() => Clipboard.SetText(id));
            if (OperatingSystem.IsWindows())
                thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
            MessageBox.Show("Subscription ID {id} has been copied to your clipboard.", "Dungeon Finder");
        }

        private void getCommandLineArgumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    // do not do anything when a group is selected:
                    if (parent.CurrentGroup != null)
                    {
                        return;
                    }

                    ShortcutTag shortcut = parent.CurrentShortcutTag;

                    Thread t = new Thread(() => Play(shortcut.Subscription, shortcut.Shortcut, true));
                    t.IsBackground = true;
                    t.Name = "GetArguments";
                    t.Start();
                }
            }
        }

        private void copyShortcutIDToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    // do not do anything when a group is selected:
                    if (parent.CurrentGroup != null)
                    {
                        return;
                    }

                    ShortcutTag shortcut = parent.CurrentShortcutTag;

                    string id = shortcut.Shortcut.Guid;

                    Thread thread = new Thread(() => Clipboard.SetText(id));
                    if (OperatingSystem.IsWindows())
                        thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
                    thread.Start();
                    thread.Join();
                    MessageBox.Show($"Shortcut ID {id} has been copied to your clipboard.", "Dungeon Finder");
                }
            }
        }

        private void createDesktopShortcutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    // group tags need to use different launch arguments:
                    if (parent.CurrentGroup != null)
                    {
                        Extensions.CreateWindowsGroupShortcut(parent.CurrentGroup);
                    }
                    else
                    {
                        ShortcutTag shortcutTag = parent.CurrentShortcutTag;
                        Extensions.CreateWindowsShortcut(shortcutTag);
                    }
                }
            }
        }

        private void hideThisShortcutFromTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    if (parent.CurrentShortcutTag != null)
                    {
                        var itemIndex = parent.CurrentShortcutTag.Subscription.Shortcuts.FindIndex(s => s == parent.CurrentShortcutTag.Shortcut);
                        if (itemIndex > -1)
                        {
                            parent.CurrentShortcutTag.Subscription.Shortcuts[itemIndex].ShowInTray = false;
                        }
                    }

                    if (parent.CurrentGroup != null)
                    {
                        parent.CurrentGroup.Group.ShowInTray = false;
                    }

                    SaveSettings();
                    PopulateAccounts();
                    PopulateUserSettings();
                }
            }
        }

        private void showThisShortcutInTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    if (parent.CurrentShortcutTag != null)
                    {
                        var itemIndex = parent.CurrentShortcutTag.Subscription.Shortcuts.FindIndex(s => s == parent.CurrentShortcutTag.Shortcut);
                        if (itemIndex > -1)
                        {
                            parent.CurrentShortcutTag.Subscription.Shortcuts[itemIndex].ShowInTray = true;
                        }
                    }

                    if (parent.CurrentGroup != null)
                    {
                        parent.CurrentGroup.Group.ShowInTray = true;
                    }

                    SaveSettings();
                    PopulateAccounts();
                    PopulateUserSettings();
                }
            }
        }

        private void assignCustomPreferencesFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem sItem)
            {
                var parent = sItem.GetCurrentParent() as AdvancedOptionsMenuStrip;
                if (parent != null)
                {
                    if (parent.CurrentShortcutTag != null)
                    {
                        var itemIndex = parent.CurrentShortcutTag.Subscription.Shortcuts.FindIndex(s => s == parent.CurrentShortcutTag.Shortcut);
                        if (itemIndex > -1)
                        {
                            PreferencesForm preferencesForm = new PreferencesForm(parent.CurrentShortcutTag.Subscription.Game, parent.CurrentShortcutTag.Shortcut);
                            preferencesForm.ShowDialog();
                        }
                    }
                }
            }
        }

        private void updateCustomPreferencesFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            assignCustomPreferencesFileToolStripMenuItem_Click(sender, e);
        }

        private void btnPreferences_Click(object sender, EventArgs e)
        {
            Subscription selectedSub = (Subscription)scAccountDetails.Tag;
            PreferencesForm preferencesForm = new PreferencesForm(selectedSub.Game, selectedSub);
            preferencesForm.ShowDialog();
        }
    }
}
