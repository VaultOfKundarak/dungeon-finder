﻿using DungeonFinder.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonFinder
{
    /// <summary>
    ///  Groups are lists of subscription shortcuts that do not share a subscription.
    ///  This form controls how shortcuts can be grouped.
    /// </summary>
    public partial class GroupForm : Form
    {
        private List<Subscription> availableSubscriptions;

        public GroupForm(List<Subscription> launchShortcuts)
        {
            InitializeComponent();
            availableSubscriptions = launchShortcuts;
            PopulateAvailableShortcuts();
        }

        public GroupForm(LaunchGroup group, List<Subscription> launchShortcuts)
        {
            InitializeComponent();
            availableSubscriptions = group.Members.Select(g => g.Subscription).ToList();
            var newShortcuts = launchShortcuts.Where(p => availableSubscriptions.All(p2 => p2.Id != p.Id)).ToList();

            // add the selected group names:
            foreach (var availablesub in availableSubscriptions.ToList())
            {
                // add subs to selected and remove from available
                var match = group.Members.Where(m => m.Subscription == availablesub).FirstOrDefault();
                if (match != null)
                {
                    availableSubscriptions.Remove(availablesub);
                    lstBxSelectedShortcuts.Items.Add(match);
                }
            }

            availableSubscriptions.AddRange(newShortcuts);

            if (group.Name?.Length > 0)
                txtBxName.Text = group.Name;

            PopulateAvailableShortcuts();
        }

        private void PopulateAvailableShortcuts()
        {
            foreach (var sub in availableSubscriptions)
            {
                foreach (var c in sub.Shortcuts)
                {
                    lstBxShortcutNames.Items.Add(new ShortcutTag() { Shortcut = c, Subscription = sub });
                }
            }

            lstBxShortcutNames.Sorted = true; // sort names
        }

        private void btnGroupAdd_Click(object sender, EventArgs e)
        {
            if (lstBxShortcutNames.Items.Count > 0 && lstBxShortcutNames.SelectedIndex > -1)
            {
                // adds the shortcut from the available subscriptions
                var selectedAvailableShortcut = lstBxShortcutNames.SelectedItem as ShortcutTag;

                if (selectedAvailableShortcut != null)
                {
                    // test if there is already an account from this sub
                    foreach (ShortcutTag groupMember in lstBxSelectedShortcuts.Items)
                    {
                        if (groupMember.Subscription == selectedAvailableShortcut.Subscription)
                        {
                            // already has it, send msg and bail
                            var msg = "Cannot add two shortcuts into the same group that are from the same subscription.";
                            // no shortcuts added to list, bail or warn?
                            MessageBox.Show(msg);
                            lblStatus.Text = msg;
                            return;
                        }
                    }
                }

                List<ShortcutTag> itemsToRemove = new List<ShortcutTag>();

                foreach (ShortcutTag otherSub in lstBxShortcutNames.Items)
                {
                    if (otherSub.Subscription == selectedAvailableShortcut.Subscription)
                    {
                        itemsToRemove.Add(otherSub);
                    }
                }

                foreach (var itemToRemove in itemsToRemove)
                {
                    lstBxShortcutNames.Items.Remove(itemToRemove);
                }

                // we made it passed bail, now know there are no subs and we can add this item to our group
                lstBxSelectedShortcuts.Items.Add(selectedAvailableShortcut);
                lstBxSelectedShortcuts.Sorted = true; // sort names
                availableSubscriptions.Remove(selectedAvailableShortcut.Subscription);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtBxName.Text.Length <= 0)
            {
                var msg = "You must add a group name.";
                // no shortcuts added to list, bail or warn?
                MessageBox.Show(msg);
                lblStatus.Text = msg;
                return;
            }

            if (lstBxSelectedShortcuts.Items.Count > 1)
            {
                // extract the shortcuts:
                List<ShortcutTag> items = new List<ShortcutTag>();

                foreach (ShortcutTag shortcut in lstBxSelectedShortcuts.Items)
                {
                    items.Add(shortcut);
                }

                // save items and close
                this.Tag = new LaunchGroup() { Members = items, Name = txtBxName.Text, Guid = Guid.NewGuid().ToString() };
                this.Close();
            }
            else
            {
                var msg = "You must have at least two shortcuts from different subscriptions to create a group.";
                // no shortcuts added to list, bail or warn?
                MessageBox.Show(msg);
                lblStatus.Text = msg;
            }
        }

        private void btnGroupRemove_Click(object sender, EventArgs e)
        {
            if (lstBxSelectedShortcuts.Items.Count > 0 && lstBxSelectedShortcuts.SelectedItem != null)
            {
                var convertToAvailableSub = lstBxSelectedShortcuts?.SelectedItem as ShortcutTag;

                if (convertToAvailableSub != null)
                {
                    availableSubscriptions.Add(convertToAvailableSub.Subscription);
                    lstBxShortcutNames.Items.Clear();
                    PopulateAvailableShortcuts();
                }

                lstBxSelectedShortcuts.Items.Remove(lstBxSelectedShortcuts.SelectedItem);
            }
        }
    }
}
