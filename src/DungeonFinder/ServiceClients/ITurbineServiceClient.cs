﻿using DungeonFinder.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.ServiceClients
{
    public interface ITurbineServiceClient
    {
        UserSession Authenticate(string username, string password, out string message, GameId game);

        GameInformation GetGameInformation(GameId game);

        GameInformation GetGameInformation(GameId game, bool reloadCache);

        LauncherConfiguration GetLauncherConfiguration(GameInformation serviceInformation);
    }
}
