﻿using DungeonFinder.Common;
using DungeonFinder.Models;
using DungeonFinder.Properties;
using log4net;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DungeonFinder.ServiceClients
{
    public class TurbineServiceClient : ITurbineServiceClient
    {
        private ISettingsFactory _settingsFactory;

        /// <summary>
        /// while labeled a cache, this stuff only changes if they change
        /// the order of default worlds.  it's load-once data.
        /// </summary>
        private Dictionary<GameId, GameInformation> _gameInfoCache = new Dictionary<GameId, GameInformation>();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public TurbineServiceClient(ISettingsFactory settingsFactory)
        {
            _settingsFactory = settingsFactory;
        }

        private Dictionary<GameId, List<GameServer>> ServerStatus = new Dictionary<GameId, List<GameServer>>();

        public UserSession Authenticate(string username, string password, out string message, GameId game)
        {
            Settings settings = _settingsFactory.GetCurrentSettings();

            // update cache if needed:
            GetGameInformation(game);

            string url = null;
            if (_gameInfoCache.ContainsKey(game))
                url = _gameInfoCache[game].AuthServer;

            try
            {
                // more xml.  oh stupid soap envelopes
                string soapAction = "http://www.turbine.com/SE/GLS/LoginAccount";
                string contentType = "text/xml; charset=utf-8";
                string body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><LoginAccount xmlns=\"http://www.turbine.com/SE/GLS\"><username>" + username + "</username><password>" + System.Security.SecurityElement.Escape(password) + "</password></LoginAccount></soap:Body></soap:Envelope>";
                message = null;

                RestClient client = new RestClient(url ?? settings.AuthServiceUrl);
                client.FollowRedirects = false;
                RestRequest req = new RestRequest(Method.POST);
                req.AddHeader("SOAPAction", soapAction);
                req.AddHeader("Content-Type", contentType);
#pragma warning disable CS0618 // Type or member is obsolete
                req.Parameters.Add(new Parameter("no-name", body, ParameterType.RequestBody));
#pragma warning restore CS0618 // Type or member is obsolete

                var res = client.Execute(req);

                // validate the return here rather than toss exceptions
                switch (res.StatusCode)
                {
                    // Unknown
                    case 0x00:

                        if (res?.ResponseStatus == ResponseStatus.TimedOut)
                        {
                            message = "Unknown error - a network timeout has occurred.";
                            return null;
                        }

                        if (res?.ResponseStatus == ResponseStatus.Error)
                        {
                            message = res.ErrorMessage;
                            return null;
                        }

                        break;

                    // Redirection Found:
                    case HttpStatusCode.Found:

                        message = "The servers are down; check for server maintenance.";
                        return null;

                    case HttpStatusCode.InternalServerError:
                        // this is a general catch-all code in soap land for just about anything that goes wrong.
                        if (string.IsNullOrWhiteSpace(res.Content))
                        {
                            message = "Unknown error - no data provided.";
                            return null;
                        }

                        // generic auth failure
                        if (res.Content.Contains("No Subscriber Formal Entity was found"))
                        {
                            message = "Username and/or Password incorrect.";
                            return null;
                        }

                        // attempt to parse the error message
                        dynamic error = DynamicXml.Parse(res.Content);
                        var faultString = error?.Body?.Fault?.faultstring;
                        var faultCode = error?.Body?.Fault?.faultcode;
                        message = $"{faultCode}: {faultString}";
                        return null;

                    case HttpStatusCode.OK:
                        {
                            dynamic userProfile = DynamicXml.Parse(res.Content);
                            dynamic result = userProfile?.Body?.LoginAccountResponse?.LoginAccountResult;

                            if (result == null)
                                return null;

                            var subs = result.Subscriptions.GameSubscription;
                            List<GameSubscription> gameSubs = new List<GameSubscription>();

                            if (subs is IEnumerable<object>)
                            {
                                var dynamicSubs = (subs as IEnumerable<object>).Select(s => s as dynamic).ToList();
                                foreach (var dynamicSub in dynamicSubs)
                                {
                                    var parsedSub = GameSubscription.ParseFromDynamicServiceResponse(dynamicSub);
                                    if (parsedSub != null)
                                        gameSubs.Add(parsedSub);
                                }
                            }
                            else
                            {
                                // subs is just 1
                                var dynamicSub = subs as dynamic;
                                var parsedSub = GameSubscription.ParseFromDynamicServiceResponse(dynamicSub);
                                if (parsedSub != null)
                                    gameSubs.Add(parsedSub);
                            }

                            string ticket = result.Ticket;

                            UserSession up = new UserSession(gameSubs, ticket, username);
                            return up;
                        }
                }

                message = "Unhandled http status code " + res.StatusCode;
                return null;
            }
            catch (Exception ex)
            {
                _log.Error("Error in Authenticate", ex);
                message = "Unable to Authenticate, and the error has been logged.  Please check server status pages.";
                return null;
            }
        }

        public LauncherConfiguration GetLauncherConfiguration(GameInformation serviceInformation)
        {
            // launcher stuff is just an HTTP get.
            RestClient client = new RestClient(serviceInformation.LauncherConfigurationServer);
            client.FollowRedirects = false;
            RestRequest restRequest = new RestRequest(Method.GET);
            var restResponse = client.Execute(restRequest);

            if (restResponse.StatusCode != HttpStatusCode.OK)
                return null;

            LauncherConfiguration lc = new LauncherConfiguration();
            ConfigXmlDocument doc = new ConfigXmlDocument();
            doc.Load(serviceInformation.LauncherConfigurationServer);
            foreach (XmlNode node in doc["configuration"]["appSettings"].ChildNodes)
                if (node.Attributes != null)
                {
                    if (!lc.Settings.ContainsKey(node.Attributes["key"].Value))
                        lc.Settings.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
                    // There is no good way right now for duplicate keys
                    else
                        lc.Settings.Add(node.Attributes["key"].Value + "Secondary", node.Attributes["value"].Value);
                }

            return lc;
        }

        public GameInformation GetGameInformation(GameId game, bool reloadCache)
        {
            UpdateServiceInformationCache(game);

            if (_gameInfoCache.ContainsKey(game))
                return _gameInfoCache[game];

            return null;
        }

        public GameInformation GetGameInformation(GameId game)
        {
            if (!_gameInfoCache.ContainsKey(game))
                UpdateServiceInformationCache(game);

            if (_gameInfoCache.ContainsKey(game))
                return _gameInfoCache[game];

            return null;
        }

        private void UpdateServiceInformationCache(GameId game)
        {
            Settings settings = _settingsFactory.GetCurrentSettings();
            string url = null;

            // also pull server status/info
            var servers = GetServerStatus(game);
            if (servers != null)
            {
                if (ServerStatus.ContainsKey(game))
                    ServerStatus[game] = servers;
                else
                    ServerStatus.Add(game, servers);
            }

            try
            {
                // get the game info from <game>.launcherconfig
                LocalLauncherConfig config;

                switch (game)
                {
                    case GameId.DDO:
                        config = new LocalLauncherConfig(settings.DdoPath, game.ToString());
                        url = config.GlsUrl;
                        break;
                    case GameId.LOTRO:
                        config = new LocalLauncherConfig(settings.LotroPath, game.ToString());
                        url = config.GlsUrl;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            catch
            {
                return;
            }

            // ugh, xml is evil.  why not just "{ "game": "DDO" }"?
            string body = "<?xml version=\"1.0\" encoding=\"utf - 8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><GetDatacenters xmlns=\"http://www.turbine.com/SE/GLS\"><game>" + game.ToString() + "</game></GetDatacenters></soap:Body></soap:Envelope>";

            RestClient client = new RestClient(url);
            client.FollowRedirects = false;
            RestRequest req = new RestRequest(Method.POST);
            req.AddHeader("SOAPAction", "\"http://www.turbine.com/SE/GLS/GetDatacenters\"");
            req.AddHeader("Content-Type", "text/xml; charset=utf-8");
            req.AddHeader("Connection", "Keep-Alive");
            req.AddParameter("no-name", body, ParameterType.RequestBody);

            var res = client.Execute(req);

            if (res.StatusCode != HttpStatusCode.OK)
                return;

            try
            {
                dynamic dynGameInfo = DynamicXml.Parse(res.Content);

                var dataCenter = dynGameInfo?.Body?.GetDatacentersResponse?.GetDatacentersResult?.Datacenter;
                string name = dataCenter.Name;
                List<GameWorld> worlds = new List<GameWorld>();
                var dynamicWorlds = dataCenter.Worlds.World;
                var enumerator = dynamicWorlds as System.Collections.IEnumerable;

                if (enumerator != null)
                {
                    foreach (var world in dataCenter.Worlds.World)
                    {
                        GameWorld gw = new GameWorld(world.Name, world.LoginServerUrl, world.ChatServerUrl, world.StatusServerUrl, int.Parse(world.Order), world.Language);
                        worlds.Add(gw);
                    }
                }
                else
                {
                    // there's only 1 world
                    var world = dynamicWorlds;
                    GameWorld gw = new GameWorld(world.Name, world.LoginServerUrl, world.ChatServerUrl, world.StatusServerUrl, int.Parse(world.Order), world.Language);
                    worlds.Add(gw);
                }

                var serverList = ServerStatus.GetValueOrDefault(game);

                GameInformation gi = new GameInformation(game, worlds, serverList, dataCenter.AuthServer, dataCenter.PatchServer, dataCenter.LauncherConfigurationServer);

                _gameInfoCache[game] = gi;
            }
            catch
            {
                // swallow?
            }
        }

        public List<GameServer> GetServerStatus(GameId game)
        {
            Settings settings = _settingsFactory.GetCurrentSettings();
            string url = null;

            try
            {
                // get the game info from <game>.launcherconfig
                LocalLauncherConfig config;

                switch (game)
                {
                    case GameId.DDO:
                        config = new LocalLauncherConfig(settings.DdoPath, game.ToString());
                        url = config.GlsUrl;
                        break;
                    case GameId.LOTRO:
                        config = new LocalLauncherConfig(settings.LotroPath, game.ToString());
                        url = config.GlsUrl;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            catch
            {
                return null;
            }

            // ugh, xml is evil.  why not just "{ "game": "DDO" }"?
            string soapBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gls=\"http://www.turbine.com/SE/GLS\"><soapenv:Header/><soapenv:Body><gls:GetDatacenterStatus><gls:game>" + game.ToString() + "</gls:game></gls:GetDatacenterStatus></soapenv:Body></soapenv:Envelope>";

            RestClient client = new RestClient(url);
            client.FollowRedirects = false;
            RestRequest req = new RestRequest(Method.POST);
            req.AddHeader("Accept-Encoding", "gzip");
            req.AddHeader("Accept-Language", "en-US,*");
            req.AddHeader("Content-Type", "text/xml; charset=utf-8");
            req.AddHeader("Connection", "Keep-Alive");
            req.AddHeader("User-Agent", "User Agent");
            req.AddParameter("no-name", soapBody, ParameterType.RequestBody);

            var res = client.Execute(req);

            if (res.StatusCode != HttpStatusCode.OK)
                return null;

            List<GameServer> servers = new List<GameServer>();

            try
            {
                dynamic dynGameInfo = DynamicXml.Parse(res.Content);
                foreach (dynamic d in dynGameInfo?.Body?.GetDatacenterStatusResponse?.GetDatacenterStatusResult?.StatusServerResult)
                {
                    var lastUpdated = d?.LastUpdated;
                    var serverResult = ParseStatusResult(d?.Results);
                    if (serverResult != null)
                        servers.Add(serverResult);
                }
            }
            catch
            {
                // swallow?
            }

            return servers;
        }

        private GameServer ParseStatusResult(dynamic results)
        {
            try
            {
                var status = DynamicXml.Parse(results);
                string queueurls = Convert.ToString(status?.queueurls);
                var lastassignedqueuenumber = Convert.ToInt32(status?.lastassignedqueuenumber, 16);
                bool world_full = status?.world_full == "true";
                var wait_hint = Convert.ToDouble(status?.wait_hint);
                var we_perma_death = status?.we_perma_death == "true";
                var nowservingqueuenumber = Convert.ToInt32(status?.nowservingqueuenumber, 16);
                string loginservers = Convert.ToString(status?.loginservers);
                var world_pvppermission = status?.world_pvppermission == "1";
                var pvp_world_event = status?.we_pvp_war_event_control == "True" ?? false;
                var we_server_legendary_active = status?.we_server_legendary_active == "True" ?? false;
                // not used:
                // TODO: Research login tier numbers, to see if we can check which queue is smaller?
                //var numbers = status?.logintierlastnumbers;
                //var tiers = status?.logintiers;
                //var queuenames = status?.queuenames;
                //var allow_billing_role = status?.allow_billing_role;
                //var farmid = status?.farmid;
                //var deny_admin_role = status?.deny_admin_role;
                //var allow_admin_role = status?.allow_admin_role;
                //var deny_billing_role = status?.deny_billing_role;
                //var logintiermultipliers = status?.logintiermultipliers;

                GameServer gameServer = new GameServer();
                gameServer.Name = status?.name;

                if (queueurls != null)
                {
                    var qList = queueurls.Split(';').ToList();
                    qList = qList.Where(q => !string.IsNullOrWhiteSpace(q)).Distinct().ToList();
                    gameServer.QueueUrls = qList;
                }

                if (loginservers != null)
                {
                    var lList = loginservers.Split(';').ToList();
                    lList = lList.Where(l => !string.IsNullOrWhiteSpace(l)).Distinct().ToList();
                    gameServer.LoginServers = lList;
                }

                gameServer.LastAssignedQueueNumber = lastassignedqueuenumber;
                gameServer.PVP = world_pvppermission;
                gameServer.NowServingQueueNumber = nowservingqueuenumber;
                gameServer.WorldFull = world_full;
                gameServer.WaitHint = wait_hint;
                gameServer.Hardcore = we_perma_death;
                gameServer.Legendary = we_server_legendary_active;
                gameServer.PVPWorldEvent = pvp_world_event;
                //gameServer.Legendary = we_perma_death;

                return gameServer;
            }
            catch
            {
                return null;
            }
        }
    }
}
