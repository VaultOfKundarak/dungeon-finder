﻿using DungeonFinder.Common;
using DungeonFinder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.ServiceClients
{
    public class GameInformation
    {
        public GameId Game { get; private set; }

        public List<GameWorld> Worlds { get; private set; } = new List<GameWorld>();

        public List<GameServer> Servers { get; private set; } = new List<GameServer>();

        public string AuthServer { get; private set; }

        public string PatchServer { get; private set; }

        public string LauncherConfigurationServer { get; private set; }

        public GameInformation(GameId game, List<GameWorld> worlds, List<GameServer> servers, string authServer, string patchServer, string launcherConfigurationServer)
        {
            Game = game;
            Worlds = worlds;
            AuthServer = authServer;
            PatchServer = patchServer;
            LauncherConfigurationServer = launcherConfigurationServer;
            Servers = servers;
        }
    }
}
