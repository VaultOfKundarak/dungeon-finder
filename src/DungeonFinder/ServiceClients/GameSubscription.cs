﻿using DungeonFinder.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonFinder.ServiceClients
{
    public class GameSubscription
    {
        public GameId Game { get; private set; }

        public string Id { get; private set; }

        public string Description { get; private set; }

        public string Status { get; private set; }

        public string BillingSystemTimeUtc { get; private set; }

        private List<string> _productTokens;

        public IReadOnlyList<string> ProductTokens
        {
            get { return _productTokens; }
        }

        public static GameSubscription ParseFromDynamicServiceResponse(dynamic sub)
        {
            if (!Enum.TryParse(sub.Game, out GameId gameId))
                return null;

            string id = sub.Name;
            string description = sub.Description;
            string status = sub.Status;
            var billingSystemTime = (string)sub.BillingSystemTime;
            var productTokensElem = sub.Root.Element("ProductTokens").FirstNode;
            List<string> productTokens = new List<string>();

            while (productTokensElem != null)
            {
                productTokens.Add(productTokensElem.Value);
                productTokensElem = productTokensElem.NextNode;
            }

            GameSubscription gs = new GameSubscription(gameId, id, description, status, billingSystemTime, productTokens);

            return gs;
        }

        public GameSubscription(GameId game,
                                string id,
                                string description,
                                string status,
                                string billingSystemTimeUtc,
                                List<string> productTokens)
        {
            Game = game;
            Id = id;
            Description = description;
            Status = status;
            BillingSystemTimeUtc = billingSystemTimeUtc;
            _productTokens = productTokens;
        }
    }
}
