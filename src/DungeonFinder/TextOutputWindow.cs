﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace DungeonFinder
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "RabidSquirrel.")]
    public partial class TextOutputWindow : Form
    {
        private bool _pauseAfterExecution;

        public TextOutputWindow()
        {
            InitializeComponent();
            DialogResult = DialogResult.OK;
        }

        public TextOutputWindow(string confirmText, string confirmButtonText = null, string cancelButtonText = null)
        {
            InitializeComponent();

            // This prevents the dialog box from closing until the "Okay/Continue" button is pressed.
            DialogResult = DialogResult.Retry;

            // this allows it to toggle?
            _pauseAfterExecution = true;

            if (!string.IsNullOrWhiteSpace(confirmButtonText))
                btnOkay.Text = confirmButtonText;

            tbPatchOutput.Text += confirmText;

            Button btnCancel = new Button();

            // add cancel/no button:
            btnCancel.Anchor = ((AnchorStyles)((AnchorStyles.Top | AnchorStyles.Right)));
            btnCancel.BackColor = Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            btnCancel.Cursor = Cursors.Hand;
            btnCancel.FlatAppearance.BorderSize = 0;
            btnCancel.Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancel.ForeColor = Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            btnCancel.Location = new Point(497, 12);
            btnCancel.Margin = new Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(190, 33);
            btnCancel.TabIndex = 21;
            if (!string.IsNullOrWhiteSpace(cancelButtonText))
                btnCancel.Text = cancelButtonText;
            else
                btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = false;
            btnCancel.Click += new EventHandler(this.btnCancel_Click);

            scMain.Panel2.Controls.Add(btnCancel);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public TextOutputWindow(bool pauseAfterExecution, string buttonText = null, string startingText = null)
        {
            InitializeComponent();

            if (startingText != null)
            {
                tbPatchOutput.Text += startingText;
            }

            // This prevents the dialog box from closing until the "Okay/Continue" button is pressed.
            if (pauseAfterExecution)
            {
                _pauseAfterExecution = true;
                DialogResult = DialogResult.Retry;
            }
            else
            {
                // Setting this causes no wait after execution:
                DialogResult = DialogResult.OK;
            }

            if (!string.IsNullOrWhiteSpace(buttonText))
                btnOkay.Text = buttonText;
        }

        public static void ShowDialog(string dialogText, string buttonText)
        {
            var window = new TextOutputWindow(true, buttonText);
            window.tbPatchOutput.Text = dialogText;
            window.btnOkay.Text = buttonText;
            window.ShowDialog();
        }

        public void EnableButtons()
        {
            if (DialogResult == DialogResult.Retry || DialogResult == DialogResult.None)
            {
                btnOkay.Text = "Continue";
                _pauseAfterExecution = true;
            }
        }

        public void AppendPatchChar(char text)
        {
#if DEBUG
            Debug.Write(text);
#endif
            tbPatchOutput.Invoke((MethodInvoker)(() =>
            {
                tbPatchOutput.AppendText(text.ToString());
            }));

            System.Windows.Forms.Application.DoEvents();
        }

        public void AppendString(string text)
        {
#if DEBUG
            Debug.Write(text);
#endif
            tbPatchOutput.Invoke((MethodInvoker)(() =>
            {
                tbPatchOutput.AppendText(text);
            }));

            System.Windows.Forms.Application.DoEvents();
        }

        private void btnOkay_Click(object sender, EventArgs e)
        {
            if (_pauseAfterExecution)
                // Changes to OK, to close the window from the parent thread.
                DialogResult = DialogResult.OK;
        }

        private void scHeaderPanel1_OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MainForm.ReleaseCapture();
                MainForm.SendMessage(Handle, MainForm.WM_NCLBUTTONDOWN, MainForm.HT_CAPTION, null);
            }
        }
    }
}
