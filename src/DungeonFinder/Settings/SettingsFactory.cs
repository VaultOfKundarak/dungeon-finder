﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DungeonFinder
{
    public class SettingsFactory : ISettingsFactory
    {
        private string _settingsFile;
        private Settings _settings;
        private FileSystemWatcher _settingsFileWatcher;

        public event EventHandler OnChange;

        public SettingsFactory(string settingsFile)
        {
            _settingsFile = settingsFile;
            _settingsFileWatcher = new FileSystemWatcher(Path.GetDirectoryName(_settingsFile), Path.GetFileName(_settingsFile));
            _settingsFileWatcher.Changed += new FileSystemEventHandler(settingsFile_OnChange);
            ReloadSettings();
        }

        public void ImportSettingsFile(string settingsFile)
        {
            Settings newSettings = null;
            try
            {
                // so, this is a bit dangerous.  we're deserializing right over this, breaking
                // all object references that pointed to the old one.  I'm half tempted to ditch
                // the file watcher if this becomes a problem.
                newSettings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(settingsFile));
            }
            catch (Exception exception)
            {
                Console.WriteLine("An exception occurred while loading the configuration file!");
                Console.WriteLine($"Exception: {exception.Message}");

                // environment.exit swallows this exception for testing purposes.  we want to expose it.
                throw;
            }

            // only import previous settings files that do not have a version.
            if (newSettings != null && (newSettings.Version == null || newSettings.Version < 1))
            {
                // upgrading version to base of "1"
                newSettings.Version = 1;
                SaveSettings(newSettings);
            }
        }

        private void settingsFile_OnChange(object sender, FileSystemEventArgs e)
        {
            ReloadSettings();
        }

        public Settings GetCurrentSettings() { return _settings; }

        private void ReloadSettings()
        {
            try
            {
                // so, this is a bit dangerous.  we're deserializing right over this, breaking
                // all object references that pointed to the old one.  I'm half tempted to ditch
                // the file watcher if this becomes a problem.
                _settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(_settingsFile));
            }
            catch (FileNotFoundException)
            {
                _settings = new Settings();
            }
            catch (Exception exception)
            {
                Console.WriteLine("An exception occurred while loading the configuration file!");
                Console.WriteLine($"Exception: {exception.Message}");

                // environment.exit swallows this exception for testing purposes.  we want to expose it.
                throw;
            }
        }

        public void SaveSettings(Settings settings)
        {
            _settings = settings;
            File.WriteAllText(_settingsFile, JsonConvert.SerializeObject(settings));
            OnChange?.Invoke(null, null);
        }
    }
}
