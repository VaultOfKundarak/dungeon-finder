﻿using System;

namespace DungeonFinder
{
    public interface ISettingsFactory
    {
        Settings GetCurrentSettings();

        void ImportSettingsFile(string settingsFile);

        void SaveSettings(Settings settings);

        event EventHandler OnChange;
    }
}