﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using DungeonFinder.Models;
using Newtonsoft.Json;

namespace DungeonFinder
{
    public class Settings
    {
        [JsonIgnore]
        private string _ddoServiceInfoUrl { get; set; }
        [JsonProperty("ddoServiceInfoUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("http://gls.ddo.com/GLS.DataCenterServer/Service.asmx")]
        public string DdoServiceInfoUrl
        {
            get => string.IsNullOrWhiteSpace(_ddoServiceInfoUrl) ? "http://gls.ddo.com/GLS.DataCenterServer/Service.asmx" : _ddoServiceInfoUrl;
            set => _ddoServiceInfoUrl = value;
        }

        [JsonIgnore]
        private string _lotroServiceInfoUrl { get; set; }
        [JsonProperty("lotroServiceInfoUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("http://gls.lotro.com/GLS.DataCenterServer/Service.asmx")]
        public string LotroServiceInfoUrl
        {
            get => string.IsNullOrWhiteSpace(_lotroServiceInfoUrl) ? "http://gls.lotro.com/GLS.DataCenterServer/Service.asmx" : _lotroServiceInfoUrl;
            set => _lotroServiceInfoUrl = value;
        }

        [JsonIgnore]
        private string _authServiceUrl { get; set; }
        /// <summary>
        /// ddo or lotro.com, same service under the hood | one auth server may be down while the other is up however... make sure to try both if necessary
        /// </summary>
        [JsonProperty("authServiceUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("https://gls-auth.ddo.com/GLS.AuthServer/Service.asmx")]
        public string AuthServiceUrl
        {
            get => string.IsNullOrWhiteSpace(_authServiceUrl) ? "https://gls-auth.ddo.com/GLS.AuthServer/Service.asmx" : _authServiceUrl;
            set => _authServiceUrl = value;
        }

        [JsonProperty("enableLogging", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(false)]
        public bool EnableLogging { get; set; }

        [JsonProperty("ddoPath")]
        public string DdoPath { get; set; }

        [JsonProperty("lotroPath")]
        public string LotroPath { get; set; }

        [JsonProperty("storedAccounts")]
        public List<StoredAccount> StoredAccounts { get; set; } = new List<StoredAccount>();

        [JsonProperty("patchBeforeLaunch")]
        public bool PatchBeforeLaunch { get; set; } = false;

        [JsonProperty("minimizeToTray")]
        public bool MinimizeToTray { get; set; } = true;

        [JsonProperty("enableNotifications")]
        [DefaultValue(true)]
        public bool EnableNotifications { get; set; } = true;

        [JsonProperty("use64BitClients")]
        [DefaultValue(true)]
        public bool Use64BitClients { get; set; } = true;

        [JsonProperty("checkForPatchMinutes")]
        [DefaultValue(null)]
        public int? CheckForPatchMinutes { get; set; }

        [JsonProperty("minimizeBeforeLaunch")]
        [DefaultValue(false)]
        public bool MinimizeBeforeLaunch { get; set; } = false;

        [JsonProperty("showCommandLineArgumentsButton")]
        [DefaultValue(false)]
        public bool ShowAdvancedLaunchOptions { get; set; } = false;

        [JsonProperty("allowDiagnostics")]
        [DefaultValue(false)]
        public bool AllowDiagnostics { get; set; } = false;

        [JsonProperty("hideDDOSub")]
        [DefaultValue(false)]
        public bool HideDDOSub { get; set; } = false;

        [JsonProperty("hideLOTROSub")]
        [DefaultValue(false)]
        public bool HideLOTROSub { get; set; } = false;

        [JsonProperty("version", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(0)]
        public int? Version { get; set; }

        [JsonProperty("checkForUpdates", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(true)]
        public bool CheckForUpdates { get; set; } = true;

        [JsonProperty("groupings", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(null)]
        public List<LaunchGroup> Groupings { get; set; }
    }
}
