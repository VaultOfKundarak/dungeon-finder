﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DungeonFinder.Properties;

namespace DungeonFinder
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "RabidSquirrel.")]
    public class TrayForm : IDisposable
    {
        private readonly NotifyIcon icon;

        public bool Visible
        {
            private get { return icon.Visible; }
            set { icon.Visible = value; }
        }

        private Icon AppIcon { get; set; }

        private readonly Bitmap _offIcon = new Bitmap(Resources.SimpleTrayInactive);

        private readonly Bitmap _onIcon = new Bitmap(Resources.SimpleTrayActive);

        private bool IsIconActive;

        private IntPtr iconHandle;

        // Updates the Main tray icon with the appropriate status icon (on or off)
        public void ToggleTrayIcon(bool? overrideActive = null)
        {
            IsIconActive = overrideActive ?? !IsIconActive;

            iconHandle = IsIconActive ? _onIcon.GetHicon() : _offIcon.GetHicon();

            AppIcon = Icon.FromHandle(iconHandle);

            // First initialize will be null
            if (icon != null)
            {
                icon.Icon = AppIcon;
            }
        }

        public void PopupNotification(string text, string title)
        {
            icon.BalloonTipIcon = ToolTipIcon.Info;
            icon.BalloonTipText = text;
            icon.BalloonTipTitle = title;
            icon.ShowBalloonTip(5000);
        }

        private void OnMenuChange()
        {
            icon.ContextMenuStrip = Menu.MainMenu;
        }

        // Initializes the tray icon
        public TrayForm()
        {
            IsIconActive = true;
            ToggleTrayIcon();
            icon = new NotifyIcon
            {
                Icon = AppIcon,
                Visible = true,
                Text = "Dungeon Finder",
                ContextMenuStrip = Menu.Initialize(OnMenuChange)
            };
            icon.MouseClick += icon_MouseClick;
            icon.MouseDoubleClick += icon_MouseDoubleClick;
        }

        // Show the context menu
        private void icon_MouseClick(object sender, MouseEventArgs e)
        {
            // Find and use the same action as the right click event:
            if (e.Button == MouseButtons.Left)
            {
                var MouseClick_Right = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                if (MouseClick_Right != null)
                    MouseClick_Right.Invoke(icon, null);
            }
        }

        private void icon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Program.MainForm.WindowState = FormWindowState.Normal;
                Program.MainForm.Show();
                Program.MainForm.BringToFront();
            }
        }


        public void Dispose()
        {
            icon.Dispose();
            AppIcon.Dispose();
        }
    }
}
