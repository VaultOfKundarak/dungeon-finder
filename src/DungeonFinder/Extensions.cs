﻿using DungeonFinder.Common;
using DungeonFinder.Models;
using DungeonFinder.Properties;
using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shortcut = DungeonFinder.Models.Shortcut;

namespace DungeonFinder
{
    class Extensions
    {
        public static string[] GetServerNamesFromResources(GameId game)
        {
            string[] gameServers = new string[] { };

            switch (game)
            {
                case GameId.DDO:
                    if (!string.IsNullOrWhiteSpace(Resources.DDOServers))
                    {
                        gameServers = Resources.DDOServers.Split(new char[] { ',' });
                    }
                    break;
                case GameId.LOTRO:
                    if (!string.IsNullOrWhiteSpace(Resources.LOTROServers))
                    {
                        gameServers = Resources.LOTROServers.Split(new char[] { ',' });
                    }
                    break;
            }
            return gameServers;
        }

        public static void CreateWindowsGroupShortcut(GroupTag group)
        {
            // group name:
            var shortcutName = "Launch Group " + group.Group.Name;
            string shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); ; // desktop.
            string targetFileLocation = string.Empty;

            // strip invalid filename characters
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                if (c == ':')
                {
                    shortcutName = shortcutName.Replace(":", " -");
                }
                else
                {
                    shortcutName = shortcutName.Replace(c, '_');
                }
            }

            string shortcutLocation = Path.Combine(shortcutPath, shortcutName + ".lnk");
            WshShell shell = new WshShell();
            IWshShortcut shortcutFile = (IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcutFile.Description = "Dungeon Finder " + group.Group.Name + " Quicklaunch";   // The description of the shortcut

            // resolve the path to the assmebly and verify the icon exists:
            string codeBase = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var asemblyDirectory = Path.GetDirectoryName(path);
            targetFileLocation = Path.Combine(asemblyDirectory, "DungeonFinder.exe");

            if (System.IO.File.Exists(shortcutLocation))
            {
                var result = MessageBox.Show($"The shortcut you are attempting to create already exists, would you like to overwrite the current shortcut on your desktop?", "Warning!", MessageBoxButtons.YesNo);
                if (result != DialogResult.Yes)
                    return;
            }

            var iconFileName = "vok_logo.ico";
            var iconPath = Path.Combine(asemblyDirectory, iconFileName);
            if (System.IO.File.Exists(iconPath))
                shortcutFile.IconLocation = iconPath;
            // attempt to load from default appdata directory if icon is still missing:
            if (string.IsNullOrWhiteSpace(shortcutFile.IconLocation) || shortcutFile.IconLocation == ",0")
            {
                var expandedAppdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Dungeon Finder";
                var defaultIconLocation = Path.Combine(expandedAppdata, iconFileName);
                if (System.IO.File.Exists(defaultIconLocation))
                {
                    shortcutFile.IconLocation = defaultIconLocation;        // set default from known appdata source
                }
            }
            shortcutFile.WorkingDirectory = asemblyDirectory;               // set the working directory so the executable can find the settings.json configuration
            shortcutFile.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcutFile.Arguments = "-g " + group.Group.Guid;             // shortcut arguments
            shortcutFile.Save();                                    // Save the shortcut
            MessageBox.Show("Desktop shortcut has been created.", "Dungeon Finder");
        }

        public static void CreateWindowsShortcut(ShortcutTag shortcutTag)
        {
            Shortcut shortcut = shortcutTag.Shortcut;

            var shortcutName = shortcut.DisplayName;
            string shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); // desktop.
            string targetFileLocation = string.Empty;

            // force a shortcut name
            if (string.IsNullOrWhiteSpace(shortcutName))
            {
                shortcutName = shortcutTag.ToString();
            }

            // strip invalid filename characters
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                if (c == ':')
                {
                    shortcutName = shortcutName.Replace(":", " -");
                }
                else
                {
                    shortcutName = shortcutName.Replace(c, '_');
                }
            }

            shortcutName = "Launch " + shortcutName;

            string shortcutLocation = Path.Combine(shortcutPath, shortcutName + ".lnk");
            WshShell shell = new WshShell();
            IWshShortcut shortcutFile = (IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcutFile.Description = "Dungeon Finder " + shortcutTag.ToString() + " Quicklaunch";   // The description of the shortcut

            // resolve the path to the assembly and verify the icon exists:
            string codeBase = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var asemblyDirectory = Path.GetDirectoryName(path);
            targetFileLocation = Path.Combine(asemblyDirectory, "DungeonFinder.exe");

            if (System.IO.File.Exists(shortcutLocation))
            {
                var result = MessageBox.Show($"The shortcut you are attempting to create already exists, would you like to overwrite the current shortcut on your desktop?", "Warning!", MessageBoxButtons.YesNo);
                if (result != DialogResult.Yes)
                    return;
            }

            var iconFileName = "vok_logo.ico";
            var iconPath = Path.Combine(asemblyDirectory, iconFileName);
            if (System.IO.File.Exists(iconPath))
                shortcutFile.IconLocation = iconPath;
            // attempt to load from default appdata directory if icon is still missing:
            if (string.IsNullOrWhiteSpace(shortcutFile.IconLocation) || shortcutFile.IconLocation == ",0")
            {
                var expandedAppdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Dungeon Finder";
                var defaultIconLocation = Path.Combine(expandedAppdata, iconFileName);
                if (System.IO.File.Exists(defaultIconLocation))
                {
                    shortcutFile.IconLocation = defaultIconLocation;        // set default from known appdata source
                }
            }
            shortcutFile.WorkingDirectory = asemblyDirectory;               // set the working directory so the executable can find the settings.json configuration
            shortcutFile.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcutFile.Arguments = "-i " + shortcut.Guid;             // shortcut arguments
            shortcutFile.Save();                                    // Save the shortcut
            MessageBox.Show("Desktop shortcut has been created.", "Dungeon Finder");
        }

        public static string StripServerFlags(string serverNameString)
        {
            return Regex.Replace(serverNameString, @"\s*\[.*?\]\s*", " ").Trim();
        }
    }
}
