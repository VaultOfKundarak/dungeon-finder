﻿namespace DungeonFinder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            scHeader = new System.Windows.Forms.SplitContainer();
            btnMinimize = new System.Windows.Forms.Button();
            btnClose = new System.Windows.Forms.Button();
            lblTitle = new System.Windows.Forms.Label();
            scNav = new System.Windows.Forms.SplitContainer();
            btnAccounts = new System.Windows.Forms.Button();
            btnSettings = new System.Windows.Forms.Button();
            btnHome = new System.Windows.Forms.Button();
            scMain = new System.Windows.Forms.SplitContainer();
            pAccounts = new System.Windows.Forms.TableLayoutPanel();
            label3 = new System.Windows.Forms.Label();
            pSettings = new System.Windows.Forms.Panel();
            btnMainSettings = new System.Windows.Forms.Button();
            pHome = new System.Windows.Forms.Panel();
            btnYouTube = new System.Windows.Forms.Button();
            btnMyDDO = new System.Windows.Forms.Button();
            btnDiscord = new System.Windows.Forms.Button();
            btnWeb = new System.Windows.Forms.Button();
            pUserSettings = new System.Windows.Forms.Panel();
            cbUpdateOnStart = new System.Windows.Forms.CheckBox();
            cbHideLOTROSub = new System.Windows.Forms.CheckBox();
            cbHideDDOSub = new System.Windows.Forms.CheckBox();
            cbAllowDiagnostics = new System.Windows.Forms.CheckBox();
            cbMinimizeToTray = new System.Windows.Forms.CheckBox();
            cbShowAdvancedOptionsButton = new System.Windows.Forms.CheckBox();
            cbStartWithWindows = new System.Windows.Forms.CheckBox();
            cb64bit = new System.Windows.Forms.CheckBox();
            cbMinimizeOnLaunch = new System.Windows.Forms.CheckBox();
            cbEnablePatchNotifications = new System.Windows.Forms.CheckBox();
            cbAutoPatch = new System.Windows.Forms.CheckBox();
            btnVersionInfo = new System.Windows.Forms.Button();
            btnUpdate = new System.Windows.Forms.Button();
            btnLOTROPatchNow = new System.Windows.Forms.Button();
            btnDDOPatchNow = new System.Windows.Forms.Button();
            btnFindLotro = new System.Windows.Forms.Button();
            btnFindDdo = new System.Windows.Forms.Button();
            lblLotroPath = new System.Windows.Forms.Label();
            btnUserSettingsSave = new System.Windows.Forms.Button();
            txtBxLOTROPath = new System.Windows.Forms.TextBox();
            txtBxDDOPath = new System.Windows.Forms.TextBox();
            lblDdoPath = new System.Windows.Forms.Label();
            pEditAccount = new System.Windows.Forms.Panel();
            btnRemoveAccount = new System.Windows.Forms.Button();
            tbEditDisplayName = new System.Windows.Forms.TextBox();
            lEditDisplay = new System.Windows.Forms.Label();
            btnEditAccountCancel = new System.Windows.Forms.Button();
            btnEditAccountSave = new System.Windows.Forms.Button();
            tbEditPassword = new System.Windows.Forms.TextBox();
            lEditPassword = new System.Windows.Forms.Label();
            tbEditUsername = new System.Windows.Forms.TextBox();
            lEditUsername = new System.Windows.Forms.Label();
            scAccountDetails = new System.Windows.Forms.SplitContainer();
            btnCreateGroup = new System.Windows.Forms.Button();
            tbShortcutDisplayName = new System.Windows.Forms.TextBox();
            btnCancelShortcut = new System.Windows.Forms.Button();
            btnAddShortcut = new System.Windows.Forms.Button();
            tbAddShortcutCharacter = new System.Windows.Forms.TextBox();
            cbMakeDefault = new System.Windows.Forms.CheckBox();
            btnPlay = new System.Windows.Forms.Button();
            cbPlayServers = new System.Windows.Forms.ComboBox();
            scAccountDetailsLower = new System.Windows.Forms.SplitContainer();
            pShortcuts = new System.Windows.Forms.Panel();
            lblShortcuts = new System.Windows.Forms.Label();
            btnPreferences = new System.Windows.Forms.Button();
            btnEditSubSave = new System.Windows.Forms.Button();
            tbEditSubDisplayName = new System.Windows.Forms.TextBox();
            lblEditSubDisplayName = new System.Windows.Forms.Label();
            lblSubscriptionInfo = new System.Windows.Forms.Label();
            pMessages = new System.Windows.Forms.Panel();
            btnMessaagesOkay = new System.Windows.Forms.Button();
            lblMessage = new System.Windows.Forms.Label();
            pAddAccount = new System.Windows.Forms.Panel();
            tbAccountDisplayName = new System.Windows.Forms.TextBox();
            lDisplayName = new System.Windows.Forms.Label();
            btnAccountCancel = new System.Windows.Forms.Button();
            btnAccountSave = new System.Windows.Forms.Button();
            tbAccountPassword = new System.Windows.Forms.TextBox();
            lPassword = new System.Windows.Forms.Label();
            tbAccountUsername = new System.Windows.Forms.TextBox();
            lUsername = new System.Windows.Forms.Label();
            pMainMenu = new System.Windows.Forms.Panel();
            btnRefreshStatus = new System.Windows.Forms.Button();
            lblServerStatus = new System.Windows.Forms.Label();
            ilMain = new System.Windows.Forms.ImageList(components);
            cmsDDO = new System.Windows.Forms.ContextMenuStrip(components);
            cmsLotro = new System.Windows.Forms.ContextMenuStrip(components);
            pUnsavedChanges = new System.Windows.Forms.Panel();
            pUnsavedChangesDialog = new System.Windows.Forms.Panel();
            btnDiscardUnsavedChanges = new System.Windows.Forms.Button();
            btnSaveUnsavedChanges = new System.Windows.Forms.Button();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            optionsTT = new System.Windows.Forms.ToolTip(components);
            tmrOrganizeAccounts = new System.Windows.Forms.Timer(components);
            cmsAdvancedOptions = new AdvancedOptionsMenuStrip(components);
            getCommandLineArgumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            copyShortcutIdShortcutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            createDesktopShortcutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            hideThisShortcutFromTrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            showThisShortcutInTrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            assignCustomPreferencesFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            updateCustomPreferencesFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)scHeader).BeginInit();
            scHeader.Panel1.SuspendLayout();
            scHeader.Panel2.SuspendLayout();
            scHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scNav).BeginInit();
            scNav.Panel1.SuspendLayout();
            scNav.Panel2.SuspendLayout();
            scNav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scMain).BeginInit();
            scMain.Panel1.SuspendLayout();
            scMain.Panel2.SuspendLayout();
            scMain.SuspendLayout();
            pAccounts.SuspendLayout();
            pSettings.SuspendLayout();
            pHome.SuspendLayout();
            pUserSettings.SuspendLayout();
            pEditAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scAccountDetails).BeginInit();
            scAccountDetails.Panel1.SuspendLayout();
            scAccountDetails.Panel2.SuspendLayout();
            scAccountDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scAccountDetailsLower).BeginInit();
            scAccountDetailsLower.Panel1.SuspendLayout();
            scAccountDetailsLower.Panel2.SuspendLayout();
            scAccountDetailsLower.SuspendLayout();
            pShortcuts.SuspendLayout();
            pMessages.SuspendLayout();
            pAddAccount.SuspendLayout();
            pMainMenu.SuspendLayout();
            pUnsavedChanges.SuspendLayout();
            pUnsavedChangesDialog.SuspendLayout();
            cmsAdvancedOptions.SuspendLayout();
            SuspendLayout();
            // 
            // scHeader
            // 
            scHeader.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            scHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            scHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            scHeader.IsSplitterFixed = true;
            scHeader.Location = new System.Drawing.Point(0, 0);
            scHeader.Margin = new System.Windows.Forms.Padding(1);
            scHeader.Name = "scHeader";
            scHeader.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scHeader.Panel1
            // 
            scHeader.Panel1.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            scHeader.Panel1.Controls.Add(btnMinimize);
            scHeader.Panel1.Controls.Add(btnClose);
            scHeader.Panel1.Controls.Add(lblTitle);
            scHeader.Panel1.MouseDown += scHeader_Panel1_MouseDown;
            scHeader.Panel1MinSize = 20;
            // 
            // scHeader.Panel2
            // 
            scHeader.Panel2.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            scHeader.Panel2.Controls.Add(scNav);
            scHeader.Size = new System.Drawing.Size(1924, 1061);
            scHeader.SplitterDistance = 54;
            scHeader.SplitterWidth = 1;
            scHeader.TabIndex = 0;
            // 
            // btnMinimize
            // 
            btnMinimize.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            btnMinimize.FlatAppearance.BorderSize = 0;
            btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnMinimize.Image = (System.Drawing.Image)resources.GetObject("btnMinimize.Image");
            btnMinimize.Location = new System.Drawing.Point(1874, 5);
            btnMinimize.Name = "btnMinimize";
            btnMinimize.Size = new System.Drawing.Size(18, 19);
            btnMinimize.TabIndex = 3;
            btnMinimize.UseVisualStyleBackColor = true;
            btnMinimize.Click += btnMinimize_Click;
            // 
            // btnClose
            // 
            btnClose.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            btnClose.FlatAppearance.BorderSize = 0;
            btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnClose.Image = (System.Drawing.Image)resources.GetObject("btnClose.Image");
            btnClose.Location = new System.Drawing.Point(1898, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(18, 19);
            btnClose.TabIndex = 1;
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // lblTitle
            // 
            lblTitle.AutoSize = true;
            lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            lblTitle.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            lblTitle.Location = new System.Drawing.Point(2, 3);
            lblTitle.Name = "lblTitle";
            lblTitle.Size = new System.Drawing.Size(147, 17);
            lblTitle.TabIndex = 0;
            lblTitle.Text = "DUNGEON FINDER";
            lblTitle.MouseDown += lblTitle_MouseDown;
            // 
            // scNav
            // 
            scNav.BackColor = System.Drawing.Color.FromArgb(16, 79, 153);
            scNav.Dock = System.Windows.Forms.DockStyle.Fill;
            scNav.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            scNav.IsSplitterFixed = true;
            scNav.Location = new System.Drawing.Point(0, 0);
            scNav.Margin = new System.Windows.Forms.Padding(0);
            scNav.Name = "scNav";
            // 
            // scNav.Panel1
            // 
            scNav.Panel1.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            scNav.Panel1.Controls.Add(btnAccounts);
            scNav.Panel1.Controls.Add(btnSettings);
            scNav.Panel1.Controls.Add(btnHome);
            scNav.Panel1MinSize = 46;
            // 
            // scNav.Panel2
            // 
            scNav.Panel2.BackColor = System.Drawing.Color.FromArgb(16, 79, 153);
            scNav.Panel2.Controls.Add(scMain);
            scNav.Size = new System.Drawing.Size(1922, 1004);
            scNav.SplitterDistance = 46;
            scNav.SplitterWidth = 1;
            scNav.TabIndex = 0;
            // 
            // btnAccounts
            // 
            btnAccounts.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAccounts.FlatAppearance.BorderSize = 0;
            btnAccounts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnAccounts.Image = (System.Drawing.Image)resources.GetObject("btnAccounts.Image");
            btnAccounts.Location = new System.Drawing.Point(0, 0);
            btnAccounts.Margin = new System.Windows.Forms.Padding(0);
            btnAccounts.Name = "btnAccounts";
            btnAccounts.Size = new System.Drawing.Size(46, 50);
            btnAccounts.TabIndex = 2;
            btnAccounts.UseVisualStyleBackColor = false;
            btnAccounts.Click += btnAccounts_Click;
            // 
            // btnSettings
            // 
            btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            btnSettings.FlatAppearance.BorderSize = 0;
            btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnSettings.Image = (System.Drawing.Image)resources.GetObject("btnSettings.Image");
            btnSettings.Location = new System.Drawing.Point(0, 50);
            btnSettings.Margin = new System.Windows.Forms.Padding(0);
            btnSettings.Name = "btnSettings";
            btnSettings.Size = new System.Drawing.Size(46, 50);
            btnSettings.TabIndex = 1;
            btnSettings.UseVisualStyleBackColor = false;
            btnSettings.Click += btnSettings_Click;
            // 
            // btnHome
            // 
            btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            btnHome.FlatAppearance.BorderSize = 0;
            btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnHome.Image = (System.Drawing.Image)resources.GetObject("btnHome.Image");
            btnHome.Location = new System.Drawing.Point(0, 100);
            btnHome.Margin = new System.Windows.Forms.Padding(0);
            btnHome.Name = "btnHome";
            btnHome.Size = new System.Drawing.Size(46, 50);
            btnHome.TabIndex = 0;
            btnHome.UseVisualStyleBackColor = false;
            btnHome.Click += btnHome_Click;
            // 
            // scMain
            // 
            scMain.BackColor = System.Drawing.Color.FromArgb(16, 79, 153);
            scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            scMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            scMain.IsSplitterFixed = true;
            scMain.Location = new System.Drawing.Point(0, 0);
            scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            scMain.Panel1.BackColor = System.Drawing.Color.FromArgb(16, 79, 153);
            scMain.Panel1.Controls.Add(pAccounts);
            scMain.Panel1.Controls.Add(pSettings);
            scMain.Panel1.Controls.Add(pHome);
            // 
            // scMain.Panel2
            // 
            scMain.Panel2.BackColor = System.Drawing.Color.FromArgb(243, 252, 254);
            scMain.Panel2.Controls.Add(pUserSettings);
            scMain.Panel2.Controls.Add(pEditAccount);
            scMain.Panel2.Controls.Add(scAccountDetails);
            scMain.Panel2.Controls.Add(pMessages);
            scMain.Panel2.Controls.Add(pAddAccount);
            scMain.Panel2.Controls.Add(pMainMenu);
            scMain.Size = new System.Drawing.Size(1875, 1004);
            scMain.SplitterDistance = 240;
            scMain.SplitterWidth = 1;
            scMain.TabIndex = 0;
            // 
            // pAccounts
            // 
            pAccounts.ColumnCount = 1;
            pAccounts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            pAccounts.Controls.Add(label3, 0, 0);
            pAccounts.Location = new System.Drawing.Point(9, 303);
            pAccounts.Name = "pAccounts";
            pAccounts.RowCount = 1;
            pAccounts.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pAccounts.Size = new System.Drawing.Size(200, 100);
            pAccounts.TabIndex = 7;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            label3.ForeColor = System.Drawing.Color.White;
            label3.Location = new System.Drawing.Point(3, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(139, 17);
            label3.TabIndex = 1;
            label3.Text = "Populated at runtime";
            // 
            // pSettings
            // 
            pSettings.Controls.Add(btnMainSettings);
            pSettings.Location = new System.Drawing.Point(1, 500);
            pSettings.Margin = new System.Windows.Forms.Padding(0);
            pSettings.Name = "pSettings";
            pSettings.Size = new System.Drawing.Size(224, 137);
            pSettings.TabIndex = 6;
            // 
            // btnMainSettings
            // 
            btnMainSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            btnMainSettings.FlatAppearance.BorderSize = 0;
            btnMainSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnMainSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnMainSettings.ForeColor = System.Drawing.Color.White;
            btnMainSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnMainSettings.Location = new System.Drawing.Point(0, 0);
            btnMainSettings.Margin = new System.Windows.Forms.Padding(0);
            btnMainSettings.Name = "btnMainSettings";
            btnMainSettings.Size = new System.Drawing.Size(224, 40);
            btnMainSettings.TabIndex = 3;
            btnMainSettings.Text = "Settings";
            btnMainSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnMainSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnMainSettings.UseVisualStyleBackColor = false;
            btnMainSettings.Click += btnMainSettings_Click;
            // 
            // pHome
            // 
            pHome.Controls.Add(btnYouTube);
            pHome.Controls.Add(btnMyDDO);
            pHome.Controls.Add(btnDiscord);
            pHome.Controls.Add(btnWeb);
            pHome.Location = new System.Drawing.Point(0, 0);
            pHome.Margin = new System.Windows.Forms.Padding(0);
            pHome.Name = "pHome";
            pHome.Size = new System.Drawing.Size(262, 269);
            pHome.TabIndex = 5;
            // 
            // btnYouTube
            // 
            btnYouTube.Cursor = System.Windows.Forms.Cursors.Hand;
            btnYouTube.FlatAppearance.BorderSize = 0;
            btnYouTube.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnYouTube.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            btnYouTube.ForeColor = System.Drawing.Color.White;
            btnYouTube.Image = (System.Drawing.Image)resources.GetObject("btnYouTube.Image");
            btnYouTube.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnYouTube.Location = new System.Drawing.Point(-3, 150);
            btnYouTube.Margin = new System.Windows.Forms.Padding(0);
            btnYouTube.Name = "btnYouTube";
            btnYouTube.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            btnYouTube.Size = new System.Drawing.Size(224, 50);
            btnYouTube.TabIndex = 5;
            btnYouTube.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnYouTube.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnYouTube.UseVisualStyleBackColor = false;
            btnYouTube.Click += btnYouTube_Click;
            // 
            // btnMyDDO
            // 
            btnMyDDO.Cursor = System.Windows.Forms.Cursors.Hand;
            btnMyDDO.FlatAppearance.BorderSize = 0;
            btnMyDDO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnMyDDO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            btnMyDDO.ForeColor = System.Drawing.Color.White;
            btnMyDDO.Image = (System.Drawing.Image)resources.GetObject("btnMyDDO.Image");
            btnMyDDO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnMyDDO.Location = new System.Drawing.Point(-3, 50);
            btnMyDDO.Margin = new System.Windows.Forms.Padding(0);
            btnMyDDO.Name = "btnMyDDO";
            btnMyDDO.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            btnMyDDO.Size = new System.Drawing.Size(224, 50);
            btnMyDDO.TabIndex = 4;
            btnMyDDO.Text = " MyDDO Website";
            btnMyDDO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnMyDDO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnMyDDO.UseVisualStyleBackColor = false;
            btnMyDDO.Click += btnMyDDO_Click;
            // 
            // btnDiscord
            // 
            btnDiscord.Cursor = System.Windows.Forms.Cursors.Hand;
            btnDiscord.FlatAppearance.BorderSize = 0;
            btnDiscord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnDiscord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            btnDiscord.ForeColor = System.Drawing.Color.White;
            btnDiscord.Image = (System.Drawing.Image)resources.GetObject("btnDiscord.Image");
            btnDiscord.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnDiscord.Location = new System.Drawing.Point(-3, 100);
            btnDiscord.Margin = new System.Windows.Forms.Padding(0);
            btnDiscord.Name = "btnDiscord";
            btnDiscord.Size = new System.Drawing.Size(224, 50);
            btnDiscord.TabIndex = 2;
            btnDiscord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnDiscord.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnDiscord.UseVisualStyleBackColor = false;
            btnDiscord.Click += btnDiscord_Click;
            // 
            // btnWeb
            // 
            btnWeb.Cursor = System.Windows.Forms.Cursors.Hand;
            btnWeb.FlatAppearance.BorderSize = 0;
            btnWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnWeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            btnWeb.ForeColor = System.Drawing.Color.White;
            btnWeb.Image = (System.Drawing.Image)resources.GetObject("btnWeb.Image");
            btnWeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnWeb.Location = new System.Drawing.Point(2, 0);
            btnWeb.Margin = new System.Windows.Forms.Padding(0);
            btnWeb.Name = "btnWeb";
            btnWeb.Size = new System.Drawing.Size(224, 50);
            btnWeb.TabIndex = 1;
            btnWeb.Text = "Dungeon Finder Website";
            btnWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnWeb.UseVisualStyleBackColor = false;
            btnWeb.Click += btnWeb_Click;
            // 
            // pUserSettings
            // 
            pUserSettings.Controls.Add(cbUpdateOnStart);
            pUserSettings.Controls.Add(cbHideLOTROSub);
            pUserSettings.Controls.Add(cbHideDDOSub);
            pUserSettings.Controls.Add(cbAllowDiagnostics);
            pUserSettings.Controls.Add(cbMinimizeToTray);
            pUserSettings.Controls.Add(cbShowAdvancedOptionsButton);
            pUserSettings.Controls.Add(cbStartWithWindows);
            pUserSettings.Controls.Add(cb64bit);
            pUserSettings.Controls.Add(cbMinimizeOnLaunch);
            pUserSettings.Controls.Add(cbEnablePatchNotifications);
            pUserSettings.Controls.Add(cbAutoPatch);
            pUserSettings.Controls.Add(btnVersionInfo);
            pUserSettings.Controls.Add(btnUpdate);
            pUserSettings.Controls.Add(btnLOTROPatchNow);
            pUserSettings.Controls.Add(btnDDOPatchNow);
            pUserSettings.Controls.Add(btnFindLotro);
            pUserSettings.Controls.Add(btnFindDdo);
            pUserSettings.Controls.Add(lblLotroPath);
            pUserSettings.Controls.Add(btnUserSettingsSave);
            pUserSettings.Controls.Add(txtBxLOTROPath);
            pUserSettings.Controls.Add(txtBxDDOPath);
            pUserSettings.Controls.Add(lblDdoPath);
            pUserSettings.Location = new System.Drawing.Point(13, 573);
            pUserSettings.Name = "pUserSettings";
            pUserSettings.Size = new System.Drawing.Size(607, 420);
            pUserSettings.TabIndex = 6;
            // 
            // cbUpdateOnStart
            // 
            cbUpdateOnStart.AutoSize = true;
            cbUpdateOnStart.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbUpdateOnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbUpdateOnStart.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbUpdateOnStart.Location = new System.Drawing.Point(139, 336);
            cbUpdateOnStart.Name = "cbUpdateOnStart";
            cbUpdateOnStart.Size = new System.Drawing.Size(196, 21);
            cbUpdateOnStart.TabIndex = 42;
            cbUpdateOnStart.Text = "Check for updates on Start";
            optionsTT.SetToolTip(cbUpdateOnStart, "Perform an Auto-Update check when starting the application.");
            cbUpdateOnStart.UseVisualStyleBackColor = true;
            // 
            // cbHideLOTROSub
            // 
            cbHideLOTROSub.AutoSize = true;
            cbHideLOTROSub.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbHideLOTROSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbHideLOTROSub.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbHideLOTROSub.Location = new System.Drawing.Point(51, 310);
            cbHideLOTROSub.Name = "cbHideLOTROSub";
            cbHideLOTROSub.Size = new System.Drawing.Size(284, 21);
            cbHideLOTROSub.TabIndex = 41;
            cbHideLOTROSub.Text = "Hide LOTRO Subscriptions in Side Menu";
            optionsTT.SetToolTip(cbHideLOTROSub, "Hide LOTRO subscriptions for each account in the side menu.");
            cbHideLOTROSub.UseVisualStyleBackColor = true;
            // 
            // cbHideDDOSub
            // 
            cbHideDDOSub.AutoSize = true;
            cbHideDDOSub.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbHideDDOSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbHideDDOSub.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbHideDDOSub.Location = new System.Drawing.Point(69, 283);
            cbHideDDOSub.Name = "cbHideDDOSub";
            cbHideDDOSub.Size = new System.Drawing.Size(266, 21);
            cbHideDDOSub.TabIndex = 40;
            cbHideDDOSub.Text = "Hide DDO Subscriptions in Side Menu";
            optionsTT.SetToolTip(cbHideDDOSub, "Hide DDO subscriptions for each account in the side menu.");
            cbHideDDOSub.UseVisualStyleBackColor = true;
            // 
            // cbAllowDiagnostics
            // 
            cbAllowDiagnostics.AutoSize = true;
            cbAllowDiagnostics.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbAllowDiagnostics.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbAllowDiagnostics.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbAllowDiagnostics.Location = new System.Drawing.Point(199, 256);
            cbAllowDiagnostics.Name = "cbAllowDiagnostics";
            cbAllowDiagnostics.Size = new System.Drawing.Size(136, 21);
            cbAllowDiagnostics.TabIndex = 39;
            cbAllowDiagnostics.Text = "Allow Diagnostics";
            optionsTT.SetToolTip(cbAllowDiagnostics, "Show extra options to send diagnostic data to RabidSquirrel.");
            cbAllowDiagnostics.UseVisualStyleBackColor = true;
            // 
            // cbMinimizeToTray
            // 
            cbMinimizeToTray.AutoSize = true;
            cbMinimizeToTray.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbMinimizeToTray.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbMinimizeToTray.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbMinimizeToTray.Location = new System.Drawing.Point(162, 94);
            cbMinimizeToTray.Name = "cbMinimizeToTray";
            cbMinimizeToTray.Size = new System.Drawing.Size(173, 21);
            cbMinimizeToTray.TabIndex = 38;
            cbMinimizeToTray.Text = "Minimize to system tray";
            optionsTT.SetToolTip(cbMinimizeToTray, "Minimize Dungeon Finder to the system tray instead of closing");
            cbMinimizeToTray.UseVisualStyleBackColor = true;
            // 
            // cbShowAdvancedOptionsButton
            // 
            cbShowAdvancedOptionsButton.AutoSize = true;
            cbShowAdvancedOptionsButton.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbShowAdvancedOptionsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbShowAdvancedOptionsButton.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbShowAdvancedOptionsButton.Location = new System.Drawing.Point(68, 229);
            cbShowAdvancedOptionsButton.Name = "cbShowAdvancedOptionsButton";
            cbShowAdvancedOptionsButton.Size = new System.Drawing.Size(267, 21);
            cbShowAdvancedOptionsButton.TabIndex = 37;
            cbShowAdvancedOptionsButton.Text = "Show advanced launch options button";
            optionsTT.SetToolTip(cbShowAdvancedOptionsButton, "Show the command line argument buttons on the Launch & Accounts page (used for 3D modeling)");
            cbShowAdvancedOptionsButton.UseVisualStyleBackColor = true;
            // 
            // cbStartWithWindows
            // 
            cbStartWithWindows.AutoSize = true;
            cbStartWithWindows.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbStartWithWindows.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbStartWithWindows.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbStartWithWindows.Location = new System.Drawing.Point(159, 202);
            cbStartWithWindows.Name = "cbStartWithWindows";
            cbStartWithWindows.Size = new System.Drawing.Size(176, 21);
            cbStartWithWindows.TabIndex = 36;
            cbStartWithWindows.Text = "Auto start with Windows";
            optionsTT.SetToolTip(cbStartWithWindows, "Start Dungeon Finder when Windows starts");
            cbStartWithWindows.UseVisualStyleBackColor = true;
            cbStartWithWindows.CheckedChanged += cbStartWithWindows_CheckedChanged;
            // 
            // cb64bit
            // 
            cb64bit.AutoSize = true;
            cb64bit.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cb64bit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cb64bit.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cb64bit.Location = new System.Drawing.Point(189, 148);
            cb64bit.Name = "cb64bit";
            cb64bit.Size = new System.Drawing.Size(146, 21);
            cb64bit.TabIndex = 35;
            cb64bit.Text = "Use 64-bit client(s)";
            optionsTT.SetToolTip(cb64bit, "Use the 64-bit client");
            cb64bit.UseVisualStyleBackColor = true;
            // 
            // cbMinimizeOnLaunch
            // 
            cbMinimizeOnLaunch.AutoSize = true;
            cbMinimizeOnLaunch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbMinimizeOnLaunch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbMinimizeOnLaunch.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbMinimizeOnLaunch.Location = new System.Drawing.Point(137, 175);
            cbMinimizeOnLaunch.Name = "cbMinimizeOnLaunch";
            cbMinimizeOnLaunch.Size = new System.Drawing.Size(198, 21);
            cbMinimizeOnLaunch.TabIndex = 34;
            cbMinimizeOnLaunch.Text = "Minimize main UI on launch";
            optionsTT.SetToolTip(cbMinimizeOnLaunch, "Minimize the application when launched");
            cbMinimizeOnLaunch.UseVisualStyleBackColor = true;
            // 
            // cbEnablePatchNotifications
            // 
            cbEnablePatchNotifications.AutoSize = true;
            cbEnablePatchNotifications.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbEnablePatchNotifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbEnablePatchNotifications.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbEnablePatchNotifications.Location = new System.Drawing.Point(108, 121);
            cbEnablePatchNotifications.Name = "cbEnablePatchNotifications";
            cbEnablePatchNotifications.Size = new System.Drawing.Size(227, 21);
            cbEnablePatchNotifications.TabIndex = 33;
            cbEnablePatchNotifications.Text = "Enable tray pop-up notifications";
            optionsTT.SetToolTip(cbEnablePatchNotifications, "Enable tray pop-up notifications");
            cbEnablePatchNotifications.UseVisualStyleBackColor = true;
            // 
            // cbAutoPatch
            // 
            cbAutoPatch.AutoSize = true;
            cbAutoPatch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            cbAutoPatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbAutoPatch.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            cbAutoPatch.Location = new System.Drawing.Point(142, 67);
            cbAutoPatch.Name = "cbAutoPatch";
            cbAutoPatch.Size = new System.Drawing.Size(193, 21);
            cbAutoPatch.TabIndex = 32;
            cbAutoPatch.Text = "Patch before every launch";
            optionsTT.SetToolTip(cbAutoPatch, "Patch the game client each time the game is launched");
            cbAutoPatch.UseVisualStyleBackColor = true;
            // 
            // btnVersionInfo
            // 
            btnVersionInfo.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnVersionInfo.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnVersionInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            btnVersionInfo.FlatAppearance.BorderSize = 0;
            btnVersionInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnVersionInfo.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnVersionInfo.Location = new System.Drawing.Point(216, 360);
            btnVersionInfo.Margin = new System.Windows.Forms.Padding(0);
            btnVersionInfo.Name = "btnVersionInfo";
            btnVersionInfo.Size = new System.Drawing.Size(180, 38);
            btnVersionInfo.TabIndex = 30;
            btnVersionInfo.Text = "Get Version Info";
            btnVersionInfo.UseVisualStyleBackColor = false;
            btnVersionInfo.Click += btnVersionInfo_Click;
            // 
            // btnUpdate
            // 
            btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdate.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            btnUpdate.FlatAppearance.BorderSize = 0;
            btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnUpdate.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnUpdate.Location = new System.Drawing.Point(37, 360);
            btnUpdate.Margin = new System.Windows.Forms.Padding(0);
            btnUpdate.Name = "btnUpdate";
            btnUpdate.Size = new System.Drawing.Size(167, 38);
            btnUpdate.TabIndex = 27;
            btnUpdate.Text = "Check for Updates";
            btnUpdate.UseVisualStyleBackColor = false;
            btnUpdate.Click += btnUpdate_Click;
            // 
            // btnLOTROPatchNow
            // 
            btnLOTROPatchNow.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnLOTROPatchNow.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnLOTROPatchNow.Cursor = System.Windows.Forms.Cursors.Hand;
            btnLOTROPatchNow.FlatAppearance.BorderSize = 0;
            btnLOTROPatchNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnLOTROPatchNow.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnLOTROPatchNow.Location = new System.Drawing.Point(490, 31);
            btnLOTROPatchNow.Margin = new System.Windows.Forms.Padding(0);
            btnLOTROPatchNow.Name = "btnLOTROPatchNow";
            btnLOTROPatchNow.Size = new System.Drawing.Size(98, 29);
            btnLOTROPatchNow.TabIndex = 21;
            btnLOTROPatchNow.Text = "Patch Now";
            btnLOTROPatchNow.UseVisualStyleBackColor = false;
            btnLOTROPatchNow.Click += btnLOTROPatchNow_Click;
            // 
            // btnDDOPatchNow
            // 
            btnDDOPatchNow.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnDDOPatchNow.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnDDOPatchNow.Cursor = System.Windows.Forms.Cursors.Hand;
            btnDDOPatchNow.FlatAppearance.BorderSize = 0;
            btnDDOPatchNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnDDOPatchNow.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnDDOPatchNow.Location = new System.Drawing.Point(490, 2);
            btnDDOPatchNow.Margin = new System.Windows.Forms.Padding(0);
            btnDDOPatchNow.Name = "btnDDOPatchNow";
            btnDDOPatchNow.Size = new System.Drawing.Size(98, 29);
            btnDDOPatchNow.TabIndex = 20;
            btnDDOPatchNow.Text = "Patch Now";
            btnDDOPatchNow.UseVisualStyleBackColor = false;
            btnDDOPatchNow.Click += btnDDOPatchNow_Click;
            // 
            // btnFindLotro
            // 
            btnFindLotro.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnFindLotro.Cursor = System.Windows.Forms.Cursors.Hand;
            btnFindLotro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnFindLotro.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnFindLotro.Location = new System.Drawing.Point(345, 34);
            btnFindLotro.Name = "btnFindLotro";
            btnFindLotro.Size = new System.Drawing.Size(37, 24);
            btnFindLotro.TabIndex = 18;
            btnFindLotro.Text = "...";
            btnFindLotro.UseVisualStyleBackColor = false;
            btnFindLotro.Click += btnFindLotro_Click;
            // 
            // btnFindDdo
            // 
            btnFindDdo.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnFindDdo.Cursor = System.Windows.Forms.Cursors.Hand;
            btnFindDdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnFindDdo.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnFindDdo.Location = new System.Drawing.Point(345, 5);
            btnFindDdo.Name = "btnFindDdo";
            btnFindDdo.Size = new System.Drawing.Size(37, 24);
            btnFindDdo.TabIndex = 17;
            btnFindDdo.Text = "...";
            btnFindDdo.UseVisualStyleBackColor = false;
            btnFindDdo.Click += btnFindDdo_Click;
            // 
            // lblLotroPath
            // 
            lblLotroPath.AutoSize = true;
            lblLotroPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblLotroPath.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lblLotroPath.Location = new System.Drawing.Point(3, 37);
            lblLotroPath.Name = "lblLotroPath";
            lblLotroPath.Size = new System.Drawing.Size(90, 17);
            lblLotroPath.TabIndex = 16;
            lblLotroPath.Text = "LOTRO Path";
            // 
            // btnUserSettingsSave
            // 
            btnUserSettingsSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUserSettingsSave.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnUserSettingsSave.Cursor = System.Windows.Forms.Cursors.Hand;
            btnUserSettingsSave.FlatAppearance.BorderSize = 0;
            btnUserSettingsSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnUserSettingsSave.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnUserSettingsSave.Location = new System.Drawing.Point(508, 360);
            btnUserSettingsSave.Margin = new System.Windows.Forms.Padding(0);
            btnUserSettingsSave.Name = "btnUserSettingsSave";
            btnUserSettingsSave.Size = new System.Drawing.Size(80, 38);
            btnUserSettingsSave.TabIndex = 15;
            btnUserSettingsSave.Text = "Save";
            btnUserSettingsSave.UseVisualStyleBackColor = false;
            btnUserSettingsSave.Click += btnUserSettingsSave_Click;
            // 
            // txtBxLOTROPath
            // 
            txtBxLOTROPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            txtBxLOTROPath.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            txtBxLOTROPath.Location = new System.Drawing.Point(118, 34);
            txtBxLOTROPath.Name = "txtBxLOTROPath";
            txtBxLOTROPath.Size = new System.Drawing.Size(221, 23);
            txtBxLOTROPath.TabIndex = 8;
            // 
            // txtBxDDOPath
            // 
            txtBxDDOPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            txtBxDDOPath.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            txtBxDDOPath.Location = new System.Drawing.Point(118, 5);
            txtBxDDOPath.Name = "txtBxDDOPath";
            txtBxDDOPath.Size = new System.Drawing.Size(221, 23);
            txtBxDDOPath.TabIndex = 7;
            // 
            // lblDdoPath
            // 
            lblDdoPath.AutoSize = true;
            lblDdoPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblDdoPath.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lblDdoPath.Location = new System.Drawing.Point(3, 8);
            lblDdoPath.Name = "lblDdoPath";
            lblDdoPath.Size = new System.Drawing.Size(72, 17);
            lblDdoPath.TabIndex = 0;
            lblDdoPath.Text = "DDO Path";
            // 
            // pEditAccount
            // 
            pEditAccount.Controls.Add(btnRemoveAccount);
            pEditAccount.Controls.Add(tbEditDisplayName);
            pEditAccount.Controls.Add(lEditDisplay);
            pEditAccount.Controls.Add(btnEditAccountCancel);
            pEditAccount.Controls.Add(btnEditAccountSave);
            pEditAccount.Controls.Add(tbEditPassword);
            pEditAccount.Controls.Add(lEditPassword);
            pEditAccount.Controls.Add(tbEditUsername);
            pEditAccount.Controls.Add(lEditUsername);
            pEditAccount.Location = new System.Drawing.Point(1214, 173);
            pEditAccount.Margin = new System.Windows.Forms.Padding(0);
            pEditAccount.Name = "pEditAccount";
            pEditAccount.Size = new System.Drawing.Size(394, 314);
            pEditAccount.TabIndex = 3;
            pEditAccount.Visible = false;
            // 
            // btnRemoveAccount
            // 
            btnRemoveAccount.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnRemoveAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            btnRemoveAccount.FlatAppearance.BorderSize = 0;
            btnRemoveAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnRemoveAccount.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnRemoveAccount.Location = new System.Drawing.Point(282, 96);
            btnRemoveAccount.Margin = new System.Windows.Forms.Padding(0);
            btnRemoveAccount.Name = "btnRemoveAccount";
            btnRemoveAccount.Size = new System.Drawing.Size(80, 38);
            btnRemoveAccount.TabIndex = 7;
            btnRemoveAccount.Text = "Remove";
            btnRemoveAccount.UseVisualStyleBackColor = false;
            btnRemoveAccount.Click += btnRemoveAccount_Click;
            // 
            // tbEditDisplayName
            // 
            tbEditDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbEditDisplayName.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbEditDisplayName.Location = new System.Drawing.Point(122, 39);
            tbEditDisplayName.Name = "tbEditDisplayName";
            tbEditDisplayName.Size = new System.Drawing.Size(240, 23);
            tbEditDisplayName.TabIndex = 2;
            // 
            // lEditDisplay
            // 
            lEditDisplay.AutoSize = true;
            lEditDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lEditDisplay.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lEditDisplay.Location = new System.Drawing.Point(3, 42);
            lEditDisplay.Name = "lEditDisplay";
            lEditDisplay.Size = new System.Drawing.Size(95, 17);
            lEditDisplay.TabIndex = 6;
            lEditDisplay.Text = "Display Name";
            // 
            // btnEditAccountCancel
            // 
            btnEditAccountCancel.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnEditAccountCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            btnEditAccountCancel.FlatAppearance.BorderSize = 0;
            btnEditAccountCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnEditAccountCancel.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnEditAccountCancel.Location = new System.Drawing.Point(202, 96);
            btnEditAccountCancel.Margin = new System.Windows.Forms.Padding(0);
            btnEditAccountCancel.Name = "btnEditAccountCancel";
            btnEditAccountCancel.Size = new System.Drawing.Size(80, 38);
            btnEditAccountCancel.TabIndex = 5;
            btnEditAccountCancel.Text = "Cancel";
            btnEditAccountCancel.UseVisualStyleBackColor = false;
            btnEditAccountCancel.Click += btnEditAccountCancel_Click;
            // 
            // btnEditAccountSave
            // 
            btnEditAccountSave.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnEditAccountSave.Cursor = System.Windows.Forms.Cursors.Hand;
            btnEditAccountSave.FlatAppearance.BorderSize = 0;
            btnEditAccountSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnEditAccountSave.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnEditAccountSave.Location = new System.Drawing.Point(122, 96);
            btnEditAccountSave.Margin = new System.Windows.Forms.Padding(0);
            btnEditAccountSave.Name = "btnEditAccountSave";
            btnEditAccountSave.Size = new System.Drawing.Size(80, 38);
            btnEditAccountSave.TabIndex = 4;
            btnEditAccountSave.Text = "Save";
            btnEditAccountSave.UseVisualStyleBackColor = false;
            btnEditAccountSave.Click += btnEditAccountSave_Click;
            // 
            // tbEditPassword
            // 
            tbEditPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbEditPassword.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbEditPassword.Location = new System.Drawing.Point(122, 69);
            tbEditPassword.Name = "tbEditPassword";
            tbEditPassword.PasswordChar = '*';
            tbEditPassword.Size = new System.Drawing.Size(240, 23);
            tbEditPassword.TabIndex = 3;
            // 
            // lEditPassword
            // 
            lEditPassword.AutoSize = true;
            lEditPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lEditPassword.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lEditPassword.Location = new System.Drawing.Point(3, 72);
            lEditPassword.Name = "lEditPassword";
            lEditPassword.Size = new System.Drawing.Size(69, 17);
            lEditPassword.TabIndex = 2;
            lEditPassword.Text = "Password";
            // 
            // tbEditUsername
            // 
            tbEditUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbEditUsername.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbEditUsername.Location = new System.Drawing.Point(122, 9);
            tbEditUsername.Name = "tbEditUsername";
            tbEditUsername.ReadOnly = true;
            tbEditUsername.Size = new System.Drawing.Size(240, 23);
            tbEditUsername.TabIndex = 1;
            // 
            // lEditUsername
            // 
            lEditUsername.AutoSize = true;
            lEditUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lEditUsername.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lEditUsername.Location = new System.Drawing.Point(3, 12);
            lEditUsername.Name = "lEditUsername";
            lEditUsername.Size = new System.Drawing.Size(73, 17);
            lEditUsername.TabIndex = 0;
            lEditUsername.Text = "Username";
            // 
            // scAccountDetails
            // 
            scAccountDetails.Location = new System.Drawing.Point(3, 3);
            scAccountDetails.Name = "scAccountDetails";
            scAccountDetails.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAccountDetails.Panel1
            // 
            scAccountDetails.Panel1.Controls.Add(btnCreateGroup);
            scAccountDetails.Panel1.Controls.Add(tbShortcutDisplayName);
            scAccountDetails.Panel1.Controls.Add(btnCancelShortcut);
            scAccountDetails.Panel1.Controls.Add(btnAddShortcut);
            scAccountDetails.Panel1.Controls.Add(tbAddShortcutCharacter);
            scAccountDetails.Panel1.Controls.Add(cbMakeDefault);
            scAccountDetails.Panel1.Controls.Add(btnPlay);
            scAccountDetails.Panel1.Controls.Add(cbPlayServers);
            scAccountDetails.Panel1MinSize = 45;
            // 
            // scAccountDetails.Panel2
            // 
            scAccountDetails.Panel2.Controls.Add(scAccountDetailsLower);
            scAccountDetails.Size = new System.Drawing.Size(589, 500);
            scAccountDetails.SplitterDistance = 105;
            scAccountDetails.TabIndex = 5;
            scAccountDetails.Visible = false;
            // 
            // btnCreateGroup
            // 
            btnCreateGroup.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnCreateGroup.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnCreateGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            btnCreateGroup.FlatAppearance.BorderSize = 0;
            btnCreateGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnCreateGroup.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnCreateGroup.Location = new System.Drawing.Point(449, 38);
            btnCreateGroup.Margin = new System.Windows.Forms.Padding(0);
            btnCreateGroup.Name = "btnCreateGroup";
            btnCreateGroup.Size = new System.Drawing.Size(136, 30);
            btnCreateGroup.TabIndex = 14;
            btnCreateGroup.Text = "Create Group";
            btnCreateGroup.UseVisualStyleBackColor = false;
            btnCreateGroup.Visible = false;
            btnCreateGroup.Click += btnCreateGroup_Click;
            // 
            // tbShortcutDisplayName
            // 
            tbShortcutDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbShortcutDisplayName.Location = new System.Drawing.Point(6, 72);
            tbShortcutDisplayName.Name = "tbShortcutDisplayName";
            tbShortcutDisplayName.Size = new System.Drawing.Size(247, 23);
            tbShortcutDisplayName.TabIndex = 13;
            // 
            // btnCancelShortcut
            // 
            btnCancelShortcut.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnCancelShortcut.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnCancelShortcut.Cursor = System.Windows.Forms.Cursors.Hand;
            btnCancelShortcut.FlatAppearance.BorderSize = 0;
            btnCancelShortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnCancelShortcut.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnCancelShortcut.Location = new System.Drawing.Point(351, 68);
            btnCancelShortcut.Margin = new System.Windows.Forms.Padding(0);
            btnCancelShortcut.Name = "btnCancelShortcut";
            btnCancelShortcut.Size = new System.Drawing.Size(80, 30);
            btnCancelShortcut.TabIndex = 12;
            btnCancelShortcut.Text = "Cancel";
            btnCancelShortcut.UseVisualStyleBackColor = false;
            btnCancelShortcut.Visible = false;
            btnCancelShortcut.Click += btnCancelShortcut_Click;
            // 
            // btnAddShortcut
            // 
            btnAddShortcut.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnAddShortcut.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnAddShortcut.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAddShortcut.FlatAppearance.BorderSize = 0;
            btnAddShortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnAddShortcut.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnAddShortcut.Location = new System.Drawing.Point(431, 68);
            btnAddShortcut.Margin = new System.Windows.Forms.Padding(0);
            btnAddShortcut.Name = "btnAddShortcut";
            btnAddShortcut.Size = new System.Drawing.Size(154, 30);
            btnAddShortcut.TabIndex = 11;
            btnAddShortcut.Text = "Create Shortcut";
            btnAddShortcut.UseVisualStyleBackColor = false;
            btnAddShortcut.Click += tbnAddUpdateShortcut_Click;
            // 
            // tbAddShortcutCharacter
            // 
            tbAddShortcutCharacter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbAddShortcutCharacter.Location = new System.Drawing.Point(6, 42);
            tbAddShortcutCharacter.Name = "tbAddShortcutCharacter";
            tbAddShortcutCharacter.Size = new System.Drawing.Size(247, 23);
            tbAddShortcutCharacter.TabIndex = 10;
            // 
            // cbMakeDefault
            // 
            cbMakeDefault.AutoSize = true;
            cbMakeDefault.Location = new System.Drawing.Point(259, 16);
            cbMakeDefault.Name = "cbMakeDefault";
            cbMakeDefault.Size = new System.Drawing.Size(93, 17);
            cbMakeDefault.TabIndex = 6;
            cbMakeDefault.Text = "Set as Default";
            cbMakeDefault.UseVisualStyleBackColor = true;
            // 
            // btnPlay
            // 
            btnPlay.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnPlay.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            btnPlay.FlatAppearance.BorderSize = 0;
            btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnPlay.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnPlay.Location = new System.Drawing.Point(479, 8);
            btnPlay.Margin = new System.Windows.Forms.Padding(0);
            btnPlay.Name = "btnPlay";
            btnPlay.Size = new System.Drawing.Size(107, 30);
            btnPlay.TabIndex = 5;
            btnPlay.Text = "Play Now!";
            btnPlay.UseVisualStyleBackColor = false;
            btnPlay.Click += btnPlay_Click;
            // 
            // cbPlayServers
            // 
            cbPlayServers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cbPlayServers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbPlayServers.FormattingEnabled = true;
            cbPlayServers.Location = new System.Drawing.Point(6, 12);
            cbPlayServers.Name = "cbPlayServers";
            cbPlayServers.Size = new System.Drawing.Size(247, 24);
            cbPlayServers.TabIndex = 0;
            // 
            // scAccountDetailsLower
            // 
            scAccountDetailsLower.Dock = System.Windows.Forms.DockStyle.Fill;
            scAccountDetailsLower.Location = new System.Drawing.Point(0, 0);
            scAccountDetailsLower.Name = "scAccountDetailsLower";
            scAccountDetailsLower.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAccountDetailsLower.Panel1
            // 
            scAccountDetailsLower.Panel1.Controls.Add(pShortcuts);
            // 
            // scAccountDetailsLower.Panel2
            // 
            scAccountDetailsLower.Panel2.Controls.Add(btnPreferences);
            scAccountDetailsLower.Panel2.Controls.Add(btnEditSubSave);
            scAccountDetailsLower.Panel2.Controls.Add(tbEditSubDisplayName);
            scAccountDetailsLower.Panel2.Controls.Add(lblEditSubDisplayName);
            scAccountDetailsLower.Panel2.Controls.Add(lblSubscriptionInfo);
            scAccountDetailsLower.Panel2MinSize = 75;
            scAccountDetailsLower.Size = new System.Drawing.Size(589, 391);
            scAccountDetailsLower.SplitterDistance = 312;
            scAccountDetailsLower.TabIndex = 0;
            // 
            // pShortcuts
            // 
            pShortcuts.AutoScroll = true;
            pShortcuts.Controls.Add(lblShortcuts);
            pShortcuts.Location = new System.Drawing.Point(0, 0);
            pShortcuts.Name = "pShortcuts";
            pShortcuts.Size = new System.Drawing.Size(585, 308);
            pShortcuts.TabIndex = 0;
            // 
            // lblShortcuts
            // 
            lblShortcuts.AutoSize = true;
            lblShortcuts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            lblShortcuts.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lblShortcuts.Location = new System.Drawing.Point(3, 3);
            lblShortcuts.Name = "lblShortcuts";
            lblShortcuts.Size = new System.Drawing.Size(206, 20);
            lblShortcuts.TabIndex = 10;
            lblShortcuts.Text = "Shortcuts (Click to Play!)";
            // 
            // btnPreferences
            // 
            btnPreferences.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnPreferences.Cursor = System.Windows.Forms.Cursors.Hand;
            btnPreferences.FlatAppearance.BorderSize = 0;
            btnPreferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnPreferences.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnPreferences.Location = new System.Drawing.Point(479, 31);
            btnPreferences.Margin = new System.Windows.Forms.Padding(0);
            btnPreferences.Name = "btnPreferences";
            btnPreferences.Size = new System.Drawing.Size(106, 38);
            btnPreferences.TabIndex = 11;
            btnPreferences.Text = "Preferences";
            btnPreferences.UseVisualStyleBackColor = false;
            btnPreferences.Click += btnPreferences_Click;
            // 
            // btnEditSubSave
            // 
            btnEditSubSave.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnEditSubSave.Cursor = System.Windows.Forms.Cursors.Hand;
            btnEditSubSave.FlatAppearance.BorderSize = 0;
            btnEditSubSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnEditSubSave.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnEditSubSave.Location = new System.Drawing.Point(384, 31);
            btnEditSubSave.Margin = new System.Windows.Forms.Padding(0);
            btnEditSubSave.Name = "btnEditSubSave";
            btnEditSubSave.Size = new System.Drawing.Size(80, 38);
            btnEditSubSave.TabIndex = 10;
            btnEditSubSave.Text = "Save";
            btnEditSubSave.UseVisualStyleBackColor = false;
            btnEditSubSave.Click += btnEditSubSave_Click;
            // 
            // tbEditSubDisplayName
            // 
            tbEditSubDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbEditSubDisplayName.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbEditSubDisplayName.Location = new System.Drawing.Point(101, 39);
            tbEditSubDisplayName.Name = "tbEditSubDisplayName";
            tbEditSubDisplayName.Size = new System.Drawing.Size(277, 23);
            tbEditSubDisplayName.TabIndex = 9;
            // 
            // lblEditSubDisplayName
            // 
            lblEditSubDisplayName.AutoSize = true;
            lblEditSubDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblEditSubDisplayName.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lblEditSubDisplayName.Location = new System.Drawing.Point(4, 42);
            lblEditSubDisplayName.Name = "lblEditSubDisplayName";
            lblEditSubDisplayName.Size = new System.Drawing.Size(95, 17);
            lblEditSubDisplayName.TabIndex = 8;
            lblEditSubDisplayName.Text = "Display Name";
            // 
            // lblSubscriptionInfo
            // 
            lblSubscriptionInfo.AutoSize = true;
            lblSubscriptionInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblSubscriptionInfo.ForeColor = System.Drawing.Color.Gray;
            lblSubscriptionInfo.Location = new System.Drawing.Point(10, 10);
            lblSubscriptionInfo.Name = "lblSubscriptionInfo";
            lblSubscriptionInfo.Size = new System.Drawing.Size(181, 17);
            lblSubscriptionInfo.TabIndex = 7;
            lblSubscriptionInfo.Text = "Subscription Info goes here";
            lblSubscriptionInfo.Click += lblSubscriptionInfo_Click;
            // 
            // pMessages
            // 
            pMessages.Controls.Add(btnMessaagesOkay);
            pMessages.Controls.Add(lblMessage);
            pMessages.Location = new System.Drawing.Point(1119, 626);
            pMessages.Margin = new System.Windows.Forms.Padding(0);
            pMessages.Name = "pMessages";
            pMessages.Size = new System.Drawing.Size(360, 142);
            pMessages.TabIndex = 2;
            pMessages.Visible = false;
            // 
            // btnMessaagesOkay
            // 
            btnMessaagesOkay.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnMessaagesOkay.Cursor = System.Windows.Forms.Cursors.Hand;
            btnMessaagesOkay.FlatAppearance.BorderSize = 0;
            btnMessaagesOkay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnMessaagesOkay.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnMessaagesOkay.Location = new System.Drawing.Point(203, 87);
            btnMessaagesOkay.Margin = new System.Windows.Forms.Padding(0);
            btnMessaagesOkay.Name = "btnMessaagesOkay";
            btnMessaagesOkay.Size = new System.Drawing.Size(89, 38);
            btnMessaagesOkay.TabIndex = 5;
            btnMessaagesOkay.Text = "Continue";
            btnMessaagesOkay.UseVisualStyleBackColor = false;
            btnMessaagesOkay.Click += btnMessagesOkay_Click;
            // 
            // lblMessage
            // 
            lblMessage.AutoSize = true;
            lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblMessage.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lblMessage.Location = new System.Drawing.Point(3, 6);
            lblMessage.Name = "lblMessage";
            lblMessage.Size = new System.Drawing.Size(204, 17);
            lblMessage.TabIndex = 0;
            lblMessage.Text = "This is a message placeholder.";
            lblMessage.Visible = false;
            // 
            // pAddAccount
            // 
            pAddAccount.Controls.Add(tbAccountDisplayName);
            pAddAccount.Controls.Add(lDisplayName);
            pAddAccount.Controls.Add(btnAccountCancel);
            pAddAccount.Controls.Add(btnAccountSave);
            pAddAccount.Controls.Add(tbAccountPassword);
            pAddAccount.Controls.Add(lPassword);
            pAddAccount.Controls.Add(tbAccountUsername);
            pAddAccount.Controls.Add(lUsername);
            pAddAccount.Location = new System.Drawing.Point(607, 18);
            pAddAccount.Margin = new System.Windows.Forms.Padding(0);
            pAddAccount.Name = "pAddAccount";
            pAddAccount.Size = new System.Drawing.Size(496, 152);
            pAddAccount.TabIndex = 0;
            pAddAccount.Visible = false;
            // 
            // tbAccountDisplayName
            // 
            tbAccountDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbAccountDisplayName.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbAccountDisplayName.Location = new System.Drawing.Point(122, 41);
            tbAccountDisplayName.Name = "tbAccountDisplayName";
            tbAccountDisplayName.Size = new System.Drawing.Size(222, 23);
            tbAccountDisplayName.TabIndex = 2;
            // 
            // lDisplayName
            // 
            lDisplayName.AutoSize = true;
            lDisplayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lDisplayName.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lDisplayName.Location = new System.Drawing.Point(3, 44);
            lDisplayName.Name = "lDisplayName";
            lDisplayName.Size = new System.Drawing.Size(95, 17);
            lDisplayName.TabIndex = 6;
            lDisplayName.Text = "Display Name";
            // 
            // btnAccountCancel
            // 
            btnAccountCancel.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnAccountCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAccountCancel.FlatAppearance.BorderSize = 0;
            btnAccountCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnAccountCancel.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnAccountCancel.Location = new System.Drawing.Point(264, 98);
            btnAccountCancel.Margin = new System.Windows.Forms.Padding(0);
            btnAccountCancel.Name = "btnAccountCancel";
            btnAccountCancel.Size = new System.Drawing.Size(80, 38);
            btnAccountCancel.TabIndex = 5;
            btnAccountCancel.Text = "Cancel";
            btnAccountCancel.UseVisualStyleBackColor = false;
            btnAccountCancel.Click += btnAccountCancel_Click;
            // 
            // btnAccountSave
            // 
            btnAccountSave.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnAccountSave.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAccountSave.FlatAppearance.BorderSize = 0;
            btnAccountSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnAccountSave.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnAccountSave.Location = new System.Drawing.Point(184, 98);
            btnAccountSave.Margin = new System.Windows.Forms.Padding(0);
            btnAccountSave.Name = "btnAccountSave";
            btnAccountSave.Size = new System.Drawing.Size(80, 38);
            btnAccountSave.TabIndex = 4;
            btnAccountSave.Text = "Save";
            btnAccountSave.UseVisualStyleBackColor = false;
            btnAccountSave.Click += btnAccountSave_Click;
            // 
            // tbAccountPassword
            // 
            tbAccountPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbAccountPassword.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbAccountPassword.Location = new System.Drawing.Point(122, 71);
            tbAccountPassword.Name = "tbAccountPassword";
            tbAccountPassword.PasswordChar = '*';
            tbAccountPassword.Size = new System.Drawing.Size(222, 23);
            tbAccountPassword.TabIndex = 3;
            tbAccountPassword.KeyPress += tbAccountPassword_KeyPress;
            // 
            // lPassword
            // 
            lPassword.AutoSize = true;
            lPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lPassword.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lPassword.Location = new System.Drawing.Point(3, 74);
            lPassword.Name = "lPassword";
            lPassword.Size = new System.Drawing.Size(69, 17);
            lPassword.TabIndex = 2;
            lPassword.Text = "Password";
            // 
            // tbAccountUsername
            // 
            tbAccountUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            tbAccountUsername.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            tbAccountUsername.Location = new System.Drawing.Point(122, 11);
            tbAccountUsername.Name = "tbAccountUsername";
            tbAccountUsername.Size = new System.Drawing.Size(222, 23);
            tbAccountUsername.TabIndex = 1;
            tbAccountUsername.Leave += tbAccountUsername_Leave;
            // 
            // lUsername
            // 
            lUsername.AutoSize = true;
            lUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lUsername.ForeColor = System.Drawing.Color.FromArgb(14, 25, 49);
            lUsername.Location = new System.Drawing.Point(3, 14);
            lUsername.Name = "lUsername";
            lUsername.Size = new System.Drawing.Size(73, 17);
            lUsername.TabIndex = 0;
            lUsername.Text = "Username";
            // 
            // pMainMenu
            // 
            pMainMenu.Controls.Add(btnRefreshStatus);
            pMainMenu.Controls.Add(lblServerStatus);
            pMainMenu.Location = new System.Drawing.Point(791, 269);
            pMainMenu.Name = "pMainMenu";
            pMainMenu.Size = new System.Drawing.Size(272, 164);
            pMainMenu.TabIndex = 4;
            pMainMenu.Visible = false;
            // 
            // btnRefreshStatus
            // 
            btnRefreshStatus.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnRefreshStatus.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnRefreshStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            btnRefreshStatus.FlatAppearance.BorderSize = 0;
            btnRefreshStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnRefreshStatus.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnRefreshStatus.Location = new System.Drawing.Point(171, 121);
            btnRefreshStatus.Margin = new System.Windows.Forms.Padding(0);
            btnRefreshStatus.Name = "btnRefreshStatus";
            btnRefreshStatus.Size = new System.Drawing.Size(93, 38);
            btnRefreshStatus.TabIndex = 5;
            btnRefreshStatus.Text = "Refresh";
            btnRefreshStatus.UseVisualStyleBackColor = false;
            btnRefreshStatus.Click += btnRefreshStatus_Click;
            // 
            // lblServerStatus
            // 
            lblServerStatus.AutoSize = true;
            lblServerStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lblServerStatus.Location = new System.Drawing.Point(8, 6);
            lblServerStatus.Name = "lblServerStatus";
            lblServerStatus.Size = new System.Drawing.Size(150, 17);
            lblServerStatus.TabIndex = 0;
            lblServerStatus.Text = "Replace me at runtime";
            // 
            // ilMain
            // 
            ilMain.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            ilMain.ImageSize = new System.Drawing.Size(36, 36);
            ilMain.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmsDDO
            // 
            cmsDDO.ImageScalingSize = new System.Drawing.Size(24, 24);
            cmsDDO.Name = "cmsDDO";
            cmsDDO.Size = new System.Drawing.Size(61, 4);
            // 
            // cmsLotro
            // 
            cmsLotro.ImageScalingSize = new System.Drawing.Size(24, 24);
            cmsLotro.Name = "cmsLotro";
            cmsLotro.Size = new System.Drawing.Size(61, 4);
            // 
            // pUnsavedChanges
            // 
            pUnsavedChanges.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            pUnsavedChanges.Controls.Add(pUnsavedChangesDialog);
            pUnsavedChanges.Location = new System.Drawing.Point(1414, 91);
            pUnsavedChanges.Name = "pUnsavedChanges";
            pUnsavedChanges.Size = new System.Drawing.Size(676, 116);
            pUnsavedChanges.TabIndex = 10;
            pUnsavedChanges.Visible = false;
            // 
            // pUnsavedChangesDialog
            // 
            pUnsavedChangesDialog.BackColor = System.Drawing.Color.FromArgb(16, 79, 153);
            pUnsavedChangesDialog.Controls.Add(btnDiscardUnsavedChanges);
            pUnsavedChangesDialog.Controls.Add(btnSaveUnsavedChanges);
            pUnsavedChangesDialog.Controls.Add(label2);
            pUnsavedChangesDialog.Controls.Add(label1);
            pUnsavedChangesDialog.Location = new System.Drawing.Point(157, 8);
            pUnsavedChangesDialog.Name = "pUnsavedChangesDialog";
            pUnsavedChangesDialog.Size = new System.Drawing.Size(362, 100);
            pUnsavedChangesDialog.TabIndex = 0;
            // 
            // btnDiscardUnsavedChanges
            // 
            btnDiscardUnsavedChanges.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnDiscardUnsavedChanges.BackColor = System.Drawing.Color.FromArgb(142, 152, 173);
            btnDiscardUnsavedChanges.Cursor = System.Windows.Forms.Cursors.Hand;
            btnDiscardUnsavedChanges.FlatAppearance.BorderSize = 0;
            btnDiscardUnsavedChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnDiscardUnsavedChanges.ForeColor = System.Drawing.Color.Black;
            btnDiscardUnsavedChanges.Location = new System.Drawing.Point(282, 59);
            btnDiscardUnsavedChanges.Margin = new System.Windows.Forms.Padding(0);
            btnDiscardUnsavedChanges.Name = "btnDiscardUnsavedChanges";
            btnDiscardUnsavedChanges.Size = new System.Drawing.Size(80, 38);
            btnDiscardUnsavedChanges.TabIndex = 17;
            btnDiscardUnsavedChanges.Text = "Discard";
            btnDiscardUnsavedChanges.UseVisualStyleBackColor = false;
            btnDiscardUnsavedChanges.Click += btnDiscardUnsavedChanges_Click;
            // 
            // btnSaveUnsavedChanges
            // 
            btnSaveUnsavedChanges.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnSaveUnsavedChanges.BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            btnSaveUnsavedChanges.Cursor = System.Windows.Forms.Cursors.Hand;
            btnSaveUnsavedChanges.FlatAppearance.BorderSize = 0;
            btnSaveUnsavedChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            btnSaveUnsavedChanges.ForeColor = System.Drawing.Color.FromArgb(51, 252, 254);
            btnSaveUnsavedChanges.Location = new System.Drawing.Point(170, 59);
            btnSaveUnsavedChanges.Margin = new System.Windows.Forms.Padding(0);
            btnSaveUnsavedChanges.Name = "btnSaveUnsavedChanges";
            btnSaveUnsavedChanges.Size = new System.Drawing.Size(112, 38);
            btnSaveUnsavedChanges.TabIndex = 16;
            btnSaveUnsavedChanges.Text = "Save now";
            btnSaveUnsavedChanges.UseVisualStyleBackColor = false;
            btnSaveUnsavedChanges.Click += btnSaveUnsavedChanges_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            label2.ForeColor = System.Drawing.Color.White;
            label2.Location = new System.Drawing.Point(3, 28);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(209, 20);
            label2.TabIndex = 1;
            label2.Text = "You have unsaved changes!";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            label1.ForeColor = System.Drawing.Color.White;
            label1.Location = new System.Drawing.Point(3, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(180, 24);
            label1.TabIndex = 0;
            label1.Text = "Unsaved Changes";
            // 
            // optionsTT
            // 
            optionsTT.AutoPopDelay = 10000;
            optionsTT.InitialDelay = 500;
            optionsTT.ReshowDelay = 100;
            // 
            // tmrOrganizeAccounts
            // 
            tmrOrganizeAccounts.Interval = 33;
            tmrOrganizeAccounts.Tick += tmrOrganizeAccounts_Tick;
            // 
            // cmsAdvancedOptions
            // 
            cmsAdvancedOptions.ImageScalingSize = new System.Drawing.Size(32, 32);
            cmsAdvancedOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { getCommandLineArgumentsToolStripMenuItem, copyShortcutIdShortcutToolStripMenuItem, createDesktopShortcutToolStripMenuItem, hideThisShortcutFromTrayToolStripMenuItem, showThisShortcutInTrayToolStripMenuItem, assignCustomPreferencesFileToolStripMenuItem, updateCustomPreferencesFileToolStripMenuItem });
            cmsAdvancedOptions.Name = "contextMenuStrip1";
            cmsAdvancedOptions.Size = new System.Drawing.Size(243, 158);
            // 
            // getCommandLineArgumentsToolStripMenuItem
            // 
            getCommandLineArgumentsToolStripMenuItem.Name = "getCommandLineArgumentsToolStripMenuItem";
            getCommandLineArgumentsToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            getCommandLineArgumentsToolStripMenuItem.Text = "Get Command Line Arguments";
            getCommandLineArgumentsToolStripMenuItem.Click += getCommandLineArgumentsToolStripMenuItem_Click;
            // 
            // copyShortcutIdShortcutToolStripMenuItem
            // 
            copyShortcutIdShortcutToolStripMenuItem.Name = "copyShortcutIdShortcutToolStripMenuItem";
            copyShortcutIdShortcutToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            copyShortcutIdShortcutToolStripMenuItem.Text = "Copy Shortcut ID to Clipboard";
            copyShortcutIdShortcutToolStripMenuItem.Click += copyShortcutIDToClipboardToolStripMenuItem_Click;
            // 
            // createDesktopShortcutToolStripMenuItem
            // 
            createDesktopShortcutToolStripMenuItem.Name = "createDesktopShortcutToolStripMenuItem";
            createDesktopShortcutToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            createDesktopShortcutToolStripMenuItem.Text = "Create Desktop Shortcut";
            createDesktopShortcutToolStripMenuItem.Click += createDesktopShortcutToolStripMenuItem_Click;
            // 
            // hideThisShortcutFromTrayToolStripMenuItem
            // 
            hideThisShortcutFromTrayToolStripMenuItem.Name = "hideThisShortcutFromTrayToolStripMenuItem";
            hideThisShortcutFromTrayToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            hideThisShortcutFromTrayToolStripMenuItem.Text = "Hide this Shortcut from Tray";
            hideThisShortcutFromTrayToolStripMenuItem.Click += hideThisShortcutFromTrayToolStripMenuItem_Click;
            // 
            // showThisShortcutInTrayToolStripMenuItem
            // 
            showThisShortcutInTrayToolStripMenuItem.Name = "showThisShortcutInTrayToolStripMenuItem";
            showThisShortcutInTrayToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            showThisShortcutInTrayToolStripMenuItem.Text = "Show this Shortcut in Tray";
            showThisShortcutInTrayToolStripMenuItem.Click += showThisShortcutInTrayToolStripMenuItem_Click;
            // 
            // assignCustomPreferencesFileToolStripMenuItem
            // 
            assignCustomPreferencesFileToolStripMenuItem.Name = "assignCustomPreferencesFileToolStripMenuItem";
            assignCustomPreferencesFileToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            assignCustomPreferencesFileToolStripMenuItem.Text = "Assign Custom Preferences File";
            assignCustomPreferencesFileToolStripMenuItem.Click += assignCustomPreferencesFileToolStripMenuItem_Click;
            // 
            // updateCustomPreferencesFileToolStripMenuItem
            // 
            updateCustomPreferencesFileToolStripMenuItem.Name = "updateCustomPreferencesFileToolStripMenuItem";
            updateCustomPreferencesFileToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            updateCustomPreferencesFileToolStripMenuItem.Text = "Update Custom Preferences File";
            updateCustomPreferencesFileToolStripMenuItem.Click += updateCustomPreferencesFileToolStripMenuItem_Click;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.FromArgb(15, 36, 79);
            ClientSize = new System.Drawing.Size(1924, 1061);
            Controls.Add(pUnsavedChanges);
            Controls.Add(scHeader);
            Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Name = "MainForm";
            Text = "Dungeon Finder";
            Deactivate += MainForm_Deactivate;
            FormClosing += MainForm_FormClosing;
            Load += MainForm_Load;
            scHeader.Panel1.ResumeLayout(false);
            scHeader.Panel1.PerformLayout();
            scHeader.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scHeader).EndInit();
            scHeader.ResumeLayout(false);
            scNav.Panel1.ResumeLayout(false);
            scNav.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scNav).EndInit();
            scNav.ResumeLayout(false);
            scMain.Panel1.ResumeLayout(false);
            scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scMain).EndInit();
            scMain.ResumeLayout(false);
            pAccounts.ResumeLayout(false);
            pAccounts.PerformLayout();
            pSettings.ResumeLayout(false);
            pHome.ResumeLayout(false);
            pUserSettings.ResumeLayout(false);
            pUserSettings.PerformLayout();
            pEditAccount.ResumeLayout(false);
            pEditAccount.PerformLayout();
            scAccountDetails.Panel1.ResumeLayout(false);
            scAccountDetails.Panel1.PerformLayout();
            scAccountDetails.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scAccountDetails).EndInit();
            scAccountDetails.ResumeLayout(false);
            scAccountDetailsLower.Panel1.ResumeLayout(false);
            scAccountDetailsLower.Panel2.ResumeLayout(false);
            scAccountDetailsLower.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)scAccountDetailsLower).EndInit();
            scAccountDetailsLower.ResumeLayout(false);
            pShortcuts.ResumeLayout(false);
            pShortcuts.PerformLayout();
            pMessages.ResumeLayout(false);
            pMessages.PerformLayout();
            pAddAccount.ResumeLayout(false);
            pAddAccount.PerformLayout();
            pMainMenu.ResumeLayout(false);
            pMainMenu.PerformLayout();
            pUnsavedChanges.ResumeLayout(false);
            pUnsavedChangesDialog.ResumeLayout(false);
            pUnsavedChangesDialog.PerformLayout();
            cmsAdvancedOptions.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer scHeader;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.SplitContainer scNav;
        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.ImageList ilMain;
        private System.Windows.Forms.Button btnAccounts;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel pSettings;
        private System.Windows.Forms.Panel pHome;
        private System.Windows.Forms.Button btnDiscord;
        private System.Windows.Forms.Button btnWeb;
        private System.Windows.Forms.Button btnMainSettings;
        private System.Windows.Forms.Panel pAddAccount;
        private System.Windows.Forms.Label lUsername;
        private System.Windows.Forms.Button btnAccountCancel;
        private System.Windows.Forms.Button btnAccountSave;
        private System.Windows.Forms.TextBox tbAccountPassword;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.TextBox tbAccountUsername;
        private System.Windows.Forms.TextBox tbAccountDisplayName;
        private System.Windows.Forms.Label lDisplayName;
        private System.Windows.Forms.Panel pMessages;
        private System.Windows.Forms.Button btnMessaagesOkay;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel pEditAccount;
        private System.Windows.Forms.TextBox tbEditDisplayName;
        private System.Windows.Forms.Label lEditDisplay;
        private System.Windows.Forms.Button btnEditAccountCancel;
        private System.Windows.Forms.Button btnEditAccountSave;
        private System.Windows.Forms.TextBox tbEditPassword;
        private System.Windows.Forms.Label lEditPassword;
        private System.Windows.Forms.TextBox tbEditUsername;
        private System.Windows.Forms.Label lEditUsername;
        private System.Windows.Forms.ContextMenuStrip cmsDDO;
        private System.Windows.Forms.ContextMenuStrip cmsLotro;
        private System.Windows.Forms.Panel pMainMenu;
        private System.Windows.Forms.Button btnRefreshStatus;
        private System.Windows.Forms.Label lblServerStatus;
        private System.Windows.Forms.SplitContainer scAccountDetails;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.ComboBox cbPlayServers;
        private System.Windows.Forms.SplitContainer scAccountDetailsLower;
        private System.Windows.Forms.Label lblSubscriptionInfo;
        private System.Windows.Forms.CheckBox cbMakeDefault;
        private System.Windows.Forms.Button btnRemoveAccount;
        private System.Windows.Forms.Button btnEditSubSave;
        private System.Windows.Forms.TextBox tbEditSubDisplayName;
        private System.Windows.Forms.Label lblEditSubDisplayName;
        private System.Windows.Forms.Button btnAddShortcut;
        private System.Windows.Forms.TextBox tbAddShortcutCharacter;
        private System.Windows.Forms.Panel pShortcuts;
        private System.Windows.Forms.Label lblShortcuts;
        private System.Windows.Forms.Panel pUserSettings;
        private System.Windows.Forms.Label lblDdoPath;
        private System.Windows.Forms.TextBox txtBxLOTROPath;
        private System.Windows.Forms.TextBox txtBxDDOPath;
        private System.Windows.Forms.Button btnUserSettingsSave;
        private System.Windows.Forms.Label lblLotroPath;
        private System.Windows.Forms.Button btnFindLotro;
        private System.Windows.Forms.Button btnFindDdo;
        private System.Windows.Forms.Button btnDDOPatchNow;
        private System.Windows.Forms.Button btnLOTROPatchNow;
        private System.Windows.Forms.Button btnMyDDO;
        private System.Windows.Forms.Button btnCancelShortcut;
        private System.Windows.Forms.TextBox tbShortcutDisplayName;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnYouTube;
        private System.Windows.Forms.Button btnVersionInfo;
        private System.Windows.Forms.CheckBox cbMinimizeToTray;
        private System.Windows.Forms.ToolTip optionsTT;
        private System.Windows.Forms.CheckBox cbShowAdvancedOptionsButton;
        private System.Windows.Forms.CheckBox cbStartWithWindows;
        private System.Windows.Forms.CheckBox cb64bit;
        private System.Windows.Forms.CheckBox cbMinimizeOnLaunch;
        private System.Windows.Forms.CheckBox cbEnablePatchNotifications;
        private System.Windows.Forms.CheckBox cbAutoPatch;
        private System.Windows.Forms.Panel pUnsavedChanges;
        private System.Windows.Forms.Panel pUnsavedChangesDialog;
        private System.Windows.Forms.Button btnDiscardUnsavedChanges;
        private System.Windows.Forms.Button btnSaveUnsavedChanges;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer tmrOrganizeAccounts;
        private System.Windows.Forms.TableLayoutPanel pAccounts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbAllowDiagnostics;
        private System.Windows.Forms.Button btnCreateGroup;
        private AdvancedOptionsMenuStrip cmsAdvancedOptions;
        private System.Windows.Forms.ToolStripMenuItem getCommandLineArgumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyShortcutIdShortcutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createDesktopShortcutToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbHideDDOSub;
        private System.Windows.Forms.CheckBox cbHideLOTROSub;
        private System.Windows.Forms.ToolStripMenuItem hideThisShortcutFromTrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showThisShortcutInTrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignCustomPreferencesFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateCustomPreferencesFileToolStripMenuItem;
        private System.Windows.Forms.Button btnPreferences;
        private System.Windows.Forms.CheckBox cbUpdateOnStart;
    }
}